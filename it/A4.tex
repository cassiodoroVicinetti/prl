\chapter{Evoluzioni di Ethernet}
%3
Con il successo di Ethernet sorsero nuovi problemi:
\begin{itemize}
 \item necessità di \ul{maggiore velocità}: Ethernet II DIX supportava una velocità di trasmissione pari a 10 Mbps, mentre FDDI, utilizzata nel backbone, supportava una velocità molto più alta (100 Mbps), ma sarebbe stato troppo costoso cablare gli edifici con la fibra ottica;
 \item necessità di \ul{interconnettere più reti}: reti di tecnologie diverse (ad es. Ethernet, FDDI, token ring) erano difficili da interconnettere perché avevano delle MTU diverse $\Rightarrow$ avere la stessa tecnologia dappertutto avrebbe risolto questo problema.
\end{itemize}

\section{Fast Ethernet}
%6-8-9
\textbf{Fast Ethernet}, standardizzato come IEEE 802.3u (1995), innalza la velocità di trasmissione a 100 Mbps e accorcia di conseguenza di 10 volte il massimo diametro di collisione ($\sim$200-300 m), mantenendone lo stesso formato di trama e lo stesso algoritmo CSMA/CD.

%7
\subsection{Livello fisico}
Il livello fisico di Fast Ethernet è completamente diverso dal livello fisico di Ethernet a 10 Mbps: deriva in parte da standard esistenti nel mondo FDDI, tanto che Fast Ethernet e FDDI sono compatibili a livello fisico, e abbandona definitivamente il cavo coassiale:
\begin{itemize}
 \item \ul{100BASE-T4}: doppino di rame che utilizza 4 coppie;
 \item \ul{100BASE-TX}: doppino di rame che utilizza 2 coppie;
 \item \ul{100BASE-FX}: fibra ottica (solo nel backbone).
\end{itemize}

%5-10-11
\subsection{Adozione}
Quando fu introdotto Fast Ethernet, il suo tasso di adozione fu piuttosto basso a causa di:
\begin{itemize}
 \item \ul{limite di distanza}: le dimensioni della rete erano limitate $ \Rightarrow $ Fast Ethernet non era appropriato per il backbone;
 \item \ul{colli di bottiglia nel backbone}: il backbone realizzato in tecnologia FDDI a 100 Mbps aveva la stessa velocità delle reti di accesso in tecnologia Fast Ethernet $\Rightarrow$ rischiava di non riuscire a smaltire tutto il traffico proveniente dalle reti di accesso.
\end{itemize}

Fast Ethernet cominciò a essere adottato più ampiamente con:
\begin{itemize}
 \item l'introduzione dei \ul{bridge}: interrompono il dominio di collisione superando il limite di distanza;
 \item l'introduzione di \ul{Gigabit Ethernet} nel backbone: evita i colli di bottiglia nel backbone.
\end{itemize}

%12-16
\section{Gigabit Ethernet}
\textbf{Gigabit Ethernet}, standardizzato come IEEE 802.3z (1998), innalza la velocità di trasmissione a 1 Gbps e introduce due funzionalità, la ``Carrier Extension'' e il ``Frame Bursting'', per mantenere funzionante il protocollo CSMA/CD.

\subsection{Carrier Extension}
%13-17-18
La decuplicazione della velocità di trasmissione ridurrebbe il massimo diametro di collisione di altre 10 volte portandolo a qualche decina di metri, troppo pochi per il cablaggio $\Rightarrow$ per mantenere invariato il massimo diametro di collisione, sarebbe necessario aumentare la dimensione minima della trama a 512 byte\footnote{Teoricamente la trama sarebbe da allungare di 10 volte, quindi a 640 byte, ma lo standard decise diversamente.}.

L'allungamento della trama minima però genererebbe un problema di incompatibilità: nell'interconnessione di una rete Fast Ethernet e di una rete Gigabit Ethernet con un bridge, le trame di dimensione minima provenienti dalla rete Fast Ethernet non potrebbero entrare nella rete Gigabit Ethernet $\Rightarrow$ invece di allungare la trama fu allungato lo slot time, cioè l'unità temporale minima di trasmissione: in fondo a tutte le trame più corte di 512 byte viene aggiunta una \textbf{Carrier Extension} composta da bit fittizi di padding (fino a 448 byte):
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|c|c|c|c|}
  \multicolumn{1}{c}{\footnotesize{7 byte}} & \multicolumn{1}{c}{\footnotesize{1 byte}} & \multicolumn{1}{c}{\footnotesize{da 64 a 1518 byte}} & \multicolumn{1}{c}{\footnotesize{da 0 a 448 byte}} & \multicolumn{1}{c}{\footnotesize{12 byte}} \\
  \hline
  preambolo & SFD & trama Ethernet II DIX/IEEE 802.3 & Carrier Extension & IFG \\
  \hline
  \multicolumn{2}{c|}{\footnotesize{}} & \multicolumn{2}{|c|}{\footnotesize{da 512 a 1518 byte}} & \multicolumn{1}{|c}{\footnotesize{}} \\
 \end{tabular}}
 \caption{Formato del pacchetto Gigabit Ethernet (da 532 a 1538 byte).}
\end{table}

%14-15
\paragraph{Svantaggi}
\begin{itemize}
 \item la Carrier Extension occupa il canale con dei \ul{bit inutili}.\\
 Ad esempio con trame da 64 byte il throughput utile è molto basso:
 \[
  \frac{64 \; \text{byte}}{512 \; \text{byte}} \cdot 1 \; \text{Gbit/s} = 125 \; \text{Mbit/s}
 \]
 %24
 \item nelle reti moderne commutate pure è attiva la \ul{modalità full duplex} $\Rightarrow$ il CSMA/CD è disabilitato $\Rightarrow$ la Carrier Extension è inutile.
\end{itemize}

\subsection{Frame Bursting}
%19
La massima dimensione della trama di 1518 byte è ormai obsoleta: in Ethernet a 10 Mbps l'occupazione del canale era pari a 1,2 ms, un tempo ragionevole per garantire il multiplexing statistico\footnote{Si veda la sezione~\ref{sez:multiplexing_statistico}.}, mentre in Gigabit Ethernet l'occupazione del canale è pari a 12 $\mu$s $\Rightarrow$ le collisioni sono molto meno frequenti $\Rightarrow$ per diminuire l'overhead delle intestazioni in rapporto ai dati utili migliorando l'efficienza, si potrebbe aumentare la dimensione massima della trama.

%20-21
L'allungamento della trama massima però genererebbe un problema di incompatibilità: nell'interconnessione di una rete Fast Ethernet e di una rete Gigabit Ethernet con un bridge, le trame di dimensione massima provenienti dalla rete Gigabit Ethernet non potrebbero entrare nella rete Fast Ethernet $\Rightarrow$ il \textbf{Frame Bursting} consiste nella concatenazione di diverse trame di dimensione standard una dopo l'altra, senza rilasciare il canale:
\begin{table}
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|c|c|cc|c|}
 \hline
 \begin{tabular}[c]{@{}c@{}}trama 1\footnotemark\\\footnotesize{+ Carrier Extension}\end{tabular} & FILL & trama 2\footnotemark[\value{footnote}] & FILL & \textellipsis & FILL & \mbox{} & ultima trama\footnotemark[\value{footnote}] & IFG \\
 \hline
 \multicolumn{7}{|c|}{\footnotesize{burst limit (8192 byte)}} & \multicolumn{2}{|c}{\footnotesize{}} \\
 \end{tabular}}
 \caption{Formato del pacchetto Gigabit Ethernet con Frame Bursting.}
\end{table}
\footnotetext{preambolo + SFD + trama Ethernet II DIX/IEEE 802.3}
\begin{itemize}
 \item solo la prima trama viene eventualmente estesa con la Carrier Extension, per assicurarsi che la finestra di collisione sia riempita; nelle trame successive la Carrier Extension non serve perché, se avvenisse una collisione, essa sarebbe individuata già con la prima trama;
 \item l'IFG tra una trama e l'altra viene sostituito con una ``Filling Extension'' (FILL) per delimitare i byte e annunciare che seguirà un'altra trama;
 \item la stazione trasmittente mantiene un contatore di byte: quando arriva al byte numero 8192, la trama correntemente in trasmissione deve essere l'ultima $\Rightarrow$ con il Frame Bursting è possibile inviare fino a 8191 byte + 1 trama.
\end{itemize}
\FloatBarrier

\paragraph{Vantaggi}
\begin{itemize}
 \item il \ul{numero di occasioni di collisione} è diminuito: una volta che la prima trama è trasmessa senza collisioni, tutte le altre stazioni rilevano che il canale è occupato grazie al CSMA;
 \item le trame successive alla prima non richiedono la Carrier Extension $\Rightarrow$ il throughput utile aumenta soprattutto in caso di \ul{trame piccole}, grazie al risparmio della Carrier Extension.
\end{itemize}

%22
\paragraph{Svantaggi}
\begin{itemize}
 \item il Frame Bursting non risponde all'obiettivo originario di \ul{ridurre l'overhead delle intestazioni}: è stato scelto di mantenere in ogni trama tutte le intestazioni (compresi il preambolo, lo SFD e l'IFG) per semplificare l'hardware di elaborazione;
 \item tipicamente una stazione che utilizza il Frame Bursting deve inviare tanti dati $\Rightarrow$ le \ul{trame grosse} non richiedono la Carrier Extension $\Rightarrow$ non c'è il risparmio della Carrier Extension;
 %24
 \item nelle reti moderne commutate pure è attiva la \ul{modalità full duplex} $\Rightarrow$ il CSMA/CD è disabilitato $\Rightarrow$ il Frame Bursting non ha alcun vantaggio e perciò è inutile.
\end{itemize}

\subsection{Livello fisico}
%25
Gigabit Ethernet può viaggiare sui seguenti mezzi fisici di trasmissione:
\begin{itemize}
 \item \ul{doppino di rame}:
 \begin{itemize}
  \item \ul{Shielded} (STP): lo standard 1000BASE-CX utilizza 2 coppie (25 m);
  \item \ul{Unshielded} (UTP): lo standard 1000BASE-T utilizza 4 coppie (100 m);
 \end{itemize}
 \item \ul{fibra ottica}: gli standard 1000BASE-SX e 1000BASE-LX utilizzano 2 fibre, e possono essere:
 \begin{itemize}
  \item \ul{Multi-Mode Fiber} (MMF): meno pregiate (275-550 m);
  \item \ul{Single-Mode Fiber} (SMF): ha una lunghezza massima di 5 km.
 \end{itemize}
\end{itemize}

%33
\paragraph{GBIC}
Gigabit Ethernet introdusse per la prima volta i \textbf{gigabit interface converter} (GBIC), che sono una soluzione comune per avere la possibilità di aggiornare il livello fisico senza dover aggiornare il resto dell'apparecchiatura: la scheda Gigabit Ethernet non ha il livello fisico integrato a bordo, ma include solo la parte logica (dal livello data-link in su), e l'utente può collegare negli appositi alloggiamenti della scheda il GBIC desiderato che implementa il livello fisico.

%34-35
\section{10 Gigabit Ethernet}
\textbf{10 Gigabit Ethernet}, standardizzato come IEEE 802.3ae (2002), innalza la velocità di trasmissione a 10 Gbps e abbandona finalmente la modalità half duplex, eliminando tutte le problematiche derivanti dal CSMA/CD.

Non è ancora usato nelle reti di accesso, ma è principalmente usato:
\begin{itemize}
 \item nei \ul{backbone}: viaggia su fibra ottica (da 26 m a 40 km) perché il doppino di rame non è più sufficiente a causa dei limiti di attenuazione del segnale;
 \item nei \ul{datacenter}: oltre alle fibre ottiche, si usano anche cavi molto corti per collegare i server agli switch top of the rack (TOR):\footnote{Si rimanda alla sezione~\ref{sez:FCoE}.}
 \begin{itemize}
  \item Twinax: cavi coassiali, inizialmente usati perché le unità per la trasmissione sui doppini di rame consumavano troppa energia;
  \item 10GBase T: doppini di rame schermati, aventi una latenza molto alta;
 \end{itemize}
 %36
 \item nelle \ul{reti metropolitane} (MAN) e nelle \ul{reti geografiche} (WAN): 10 Gigabit Ethernet può essere trasportato sulle infrastrutture MAN e WAN già esistenti, anche se a una velocità di trasmissione ridotta a 9,6 Gb/s.
\end{itemize}

%42
\section{40 Gigabit Ethernet e 100 Gigabit Ethernet}
\textbf{40 Gigabit Ethernet} e \textbf{100 Gigabit Ethernet}, entrambi standardizzati come IEEE 802.3ba (2010), innalzano la velocità di trasmissione rispettivamente a 40 Gbps e a 100 Gbps: per la prima volta l'evoluzione della velocità di trasmissione non è più a 10$\times$, ma è stato deciso di definire uno standard a una velocità intermedia per via degli costi ancora elevati di 100 Gigabit Ethernet. Inoltre, 40 Gigabit Ethernet può essere trasportato sull'infrastruttura DWDM già esistente.

Queste velocità sono usate solo nel backbone perché non sono ancora adatte non solo per gli host, ma anche per i server, perché sono molto vicine alle velocità interne delle unità di elaborazione (bus, memoria, ecc.) $\Rightarrow$ il collo di bottiglia non è più la rete.