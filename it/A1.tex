%Note: References are to 2013 slides.
\chapter{Introduzione alle Local Area Network}
\section{Origini}
%11
\subsection{Definizione di LAN}
Il gruppo di lavoro IEEE 802 definì la \textbf{rete locale} (LAN, dall'acronimo inglese ``Local Area Network'') come un sistema di comunicazione attraverso un \ul{mezzo condiviso}, che permette a \ul{dispositivi indipendenti} di comunicare tra loro entro un'\ul{area limitata}, utilizzando un canale di comunicazione \ul{ad alta velocità} e \ul{affidabile}.

\paragraph{Parole chiave}
\begin{itemize}
 \item \ul{mezzo condiviso}: tutti sono attaccati allo stesso mezzo di comunicazione;
 \item \ul{dispositivi indipendenti}: tutti sono paritetici, cioè hanno gli stessi privilegi nel poter parlare (no interazione client-server);
 \item \ul{area limitata}: tutti si trovano nella stessa area locale (ad es. azienda, campus universitario) e sono distanti al massimo qualche chilometro l'uno dall'altro (no attraversamento del suolo pubblico);
 \item \ul{ad alta velocità}: all'epoca le velocità delle LAN si misuravano in Megabit al secondo (Mbps), mentre le velocità delle WAN in bit al secondo;
 \item \ul{affidabile}: i guasti sono poco frequenti $\Rightarrow$ i controlli sono meno sofisticati a vantaggio delle prestazioni.
\end{itemize}

%3-4-5-6-7-8
\subsection{Confronto LAN vs. WAN}
I protocolli per le \textbf{reti geografiche} (WAN, dall'acronimo inglese ``Wide Area Network'') e per le reti locali si evolverono in maniera indipendente fino agli '80 perché gli scopi erano differenti. Negli anni '90 la tecnologia IP permise finalmente di interconnettere questi due mondi.

\paragraph{WAN} Le WAN nacquero negli anni '60 per connettere i terminali remoti ai pochi mainframe esistenti:
 \begin{itemize}
  \item \ul{mezzo fisico di comunicazione}: linea dedicata punto punto su lunga distanza;
  \item \ul{proprietà del mezzo fisico}: l'amministratore della rete deve affittare i cavi dal monopolio di Stato;
  \item \ul{modello di utilizzo}: regolare, cioè piccola occupazione di banda per lunghi periodi di tempo (ad es. sessione di terminale);
  \item \ul{tipo di comunicazione}: sempre unicast, più comunicazioni allo stesso tempo;
  \item \ul{qualità del mezzo fisico}: elevata frequenza dei guasti, velocità basse, elevata presenza di disturbi elettromagnetici;
  \item \ul{costi}: elevati, anche in termini di costi operativi (ad es. canone di affitto dei cavi);
  \item \ul{sistema di commutazione intermedio}: richiesto per gestire le comunicazioni su larga scala (ad es. commutatori telefonici) $\Rightarrow$ gli apparati di commutazione possono guastarsi.
 \end{itemize}
 
\paragraph{LAN} Le LAN comparvero alla fine degli anni '70 per condividere le risorse (come stampanti, dischi) tra piccoli gruppi di lavoro (ad es. dipartimenti):
 \begin{itemize}
  \item \ul{mezzo fisico di comunicazione}: architettura a bus condiviso multi-punto su breve distanza;
  \item \ul{proprietà del mezzo fisico}: l'amministratore della rete possiede i cavi;
  \item \ul{modello di utilizzo}: bursty, cioè picchi di dati di breve durata (ad es. stampa di un documento) seguiti da lunghi silenzi;
  \item \ul{tipo di comunicazione}: sempre broadcast, una sola comunicazione allo stesso tempo;
  \item \ul{qualità del mezzo fisico}: maggiore affidabilità contro i guasti, velocità elevate, minore esposizione a disturbi esterni;
  \item \ul{costi}: ragionevoli, concentrati principalmente nell'installazione della rete;
  \item \ul{sistema di commutazione intermedio}: non richiesto $\Rightarrow$ costo minore, velocità più alta, maggiore affidabilità, maggiore flessibilità nell'aggiungere e rimuovere stazioni.
 \end{itemize}

\subsection{Condivisione del mezzo di comunicazione}
\label{sez:multiplexing_statistico}
%10
Prima dell'avvento di hub e bridge, il mezzo di comunicazione condiviso poteva essere implementato in due modi:
\begin{itemize}
 \item \ul{broadcast fisico}: tecnologie basate sul broadcast, come il bus: il segnale inviato da una stazione si propaga a tutte le altre stazioni;
 \item \ul{broadcast logico}: tecnologie punto punto, come il token ring: il segnale inviato da una stazione arriva alla stazione successiva, la quale lo duplica verso la stazione ancora successiva, e così via.
\end{itemize}

%9
\paragraph{Problemi}
\begin{itemize}
 \item \ul{privacy}: tutti possono sentire quello che passa nel mezzo condiviso $\Rightarrow$ è necessario realizzare un sistema di indirizzamento (oggi: gli indirizzi MAC);
 \item \ul{concorrenza}: è possibile solo una comunicazione alla volta:
 \begin{itemize}
  \item \ul{collisioni}: se due stazioni trasmettono contemporaneamente, i dati inviati da una stazione potrebbero sovrapporsi ai dati inviati dall'altra $\Rightarrow$ è necessario realizzare un meccanismo per il rilevamento delle collisioni e il recupero da esse (oggi: il protocollo CSMA/CD, si rimanda alla sezione~\ref{sez:csma_cd});
  \item \ul{monopolizzazione del canale}: nella \textbf{trasmissione back to back}, una stazione può occupare il canale per un lungo periodo di tempo impedendo alle altre stazioni di parlare $\Rightarrow$ è necessario realizzare una sorta di \textbf{multiplexing statistico}, cioè simulare più comunicazioni in contemporanea definendo un'unità di trasmissione massima detta \textbf{chunk} e alternando i chunk di una stazione con quelli di un'altra (oggi: le trame Ethernet).
 \end{itemize}
\end{itemize}

\section{Sottolivelli data-link}
%20
Nelle LAN il livello data-link è diviso in due sottolivelli:
\begin{itemize}
 \item MAC: arbitra l'accesso al mezzo fisico, ed è specifico per ogni tecnologia di livello fisico (sezione~\ref{sez:mac});
 \item LLC: definisce l'interfaccia verso il livello rete, ed è comune in tutte le tecnologie di livello fisico (sezione~\ref{sez:llc}).
\end{itemize}

\subsection{MAC}
\label{sez:mac}

\let\orighref\href
\renewcommand{\href}{\Collectverb{\hrefii}}
\newcommand\hrefii[2]{\footnote{Secondo l'ordine canonico (network byte order), che è l'ordine nativo in IEEE 802.3 (Ethernet) ma non in IEEE 802.5 (token ring) (si veda la sezione \orighref{#1}{#2} nella voce \textit{MAC address} su Wikipedia in inglese).}}

%26
Ogni scheda di rete è identificata in modo univoco da un \textbf{indirizzo MAC}. Gli indirizzi MAC hanno il seguente formato:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|}
  \multicolumn{1}{r}{\footnotesize{24}} & \multicolumn{1}{r}{\footnotesize{48}} \\
  \hline
  OUI & NIC ID \\
  \hline
 \end{tabular}}
 \caption{Formato degli indirizzi MAC (6 byte).}
\end{table}
\noindent
dove i campi sono:
\begin{itemize}
 \item campo \ul{Organization Unique Identifier} (OUI) (3 byte): codice assegnato univocamente da IEEE per identificare il costruttore della scheda di rete:
 %28-30-31
 \begin{itemize}
  \item primo bit meno significativo del primo byte:\href{https://en.wikipedia.org/wiki/MAC_address#Bit-reversed_notation}{Bit-reversed notation}
  \begin{itemize}
    \item \ul{Individual} (valore 0): l'indirizzo è associato a una singola stazione (unicast);
    \item \ul{Group} (valore 1): l'indirizzo fa riferimento a più stazioni (multicast/broadcast);
  \end{itemize}
  \item secondo bit meno significativo del primo byte:\footnotemark[\value{footnote}]
  \begin{itemize}
   \item \ul{Universal} (valore 0): l'indirizzo è assegnato univocamente;
   \item \ul{Local} (valore 1): l'indirizzo è personalizzato dall'utente;
  \end{itemize}
 \end{itemize}
 \item campo \ul{NIC Identifier} (NIC ID) (3 byte): codice assegnato univocamente dal costruttore per identificare la specifica scheda di rete (detta anche ``Network Interface Controller'' [NIC]).
\end{itemize}

\let\href\orighref

%24
L'intestazione \textbf{Media Access Control} (MAC) ha il seguente formato:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|c|}
  \multicolumn{1}{r}{\footnotesize{48}} & \multicolumn{1}{r}{\footnotesize{96}} & \multicolumn{1}{r}{\footnotesize{112}} & \multicolumn{1}{c}{\footnotesize{da 46 a 1500 byte}} & \multicolumn{1}{c}{\footnotesize{4 byte}} \\
  \hline
  Destination Address & Source Address & Length & payload & FCS \\
  \hline
 \end{tabular}}
 \caption{Formato dell'intestazione MAC (18 byte).}
\end{table}
\noindent
dove i campi sono:
\begin{itemize}
 \item campo \ul{Destination Address} (6 byte): specifica l'indirizzo MAC della destinazione.\\
 È messo prima dell'indirizzo MAC della sorgente perché così la destinazione lo può elaborare prima e scartare la trama se non è indirizzata ad essa;
 \item campo \ul{Source Address} (6 byte): specifica l'indirizzo MAC della sorgente (sempre unicast);
 \item campo \ul{Length} (2 byte): specifica la lunghezza del payload;
 %37-38
 \item campo \ul{Frame Control Sequence} (FCS) (4 byte): contiene il codice CRC per il controllo di integrità sull'intera trama.\\
 Se il controllo del codice CRC fallisce, la trama arrivata è stata corrotta (ad es. a causa di una collisione) e viene scartata; i meccanismi di livello superiore (ad es. il TCP) si occuperanno di recuperare l'errore reinviando la trama.
\end{itemize}

%34-35
Una scheda di rete quando riceve una trama:
\begin{itemize}
\item se l'indirizzo MAC di destinazione coincide con quello della scheda di rete o è di tipo broadcast (``FF-FF-FF-FF-FF-FF''), la accetta e la manda ai livelli superiori;
\item se l'indirizzo MAC di destinazione non coincide con quello della scheda di rete, la scarta.
\end{itemize}

%36
Una scheda di rete impostata in modalità promiscua accetta tutte le trame $\Rightarrow$ serve per lo sniffing di rete.

\subsection{LLC}
\label{sez:llc}
%43
L'intestazione \textbf{Logical Link Control} (LLC) ha il seguente formato:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|}
  \multicolumn{1}{r}{\footnotesize{8}} & \multicolumn{1}{r}{\footnotesize{16}} & \multicolumn{1}{r}{\footnotesize{24 o 32}} \\
  \hline
  DSAP & SSAP & CTRL \\
  \hline
 \end{tabular}}
 \caption{Formato dell'intestazione LLC (3 o 4 byte).}
\end{table}
\noindent
dove i campi sono:
\begin{itemize}
 %43
 \item campo \ul{DSAP} (1 byte, di cui 2 bit riservati): identifica il protocollo di livello superiore utilizzato dalla destinazione;
 \item campo \ul{SSAP} (1 byte, di cui 2 bit riservati): identifica il protocollo di livello superiore utilizzato dalla sorgente;
 %44
 \item campo \ul{Control} (CTRL) (1 o 2 byte): deriva dal campo di controllo HDLC, ma è inutilizzato.
\end{itemize}

%46
\paragraph{Problemi dei campi DSAP e SSAP}
\begin{itemize}
 \item \ul{intervallo di valori limitato}: sono codificabili solo 64 protocolli;
 \item \ul{codici assegnati da ISO}: corrispondono a dei codici solo i protocolli pubblicati da una organizzazione degli standard internazionalmente riconosciuta, mentre sono esclusi i protocolli definiti da altri organi o spinti da alcuni fornitori (ad es. IP);
 \item \ul{ridondanza di codici}: non c'è alcun motivo di avere due campi per definire i protocolli, perché la sorgente e la destinazione parlano sempre lo stesso protocollo (ad es. entrambe IPv4 o entrambe IPv6).
\end{itemize}

%47
\subsubsection{SNAP}
Il \textbf{Subnetwork Access Protocol} (SNAP) è una particolare implementazione dell'LLC per i protocolli che non hanno un codice standard.

L'intestazione LLC SNAP ha il seguente formato:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|c|}
  \multicolumn{1}{r}{\footnotesize{8}} & \multicolumn{1}{r}{\footnotesize{16}} & \multicolumn{1}{r}{\footnotesize{24}} & \multicolumn{1}{r}{\footnotesize{48}} & \multicolumn{1}{r}{\footnotesize{64}} \\
  \hline
  DSAP (0xAA) & SSAP (0xAA) & CTRL (3) & OUI & Protocol Type \\
  \hline
 \end{tabular}}
 \caption{Formato dell'intestazione LLC SNAP (8 byte).}
\end{table}
\noindent
dove i campi sono:
\begin{itemize}
 \item campi \ul{DSAP}, \ul{SSAP}, \ul{CTRL}: i campi LLC sono fissi per indicare la presenza dell'intestazione SNAP;
 \item campo \ul{Organization Unique Identifier} (OUI) (3 byte): identifica l'organizzazione che ha definito il protocollo.\\
 Se è uguale a 0, il valore nel campo ``Protocol Type'' corrisponde a quello utilizzato in Ethernet DIX;
 \item campo \ul{Protocol Type} (2 byte): identifica il protocollo di livello superiore (ad es. 0x800 = IP, 0x806 = ARP).
\end{itemize}

%48
In realtà, l'intestazione LLC SNAP non è molto utilizzata a causa del suo spreco di byte, a vantaggio del campo ``Ethertype'' di Ethernet DIX (si rimanda alla sezione~\ref{sez:ethernet_dix}).