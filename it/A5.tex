\chapter{Funzionalità avanzate sulle reti Ethernet}
\section{Autonegoziazione}
%4
L'\textbf{autonegoziazione} è una funzione orientata al plug-and-play: quando una scheda di rete si connette a una rete, manda degli impulsi con una codifica particolare per provare a determinare le caratteristiche della rete:
\begin{itemize}
 \item \ul{modalità}: half duplex o full duplex (su doppino);
 \item \ul{velocità di trasmissione}: a partire dalla velocità più alta fino a quella più bassa (su doppino e fibra ottica).
\end{itemize}

\paragraph{Sequenza di negoziazione}
\begin{itemize}
 \item 1 Gb/s full duplex
 \item 1 Gb/s half duplex
 \item 100 Mb/s full duplex
 \item 100 Mb/s half duplex
 \item 10 Mb/s full duplex
 \item 10 Mb/s half duplex
\end{itemize}

%5
\subsection{Problemi}
L'autonegoziazione è possibile solo se la stazione si connette a un altro host o a un bridge: gli hub infatti operano a velocità fissa, quindi non possono negoziare niente. Se durante la procedura l'altra parte non risponde, la stazione in negoziazione assume di essere connessa a un hub $\Rightarrow$ imposta automaticamente la modalità a half duplex.

Se l'utente configura manualmente la propria scheda di rete a lavorare sempre in modalità full duplex disabilitando la funzione di autonegoziazione, quando si collega a un bridge quest'ultimo, non ricevendo risposta dall'altra parte, assume di essere collegato a un hub e imposta la modalità half duplex $\Rightarrow$ l'host considera possibile inviare e ricevere nello stesso momento sul canale, mentre il bridge considera ciò una collisione sul canale condiviso $\Rightarrow$ il bridge rileva molte collisioni che sono dei falsi positivi, e scarta erroneamente molte trame $\Rightarrow$ ogni trama scartata viene recuperata dai meccanismi di recupero dagli errori del TCP, che però sono molto lenti $\Rightarrow$ la velocità di accesso alla rete è molto bassa. Valori molto alti dei contatori di collisione su una specifica porta di un bridge sono sintomo di questa problematica.

%6-7-8
\section{Aumento della dimensione massima della trama}
La specifica originale di Ethernet definisce:
\begin{itemize}
 \item dimensione massima della trama: 1518 byte;
 \item dimensione massima del payload (Maximum Transmission Unit [MTU]): 1500 byte.
\end{itemize}

In molti casi però sarebbe utile avere una trama più grande del normale:
\begin{itemize}
 \item intestazioni aggiuntive (sezione~\ref{sez:baby_giant})
 \item payload più grande (sezione~\ref{sez:jumbo_frame})
 \item meno interrupt alla CPU (sezione~\ref{sez:tcp_offloading})
\end{itemize}

\subsection{Trame Baby Giant}
\label{sez:baby_giant}
Le \textbf{trame Baby Giant} sono trame con una dimensione maggiore della dimensione massima di 1518 byte definita dalla specifica originale di Ethernet, a causa dell'inserimento di nuove intestazioni di livello data-link al fine di trasportare informazioni aggiuntive sulla trama:
\begin{itemize}
 \item il \ul{tagging VLAN delle trame} (IEEE 802.1Q) aggiunge 4 byte;\footnote{Si rimanda alla sezione~\ref{sez:frame_tagging}.};
 \item il \ul{tag stacking VLAN} (IEEE 802.1ad) aggiunge 8 byte;\footnote{Si rimanda alla sezione~\ref{sez:tag_stacking}.}
 \item \ul{MPLS} aggiunge 4 byte per ogni etichetta impilata.\footnote{Si rimanda alla sezione \textit{Intestazione MPLS} nel capitolo \textit{MPLS} negli appunti di ``Tecnologie e servizi di rete''.}
\end{itemize}

Nelle trame Baby Giant la dimensione massima del payload (ad es. pacchetto IP) è invariata $\Rightarrow$ un router, quando riceve una trama Baby Giant, nel rigenerare il livello data-link può imbustare il payload in una trama Ethernet normale $\Rightarrow$ non è un problema l'interconnessione di LAN con differenti dimensioni massime delle trame supportate.

Lo standard IEEE 802.3as (2006) propone di estendere la dimensione massima della trama a 2000 byte, mantenendo invariata la dimensione della MTU.

%9-10
\subsection{Jumbo Frame}
\label{sez:jumbo_frame}
Le \textbf{Jumbo Frame} sono trame con una dimensione maggiore della dimensione massima di 1518 byte definita dalla specifica originale di Ethernet:
\begin{itemize}
 \item \ul{Mini Jumbo}: trame con MTU di dimensione pari a 2500 byte;
 \item \ul{Jumbo} (o ``Giant'' o ``Giant Frame''): trame con MTU di dimensione fino a 9 KB;
\end{itemize}
a causa dell'imbustamento di payload più grandi al fine di:
\begin{itemize}
 \item trasportare \ul{dati di archiviazione}: tipicamente le unità elementari dei dati di archiviazione sono troppo grandi da trasportare in un'unica trama Ethernet:
 \begin{itemize}
  \item il \ul{protocollo NFS} per i NAS trasporta blocchi di dati da 8 KB circa;\footnote{Si rimanda alla sezione~\ref{sez:NAS}.}
  \item il \ul{protocollo FCoE} per le SAN e il \ul{protocollo FCIP} per l'interconnessione di SAN trasportano trame Fibre Channel da 2,5 KB circa;\footnote{Si rimanda alle sezioni~\ref{sez:FCoE} e~\ref{sez:FCIP}.}
  \item il \ul{protocollo iSCSI} per le SAN trasporta comandi SCSI da 8 KB circa;\footnote{Si rimanda alla sezione~\ref{sez:iSCSI}.}
 \end{itemize}
 \item diminuire l'\ul{overhead delle intestazioni} in termini di:
 \begin{itemize}
  \item \ul{risparmio di byte}: non è molto significativo, soprattutto considerando l'elevata larghezza di banda a disposizione nelle reti odierne;
  \item \ul{capacità di elaborazione} per i meccanismi TCP (numeri di sequenza, timer\textellipsis): ogni pacchetto TCP scatena un interrupt alla CPU.
 \end{itemize}
\end{itemize}

Se una LAN che usa Jumbo Frame viene connessa con una LAN che non usa Jumbo Frame, tutte le Jumbo Frame verranno frammentate a livello IP, ma la frammentazione IP non è conveniente dal punto di vista delle prestazioni $\Rightarrow$ le Jumbo Frame vengono usate in reti indipendenti entro ambiti particolari.

\subsection{TCP offloading}
\label{sez:tcp_offloading}
Le schede di rete con la funzione \textbf{TCP offloading} possono automaticamente condensare al volo più payload TCP in un solo pacchetto IP prima di passarlo al sistema operativo (i numeri di sequenza e altri parametri sono gestiti internamente dalla scheda di rete) $\Rightarrow$ il sistema operativo, anziché dover elaborare più pacchetti piccoli scatenando tanti interrupt, vede un singolo pacchetto IP più grande e può farlo elaborare alla CPU in una volta sola $\Rightarrow$ ciò diminuisce l'overhead dovuto ai meccanismi del TCP.

\section{PoE}
%11-13
I bridge dotati della funzionalità \textbf{Power over Ethernet} (PoE) sono in grado di distribuire energia elettrica (fino a qualche decina di Watt) sui cavi Ethernet (solo doppini di rame), per connettere dispositivi con moderate esigenze di energia (telefoni VoIP, access point wi-fi, videocamere di sorveglianza, ecc.) evitando cavi aggiuntivi per l'energia elettrica.

Stazioni non PoE possono essere collegate a prese PoE.

%12
\subsection{Problemi}
\begin{itemize}
 \item \ul{consumo di energia}: un bridge PoE consuma molta più energia elettrica (ad es. 48 porte a 25 W l'una consumano 1,2 kW) ed è più costoso di un bridge tradizionale;
 \item \ul{continuità del servizio}: un guasto del bridge PoE o l'interruzione dell'energia elettrica fanno smettere di funzionare i telefoni, che sono invece un servizio importante in caso di emergenza $\Rightarrow$ è necessario installare dei gruppi di continuità (UPS, dall'inglese ``uninterruptible power supply'') ma, invece di fornire energia elettrica solamente ai telefoni tradizionali, devono fornire energia elettrica a tutta l'infrastruttura dati.
\end{itemize}