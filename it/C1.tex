\chapter{Qualità del servizio nelle LAN IEEE 802}
%3
La \textbf{qualità del servizio} nell'inoltro del traffico è richiesta quando c'è una limitata quantità di risorse tale che il traffico offerto eccede la capacità di smaltimento dei dati creando delle congestioni.

%4
Di solito le LAN sono sovrabbondanti, in quanto è molto più economico espandere la rete che imporre la qualità del servizio $\Rightarrow$ nel caso peggiore, l'occupazione del canale è pari al 30-40\% della banda disponibile $\Rightarrow$ apparentemente non c'è alcun bisogno della qualità del servizio perché non ci sono congestioni.

%5-6
In alcuni scenari possibili si potrebbero avere dei problemi:
\begin{figure}
\centering
	\begin{subfigure}[b]{.3\textwidth}
		\centering
		\includegraphics[width=.8\linewidth]{../pic/C1/5a}
		\caption{Backbone non ben dimensionato.}
	\end{subfigure}
\hspace{0.03\textwidth}
	\begin{subfigure}[b]{.3\textwidth}
		\centering
		\includegraphics[width=.9\linewidth]{../pic/C1/5b}
		\caption{Trasferimento dati da diversi client a un singolo server.}
	\end{subfigure}
\hspace{0.03\textwidth}
	\begin{subfigure}[b]{.3\textwidth}
		\centering
		\includegraphics[width=.9\linewidth]{../pic/C1/5c}
		\caption{Trasferimento dati da un server veloce a un client lento.}
	\end{subfigure}
\end{figure}
\begin{enumerate}[(a)]
 \item un bridge dotato di buffer troppo piccoli nel backbone può portare a \textbf{micro-congestioni} sugli uplink, le quali non sono persistenti ma sono tante e di breve durata (micro) perché il traffico dei client è estremamente bursty;
 \item un bridge dotato di buffer troppo piccoli come singolo punto di accesso a un server può portare a \textbf{congestioni persistenti} dovute alla concorrenza di tanti client collegati al server allo stesso tempo;
 \item un client lento (in termini di velocità del link, capacità della CPU, ecc.) può portare a \textbf{congestioni temporanee} sul client stesso perché non riesce a smaltire il traffico proveniente da un server veloce.
\end{enumerate}
\FloatBarrier

%33-34
La qualità del servizio è potenzialmente una bella funzionalità, ma presenta delle controindicazioni che ne rendono non così forte la necessità: la qualità del servizio è solo uno dei problemi da risolvere per rendere la rete efficiente, e i miglioramenti che essa porta spesso non sono percepiti dall'utente finale.

\section{IEEE 802.1p}
\label{sez:ieee_8021p}
%14-21
Lo standard \textbf{IEEE 802.1p} definisce 8 classi di servizio, chiamate \textbf{livelli di priorità}, e a ognuna di esse è assegnata una diversa coda (logica).

%15-18
Una trama può essere contrassegnato con una specifica classe di servizio nel campo ``Priority Code Point'' (PCP) del tag VLAN (si rimanda alla sezione~\ref{sez:frame_tagging}).\footnote{Esistono due campi di marcatura per la qualità del servizio, uno a livello data-link e l'altro a livello rete:
\begin{itemize}
 \item il campo ``Priority Code Point'' (PCP), usato dallo standard IEEE 802.1p, sta nell'intestazione della trama Ethernet;
 \item il campo ``Differentiated Service Code Point'' (DSCP), usato dall'architettura Differentiated Services (DiffServ), sta nell'intestazione del pacchetto IP, in particolare nel campo ``Type of Service'' dell'intestazione IPv4 e nel campo ``Priority'' dell'intestazione IPv6.
\end{itemize}
} Lo standard offre anche la possibilità di selezionare l'algoritmo di scheduling a priorità desiderato: round robin, weighted round robin, weighted fair queuing.

%19-20
Sarebbe meglio lasciare che la sorgente, a livello applicazione, effettui la marcatura perché solo la sorgente conosce esattamente il tipo di traffico (traffico voce o traffico dati), ma quasi tutti gli utenti dichiarerebbero tutti i pacchetti come ad alta priorità perché non sarebbero onesti $\Rightarrow$ la marcatura va eseguita dai bridge di accesso che sono sotto il controllo del provider. Tuttavia il riconoscimento del tipo di traffico è molto difficile per i bridge e li rende molto costosi, perché richiede di salire al livello applicazione e può non funzionare con il traffico criptato $\Rightarrow$ si può semplificare la distinzione per i bridge in due modi:
\begin{itemize}
 \item \ul{marcatura per porta}: il PC è connesso a una porta e il telefono a un'altra porta, così il bridge può marcare il traffico in base alla porta di ingresso;
 \item \ul{marcatura per dispositivo edge}: il PC è connesso al telefono e il telefono al bridge $\Rightarrow$ tutto il traffico del PC passa per il telefono, che semplicemente lo marca come traffico dati, mentre marca il suo traffico come traffico voce.
\end{itemize}

%16-34
Lo standard suggerisce a quale tipo di traffico è destinato ogni livello di priorità (ad es. 6 = traffico voce), ma lascia la libertà di cambiare queste associazioni $\Rightarrow$ possono sorgere dei problemi di interoperabilità tra fornitori diversi.

\section{IEEE 802.3x}
\label{sez:PAUSE}
%23-26
Lo standard \textbf{802.3x} implementa un \textbf{controllo di flusso} a livello Ethernet, in aggiunta al controllo di flusso esistente a livello TCP: dato un link, se il nodo (bridge o host) a valle ha i buffer pieni può inviare al nodo a monte all'altra estremità del link un \textbf{pacchetto PAUSE} chiedendo ad esso di interrompere la trasmissione dei dati \ul{su quel link} per una certa quantità di tempo, detta \textbf{tempo di pausa} che è espresso in ``quanti di pausa'' (1 quanto = tempo per trasmettere 512 bit). Il nodo a monte quindi memorizza i pacchetti che arrivano durante il tempo di pausa nel suo buffer di uscita, e li invierà quando il buffer di ingresso del nodo a valle sarà pronto a ricevere altri pacchetti $\Rightarrow$ i pacchetti non vanno più persi per colpa della congestione dei buffer.

%27
Esistono due \textbf{modalità di controllo di flusso}:
\begin{itemize}
 \item \ul{modalità asimmetrica}: solo un nodo invia il pacchetto PAUSE, l'altro si limita a ricevere il pacchetto e a interrompere la trasmissione;
 \item \ul{modalità simmetrica}: entrambi i nodi alle estremità del link possono trasmettere e ricevere pacchetti PAUSE.
\end{itemize}

Su ogni nodo si può configurare la modalità di controllo di flusso, ma la fase di autonegoziazione deve determinare l'effettiva configurazione in modo che la modalità scelta sia coerente su entrambi i nodi alle estremità del link.

\begin{figure}
	\centering
	\includegraphics[width=0.66\linewidth]{../pic/C1/30}
	\caption{L'invio di pacchetti PAUSE può essere problematico nel backbone.}
\end{figure}

%29-30-31-32
L'invio di pacchetti PAUSE può essere problematico nel backbone: un bridge con i buffer pieni è in grado di far interrompere il traffico solo sul link a cui è direttamente collegato ma, se i bridge intermedi nel percorso di upstream non sentono la necessità di mandare a loro volta dei pacchetti PAUSE perché dotati di buffer più grandi, non è in grado di ``zittire'' l'host che sta inviando troppi pacchetti $\Rightarrow$ finché il bridge di accesso non invierà a sua volta un pacchetto PAUSE all'host interessato, la rete appare bloccata anche a tutti gli altri host che non sono responsabili del problema $\Rightarrow$ i pacchetti PAUSE inviati da bridge non di accesso non hanno la capacità di \ul{selezionare il traffico in eccesso} per rallentare l'host responsabile, ma hanno effetto sul traffico di tutti gli host.
\FloatBarrier

Per questo motivo, è consigliabile disabilitare il controllo di flusso nel backbone e utilizzare i pacchetti PAUSE solo tra i bridge di accesso e gli host. Spesso viene scelta la modalità di controllo di flusso asimmetrica, dove solo gli host possono mandare i pacchetti PAUSE: in genere i buffer dei bridge di accesso sono sufficientemente grandi, e diversi bridge commerciali accettano pacchetti PAUSE dagli host, bloccando la trasmissione dati sulla porta interessata, ma non possono inviarli.

%28
Tuttavia l'invio dei pacchetti PAUSE può essere problematico anche per gli host, in quanto può scatenare un \textbf{livelock} nel kernel del sistema operativo: la CPU dell'host lento è così impegnata ad elaborare i pacchetti in arrivo sull'interfaccia NIC che non riesce a trovare un momento per mandare un pacchetto PAUSE $\Rightarrow$ i pacchetti si accumulano in RAM portandola alla saturazione.