%Note: References are to 2013 slides.
\chapter{Ridondanza e bilanciamento del carico a livello 3 nelle LAN}
%3
Ai confini della LAN aziendale con il livello rete, il router che fornisce la connettività con l'esterno (tipicamente Internet) costituisce, per gli host che lo hanno come loro default gateway, un singolo punto di guasto, a meno che il router non sia ridondato opportunamente.

%4
La semplice duplicazione del router non è sufficiente: gli host non sono in grado di passare automaticamente all'altro router in caso di guasto del loro default gateway, perché essi non sono capaci ad apprendere la topologia di rete tramite i protocolli di instradamento del livello rete.

%5
Sono stati pertanto definiti alcuni protocolli per la gestione automatica di router ridondanti:
\begin{itemize}
 %6
 \item HSRP: protocollo proprietario di Cisco specifico per la \textbf{ridondanza del default gateway} e con parziale supporto al bilanciamento del carico (sezione~\ref{sez:hsrp});
 \item VRRP: protocollo standard molto simile a HSRP ma libero da brevetti;
 \item GLBP: protocollo proprietario di Cisco che migliora il \textbf{bilanciamento del carico} rispetto a HSRP (sezione~\ref{sez:glbp}).
\end{itemize}

\section{HSRP}
\label{sez:hsrp}
%7
\afterpage{
\begin{landscape}
\begin{figure}[p]
	\centering
	\includegraphics[width=0.8\linewidth]{../pic/D4/7}
	\caption{Esempio di rete aziendale con tre router ridondanti grazie all'HSRP.}
\end{figure}
\end{landscape}
}

L'\textbf{Hot Standby Routing Protocol} (HSRP) garantisce automaticamente che ogni host mantenga la connettività con l'esterno della LAN attraverso il proprio default gateway anche in caso di guasto di uno dei router ridondanti.

\subsection{Configurazione della rete}
%8-9
Alle interfacce appartenenti alla LAN aziendale di tutti i router ridondanti viene assegnato uno stesso \textbf{indirizzo IP virtuale} e uno stesso \textbf{indirizzo MAC virtuale}, in aggiunta ai loro indirizzi IP e MAC reali. I router possono essere:
\begin{itemize}
 \item \textbf{active}: è il router che ha il diritto di servire la LAN, cioè di rispondere all'indirizzo IP virtuale e all'indirizzo MAC virtuale;
 \item \textbf{stand-by}: è il router che ha il diritto di sostituire il router active in caso di guasto di quest'ultimo;
 \item \textbf{listen}: sono gli altri router né active né stand-by; uno di essi diventa il router stand-by in caso di guasto del router active.
\end{itemize}

%11-12
L'indirizzo IP virtuale va impostato esplicitamente dall'amministratore di rete durante la configurazione di HSRP, mentre l'indirizzo MAC virtuale ha il prefisso well-known di Cisco ``00:00:0C:07:AC'':
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|}
  \multicolumn{1}{r}{\footnotesize{24}} & \multicolumn{1}{r}{\footnotesize{40}} & \multicolumn{1}{r}{\footnotesize{48}} \\
  \hline
  OUI (00:00:0C) & stringa HSRP (07:AC) & ID di gruppo \\
  \hline
 \end{tabular}}
 \caption{Formato dell'indirizzo MAC virtuale HSRP.}
\end{table}
\noindent
dove i campi sono:
\begin{itemize}
 \item campo \ul{Organizationally Unique Identifier} (OUI) (3 byte): la stringa di bit ``00:00:0C'' è l'OUI assegnato a Cisco affinché gli indirizzi MAC delle schede di rete vendute da Cisco siano globalmente univoci;
 \item campo \ul{stringa HSRP} (2 byte): la stringa di bit ``07:AC'' identifica un indirizzo MAC virtuale HSRP, e non può comparire in alcun indirizzo MAC fisico $\Rightarrow$ l'indirizzo MAC virtuale è garantito essere univoco all'interno della LAN: non è possibile che un host abbia un indirizzo MAC uguale all'indirizzo MAC virtuale HSRP;
 \item campo \ul{ID di gruppo} (1 byte): identifica il gruppo a cui fa riferimento l'istanza di HSRP corrente (si rimanda alla sezione~\ref{sez:gruppi_hsrp}).
\end{itemize}

A tutti gli host viene impostato l'indirizzo IP virtuale come indirizzo di default gateway, cioè come indirizzo IP a cui gli host invieranno i pacchetti IP diretti al di fuori della LAN.

\subsection{Instradamento asimmetrico del traffico}
%14
L'obiettivo dell'HSRP è ``ingannare'' l'host facendo credere ad esso di stare comunicando con l'esterno attraverso un singolo router caratterizzato da un indirizzo IP pari all'indirizzo di default gateway e da un indirizzo MAC pari all'indirizzo MAC ottenuto tramite il protocollo ARP, mentre in realtà l'HSRP in caso di guasto sposta il router active su un altro router senza che l'host se ne accorga:
\begin{enumerate}
 \item \ul{ARP Request}: quando un host si connette alla rete, invia una ARP Request all'indirizzo IP impostato come default gateway, che è l'indirizzo IP virtuale;\footnote{Si ricorda che la ARP Request è una trama di livello data-link con indirizzo MAC di destinazione broadcast e con l'indirizzo IP nel payload.}
 \item \ul{ARP Reply}: il router active invia in risposta una ARP Reply con il proprio indirizzo MAC virtuale;
 %35
 \item \ul{traffico in uscita}: l'host invia ogni pacchetto successivo all'\ul{indirizzo MAC virtuale}, e solo il router active lo elabora, mentre i router stand-by e listen lo scartano.\\
 Poi, il router active inoltra il pacchetto secondo i protocolli di instradamento esterni (OSPF, BGP, ecc.) che sono indipendenti dall'HSRP $\Rightarrow$ il pacchetto potrebbe anche attraversare i router stand-by e listen se i protocolli di instradamento ritengono che questo sia il percorso migliore;
 %32-33-34
 \item \ul{traffico in entrata}: ogni pacchetto proveniente dall'esterno e diretto all'host può entrare nella LAN da uno qualsiasi dei router ridondanti secondo i protocolli di instradamento esterni indipendenti dall'HSRP, e l'host lo riceverà con l'\ul{indirizzo MAC reale} del router come indirizzo MAC sorgente.\\
 I protocolli di instradamento esterni sono anche in grado di rilevare i guasti dei router, compreso il default gateway, per il traffico in entrata $\Rightarrow$ la protezione è ottenuta anche se la LAN è priva dell'HSRP.
\end{enumerate}

%31
\subsubsection{Server dual-homed}
L'HSRP può essere utilizzato per ottenere la ridondanza di una macchina singola per migliorare la tolleranza ai guasti: un server può essere dotato di due interfacce di rete, una primaria e una secondaria, a cui l'HSRP assegna un indirizzo IP virtuale e un indirizzo MAC virtuale $\Rightarrow$ il server continuerà a essere raggiungibile allo stesso indirizzo IP anche in caso di guasto del link che collega l'interfaccia primaria alla rete.

\subsection{Pacchetti di Hello}
%6-10
I \textbf{pacchetti di Hello} sono dei messaggi generati dai router ridondanti per:
\begin{itemize}
 \item \ul{eleggere il router active}: nella fase di negoziazione, i router si scambiano dei pacchetti di Hello proponendosi come router active $\Rightarrow$ il router active è quello con la priorità più alta (configurabile dall'amministratore di rete), o in caso di parità quello con l'indirizzo IP più alto;
 %26
 \item \ul{rilevare i guasti del router active}: il router active invia periodicamente pacchetti di Hello come messaggi di ``keep-alive'' $\Rightarrow$ in caso di guasto del router active, il router stand-by non riceve più il messaggio di ``keep-alive'' ed elegge se stesso come router active;
 %15
 \item \ul{aggiornare i filtering database}: quando il router active cambia, il nuovo router active inizia a inviare dei messaggi di Hello segnalando ai bridge all'interno della LAN aziendale la nuova posizione dell'indirizzo MAC virtuale $\Rightarrow$ tutti i bridge aggiorneranno i loro filtering database conformemente.\\
 %16
 Quando un router diventa active, invia anche una ARP Reply gratuita in broadcast (le ARP Reply normali sono unicast) con l'indirizzo MAC virtuale come indirizzo MAC sorgente.
\end{itemize}

%24
Nel pacchetto di Hello l'intestazione HSRP è incapsulata nel formato seguente:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|rl|rl|rl|c|}
 \multicolumn{2}{c}{\footnotesize{14 byte}} & \multicolumn{2}{c}{\footnotesize{20 byte}} & \multicolumn{2}{c}{\footnotesize{8 byte}} & \multicolumn{1}{c}{\footnotesize{20 byte}} \\
 \hline
 \multicolumn{2}{|c|}{intestazione MAC} & \multicolumn{2}{|c|}{intestazione IP} & \multicolumn{2}{|c|}{intestazione UDP} & \multirow{4}{*}{\begin{tabular}[t]{@{}c@{}}intestazione\\HSRP\end{tabular}} \\
 \begin{tabular}[t]{@{}r@{}}\footnotesize{src:}\end{tabular} & \footnotesize{indirizzo MAC virtuale} & \begin{tabular}[t]{@{}r@{}}\footnotesize{src:}\end{tabular} & \footnotesize{indirizzo IP reale} & \begin{tabular}[t]{@{}r@{}}\footnotesize{src:}\end{tabular} & \footnotesize{porta 1985} & \\
 \begin{tabular}[t]{@{}r@{}}\footnotesize{dst:}\end{tabular} & \footnotesize{01:00:5E:00:00:02} & \begin{tabular}[t]{@{}r@{}}\footnotesize{dst:}\end{tabular} & \footnotesize{224.0.0.2} & \begin{tabular}[t]{@{}r@{}}\footnotesize{dst:}\end{tabular} & \footnotesize{porta 1985} & \\
 & & \begin{tabular}[t]{@{}r@{}}\footnotesize{TTL:}\end{tabular} & \footnotesize{1} & & & \\
 \hline
 \end{tabular}}
 \caption{Formato del pacchetto di Hello HSRP generato dal router active.}
\end{table}

\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|rl|rl|rl|c|}
 \multicolumn{2}{c}{\footnotesize{14 byte}} & \multicolumn{2}{c}{\footnotesize{20 byte}} & \multicolumn{2}{c}{\footnotesize{8 byte}} & \multicolumn{1}{c}{\footnotesize{20 byte}} \\
 \hline
 \multicolumn{2}{|c|}{intestazione MAC} & \multicolumn{2}{|c|}{intestazione IP} & \multicolumn{2}{|c|}{intestazione UDP} & \multirow{4}{*}{\begin{tabular}[t]{@{}c@{}}intestazione\\HSRP\end{tabular}} \\
 \begin{tabular}[t]{@{}r@{}}\footnotesize{src:}\end{tabular} & \footnotesize{indirizzo MAC reale} & \begin{tabular}[t]{@{}r@{}}\footnotesize{src:}\end{tabular} & \footnotesize{indirizzo IP reale} & \begin{tabular}[t]{@{}r@{}}\footnotesize{src:}\end{tabular} & \footnotesize{porta 1985} & \\
 \begin{tabular}[t]{@{}r@{}}\footnotesize{dst:}\end{tabular} & \footnotesize{01:00:5E:00:00:02} & \begin{tabular}[t]{@{}r@{}}\footnotesize{dst:}\end{tabular} & \footnotesize{224.0.0.2} & \begin{tabular}[t]{@{}r@{}}\footnotesize{dst:}\end{tabular} & \footnotesize{porta 1985} & \\
 & & \begin{tabular}[t]{@{}r@{}}\footnotesize{TTL:}\end{tabular} & \footnotesize{1} & & & \\
 \hline
 \end{tabular}}
 \caption{Formato del pacchetto di Hello HSRP generato dal router stand-by.}
\end{table}

%25
\paragraph{Osservazioni}
\begin{itemize}
\item \ul{indirizzo MAC sorgente}: è l'indirizzo MAC virtuale per il router active, è l'indirizzo MAC reale per il router stand-by;
\item \ul{indirizzo IP di destinazione}: ``224.0.0.2'' è l'indirizzo IP del gruppo multicast ``all routers''; è uno degli indirizzi multicast non filtrati da IGMP snooping e quindi mandati sempre in flooding dai bridge\footnote{Si veda il capitolo~\ref{cap:igmp_snooping}.};
\item \ul{indirizzo MAC di destinazione}: ``01:00:5E:00:00:02'' è l'indirizzo MAC multicast derivato dall'indirizzo IP multicast;
\item \ul{Time To Live} (TTL): è pari a 1 in modo che i pacchetti vengano scartati subito dai router a cui giungono, perché essi possono essere propagati solo per la LAN;
\item l'intestazione HSRP del pacchetto di Hello è incapsulata in UDP e non in TCP perché la perdita di un pacchetto di Hello non richiede la sua ritrasmissione;
\item i router listen non generano pacchetti di Hello, a meno che non rilevano che il router stand-by è diventato router active e devono candidarsi affinché uno di essi diventi il nuovo router stand-by.
\end{itemize}

\subsubsection{Formato dell'intestazione HSRP}
%17
L'intestazione HSRP ha il formato seguente:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|}
  \multicolumn{1}{r}{\footnotesize{8}} & \multicolumn{1}{r}{\footnotesize{16}} & \multicolumn{1}{r}{\footnotesize{24}} & \multicolumn{1}{r}{\footnotesize{32}} \\
  \hline
  Version & Op Code & State & Hello Time \\
  \hline
  Hold Time & Priority & Group & Reserved \\
  \hline
  \multicolumn{4}{|c}{Authentication} \\
  \hdashline
  \multicolumn{4}{c|}{Data} \\
  \hline
  \multicolumn{4}{|c|}{Virtual IP Address} \\
  \hline
 \end{tabular}}
 \caption{Formato dell'intestazione HSRP (20 byte).}
\end{table}
\noindent
dove i campi più significativi sono:
\begin{itemize}
 %18
 \item campo \ul{Op Code} (1 byte): descrive il tipo di messaggio contenuto nel pacchetto di Hello:
 \begin{itemize}
  \item[0 =] Hello: il router è in esecuzione ed è capace di diventare il router active o stand-by;
  \item[1 =] Coup: il router vuole diventare il router active;
  \item[2 =] Resign: il router non vuole più essere il router active;
 \end{itemize}

 %19-20
 \item campo \ul{State} (1 byte): descrive lo stato corrente del router mittente del messaggio:
 \begin{itemize}
  \item[8 =] Standby: il pacchetto HSRP è stato inviato dal router stand-by;
  \item[16 =] Active: il pacchetto HSRP è stato inviato dal router active;
 \end{itemize}
 
 %21
 \item campo \ul{Hello Time} (1 byte): è il tempo tra i messaggi di Hello inviati dai router (predefinito: 3 s);
 \item campo \ul{Hold Time} (1 byte): è il tempo di validità del messaggio di Hello corrente, scaduto il quale il router stand-by si propone come router active (predefinito: 10 s);
 
 %22
 \item campo \ul{Priority} (1 byte): è la priorità del router usata per il processo di elezione del router active/stand-by (predefinita: 100);
 \item campo \ul{Group} (1 byte): identifica il gruppo a cui fa riferimento l'istanza di HSRP corrente (si rimanda alla sezione~\ref{sez:gruppi_hsrp});
 
 %23
 \item campo \ul{Authentication Data} (8 byte): contiene una password da 8 caratteri in chiaro (predefinita: ``cisco'');
 \item campo \ul{Virtual IP Address} (4 byte): è l'indirizzo IP virtuale utilizzato dal gruppo, cioè l'indirizzo IP utilizzato come indirizzo di default gateway dagli host della LAN aziendale.
\end{itemize}

%44
Con i valori predefiniti per i parametri di Hello Time e di Hold Time, il tempo di convergenza è pari a circa 10 secondi.

\subsection{Gruppi HSRP}
%29
\label{sez:gruppi_hsrp}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{../pic/D4/29}
	\caption{Esempio di rete con più gruppi HSRP.}
	\label{fig:gruppi_hsrp}
\end{figure}

\noindent
I \textbf{gruppi HSRP} permettono di distinguere più reti IP logiche nella stessa LAN fisica: a ogni rete IP corrisponde un gruppo HSRP, con una coppia indirizzo MAC virtuale e indirizzo IP virtuale. Gli host di una rete IP hanno uno degli indirizzi IP virtuali impostato come indirizzo di default gateway, gli host di un'altra rete IP hanno un altro indirizzo IP virtuale impostato come indirizzo di default gateway, e così via.

Ogni router ridondante conosce più coppie indirizzo MAC virtuale e indirizzo IP virtuale, una per ogni gruppo $\Rightarrow$ ogni router (tranne i router listen) genera un pacchetto di Hello per ogni gruppo, e risponde a uno degli indirizzi MAC virtuali alla ricezione di traffico da host di una rete IP, a un altro alla ricezione di traffico da host di un'altra rete IP, e così via.

Gli ultimi 8 bit dell'indirizzo MAC virtuale identificano il gruppo a cui l'indirizzo fa riferimento $\Rightarrow$ l'HSRP è in grado di gestire fino a 256 gruppi diversi in una stessa LAN.
\FloatBarrier

%30
\subsubsection{In presenza di VLAN}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{../pic/D4/30}
	\caption{Esempio di rete con più gruppi HSRP in presenza di VLAN.}
	\label{fig:gruppi_hsrp_2}
\end{figure}

\noindent
Definire più gruppi HSRP è obbligatorio in presenza di VLAN: ogni VLAN è infatti una LAN separata con il proprio default gateway $\Rightarrow$ a ogni VLAN è assegnato un gruppo HSRP. Ogni router a braccio singolo\footnote{Si veda la sezione~\ref{sez:onearm_router}.} ridondante ha più interfacce virtuali\footnote{Si veda la sezione~\ref{sez:link_bridge_server}.}, una per ogni VLAN $\Rightarrow$ i gruppi HSRP sono configurati sulla stessa interfaccia fisica ma ciascuno su interfacce logiche differenti.
\FloatBarrier

%36-37-38
\subsubsection{Multi-group HSRP (mHSRP)}
Tramite un'opportuna configurazione delle priorità, è possibile distribuire il traffico delle reti IP sui router ridondanti (\textbf{condivisione del carico}):
\begin{itemize}
 \item figura~\ref{fig:gruppi_hsrp}: il traffico della rete IP 1 passa per il router R2, mentre il traffico della rete IP 2 passa per il router R1;
 \item figura~\ref{fig:gruppi_hsrp_2}: il traffico della VLAN 1 passa per il router R2, mentre il traffico della VLAN 2 passa per il router R1.
\end{itemize}

\paragraph{Vantaggi}
\begin{itemize}
 \item il mHSRP è molto conveniente quando il \ul{traffico in entrata} nella LAN è \ul{simmetrico}: un router a braccio singolo per l'interconnessione di VLAN può essere ridondato in modo che un router sostiene il traffico in entrata da una prima VLAN e in uscita in una seconda VLAN, mentre l'altro router sostiene il traffico in entrata dalla seconda VLAN e in uscita nella prima VLAN;
 \item \ul{migliore utilizzo delle risorse}: in una rete con un singolo gruppo HSRP la larghezza di banda del router stand-by è del tutto inutilizzata $\Rightarrow$ il mHSRP permette di utilizzare la larghezza di banda di entrambi i router.
\end{itemize}

\paragraph{Svantaggi}
\begin{itemize}
 \item il mHSRP non è così conveniente quando il \ul{traffico in entrata} nella LAN è \ul{asimmetrico}: la condivisione del carico infatti riguarda solo il traffico in uscita (il traffico in entrata è indipendente dall'HSRP), e il traffico in uscita (upload) generalmente è inferiore rispetto al traffico in entrata (download);
 \item la condivisione del carico non implica necessariamente il \ul{bilanciamento del traffico}: il traffico proveniente da una LAN potrebbe essere molto maggiore del traffico proveniente da un'altra LAN;
 \item \ul{difficoltà di configurazione}: gli host in ogni rete IP devono avere un indirizzo di default gateway diverso rispetto agli host delle altre reti IP, ma il server DHCP solitamente restituisce un singolo indirizzo di default gateway per tutti gli host.
\end{itemize}

\subsection{Funzione di track}
%41
L'HSRP offre protezione dai guasti del link che collega il router default gateway alla LAN e dai guasti del router default gateway stesso, ma non dai guasti del link che collega il router default gateway a Internet: un guasto sul link WAN infatti obbliga i pacchetti a essere inviati al router active che a sua volta li invia tutti al router stand-by, invece di andare subito al router stand-by $\Rightarrow$ ciò non comporta una reale perdita di connettività Internet, ma comporta un overhead aggiuntivo nel processo di inoltro dei pacchetti.

La \textbf{funzione di track} consente di rilevare i guasti sui link WAN e di scatenare il router stand-by a prendere il posto del router active tramite una diminuzione automatica della priorità del router active (predefinito: \textminus 10).

%43
La funzione di track funziona solo se è attiva la \textbf{capacità di prelazione}: se la diminuzione della priorità del router active è tale da portarla al di sotto della priorità del router stand-by, quest'ultimo può ``sottrarre'' lo stato di active al router active inviando un messaggio di Hello di tipo Coup.

%42
Tuttavia, il rilevamento dei guasti avviene esclusivamente a livello fisico: la funzione di track non è in grado di rilevare un guasto avvenuto su un link più lontano al di là di un bridge.

\subsection{Problemi}
%47-48
\subsubsection{Resilienza del livello data-link}
\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{../pic/D4/47}
	\caption{Esempio di guasto interno alla rete di livello data-link.}
	\label{fig:resilienza_l2}
\end{figure}

\noindent
L'HSRP non protegge da tutti i guasti della rete di livello data-link. Per esempio, il guasto in figura~\ref{fig:resilienza_l2} partiziona la rete aziendale in due parti, e siccome avviene tra due bridge non può essere rilevato dai router a livello fisico. Il router stand-by non riceve più i messaggi di Hello dal router active e si auto-promuove active $\Rightarrow$ il \ul{traffico in uscita} non viene influenzato per nulla dal verificarsi del guasto: ognuno dei due router serve il traffico in uscita di una delle due porzioni di rete.

Il guasto ha invece impatto sul \ul{traffico in entrata}, in quanto alcune trame non possono raggiungere gli host di destinazione. I protocolli di instradamento di livello rete infatti operano esclusivamente tra router e router: si limitano a rilevare che il percorso tra i due router è stato spezzato da qualche parte, ma non sono in grado di rilevare le interruzioni di percorso tra un router e un host, perché il loro compito è inoltrare il pacchetto in modo che raggiunga uno qualsiasi dei router di frontiera, a cui poi spetta la consegna diretta della trama alla destinazione finale. Visti dall'esterno, entrambi i router appaiono avere la connettività alla stessa rete IP, perciò i protocolli di instradamento di livello rete assumeranno che tutti gli host appartenenti a quella rete IP siano raggiungibili da entrambe le interfacce e ne sceglieranno una in base al criterio del percorso più breve:
\begin{itemize}
\item se viene scelto il router che serve la porzione di rete a cui appartiene la destinazione, la trama è consegnata a destinazione senza problemi;
\item se viene scelto il router che serve l'altra porzione di rete, il router effettua una ARP Request a cui nessun host risponderà e perciò la destinazione apparirà inesistente sulla rete.
\end{itemize}

È quindi importante ridondare tutti i link interni alla rete di livello data-link, mettendo più link in parallelo gestiti dal protocollo di spanning tree o configurati in link aggregation.
\FloatBarrier

%49-50-51-52-53
\subsubsection{Flooding}
\begin{figure}
	\centering
	\includegraphics[width=0.35\linewidth]{../pic/D4/50}
	\caption{Esempio di topologia di rete affetta da flooding periodico.}
	\label{fig:flooding_hsrp}
\end{figure}

\noindent
In alcune topologie di rete, l'instradamento asimmetrico del traffico può far sì che in alcuni periodi di tempo aumenti in maniera considerevole il traffico in entrata dall'esterno mandato in flooding, mentre in altri il traffico in entrata venga inoltrato correttamente dai bridge. Ciò è dovuto al fatto che le ARP cache dei router generalmente durano più a lungo dei filtering database dei bridge.

Per esempio, in figura~\ref{fig:flooding_hsrp} i mapping nella ARP cache sul router ingress R2 scadono dopo 5 minuti, mentre le entry nel filtering database del bridge B2 scadono dopo soli 2 minuti:
\begin{enumerate}
 \item il pacchetto unicast in uscita aggiorna solo il filtering database del bridge B1, perché non passa attraverso il bridge B2;
 \item il pacchetto in entrata scatena l'invio da parte del router R2 di una ARP Request all'host H1 (in broadcast);
 \item la ARP Reply che l'host H1 invia in risposta aggiorna sia la ARP cache del router R2 sia il filtering database del bridge B2;
 \item per i primi 2 minuti, i pacchetti in entrata diretti all'host H1 vengono inoltrati senza problemi;
 \item dopo 2 minuti dalla ARP Reply, scade la entry relativa all'host H1 nel filtering database del bridge B2;
 \item per i successivi 3 minuti, il router R2, che ha ancora un mapping valido per l'host H1 nella sua ARP cache, invia i pacchetti in entrata verso il bridge B2, il quale li manda tutti in flooding perché non riceve alcuna trama avente l'host H1 come sorgente;
 \item dopo 5 minuti dalla ARP Reply, scade anche il mapping nella ARP cache del router R2;
 \item la successiva ARP Reply sollecitata dal router R2 finalmente aggiorna anche il filtering database del bridge R2.
\end{enumerate}

\paragraph{Soluzioni possibili} Non è facile identificare questo problema nella rete perché si manifesta in maniera intermittente; una volta identificato il problema, è possibile:
\begin{itemize}
 \item forzare le stazioni a inviare trame in broadcast gratuite più spesso, con una frequenza minore dell'ageing time delle entry nei filtering database dei bridge;
 \item aumentare il valore di ageing time sui bridge lungo il percorso ingress ad almeno il tempo di durata delle ARP cache dei router.
\end{itemize}
\FloatBarrier

%54-55
\subsubsection{Link unidirezionali}
\begin{figure}[H]%WARNING
	\centering
	\includegraphics[width=0.45\linewidth]{../pic/D4/54}
	\caption{Esempio di guasto su un link unidirezionale.}
	\label{fig:unidirezionali_hsrp}
\end{figure}

\noindent
L'HSRP non prevede la gestione di guasti su link unidirezionali: per esempio, in figura~\ref{fig:unidirezionali_hsrp} avviene un guasto verso il router stand-by, il quale non riceve più i messaggi di Hello dal router active e si auto-elegge active, iniziando a inviare pacchetti di Hello con l'indirizzo MAC virtuale come indirizzo sorgente $\Rightarrow$ il bridge riceve alternativamente pacchetti di Hello da entrambi i router active aventi lo stesso indirizzo MAC come indirizzo sorgente, e la entry relativa a quell'indirizzo MAC continuerà a oscillare periodicamente $\Rightarrow$ se un host invia una trama al default gateway mentre la entry nel filtering database del bridge è associata all'ex-router stand-by, la trama, non potendo passare per il link unidirezionale guasto, andrà persa.
%\FloatBarrier

%67-68-69-70
\section[GLBP]{GLBP\footnote{Questa sezione contiene contenuti CC BY-SA dalla voce \href{https://en.wikipedia.org/wiki/Gateway_Load_Balancing_Protocol}{Gateway Load Balancing Protocol} su Wikipedia in inglese.}}
\label{sez:glbp}
\afterpage{
\begin{landscape}
\begin{figure}[p]
	\centering
	\includegraphics[width=0.85\linewidth]{../pic/D4/68}
	\caption{Esempio di rete aziendale con tre router ridondanti grazie al GLBP.}
\end{figure}
\end{landscape}
}

Il \textbf{Gateway Load Balancing Protocol} (GLBP) aggiunge alla ridondanza del default gateway la possibilità di distribuire automaticamente il traffico in uscita su tutti i router ridondanti.

Il GLBP elegge un Active Virtual Gateway (AVG) per ogni gruppo; gli altri membri del gruppo, chiamati Actual Virtual Forwarder (AVF), fungono da backup in caso di guasto dell'\mbox{AVG}. L'AVG eletto assegna quindi un indirizzo MAC virtuale a ogni membro del gruppo GLBP, compreso se stesso; ogni AVF si assume la responsabilità di inoltrare i pacchetti inviati al suo indirizzo MAC virtuale.

In caso di guasto di un AVF, l'AVG notifica uno degli AVF rimasti attivi affidando ad esso il compito di rispondere anche al traffico diretto verso l'indirizzo MAC virtuale dell'AVF guasto.

%71
L'AVG risponde alle ARP Request inviate dagli host con indirizzi MAC che puntano a router diversi, in base a uno dei seguenti algoritmi di bilanciamento del carico:
\begin{itemize}
 \item nessuno: l'AVG è l'unico forwarder (come nell'HSRP);
 \item \ul{weighted}: a ogni router è assegnato un peso, che determina la percentuale di ARP Request risposte con l'indirizzo MAC virtuale di quel router, e quindi la percentuale di host che useranno quel router come forwarder $\Rightarrow$ utile quando i link di uscita hanno differenti capacità;
 \item \ul{round robin}: gli indirizzi MAC virtuali sono selezionati sequenzialmente in una coda circolare;
 \item \ul{host dependent}: garantisce che un host rimanga associato sempre allo stesso forwarder, cioè se l'host effettua due ARP Request riceverà due ARP Reply con lo stesso indirizzo MAC virtuale $\Rightarrow$ ciò evita problemi con i meccanismi di traduzione degli indirizzi dei NAT.
\end{itemize}