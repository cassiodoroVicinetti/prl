%Note: References are to 2013 slides.
\chapter{Rapid Spanning Tree Protocol}
\label{cap:rapid_stp}
%4
\textbf{Rapid Spanning Tree Protocol} (RSTP), standardized as IEEE 802.1w (2001), is characterized by a greater convergence speed with respect to STP in terms of:
\begin{itemize}
 \item \ul{spanning tree recomputation} (section~\ref{sez:convergenza_topologia})
 \item \ul{filtering database update} (section~\ref{sez:aggiornamento_filtering})
\end{itemize}

%6-7-9-12
\section{Port roles and states}
RSTP defines new port roles and states:
\begin{itemize}
 \item \textbf{discarding} state: the port does not forward frames and it discards the received ones (except for Configuration BPDUs), unifying disabled, blocking and listening states;
 \item \textbf{alternate} role: the port, in discarding state, is connected to the same link as a designated port of \ul{another} bridge, representing a fast replacement for the root port;
 \item \textbf{backup} role: the port, in discarding state, is connected to the same link as a designed port of the \ul{same} bridge, representing a fast replacement for the designated port;
 \item \textbf{edge} role: just hosts can be connected to the port, being aimed to reduce, with respect to the classical STP, disservices experienced by users in connecting their hosts to the network.
\end{itemize}

%8
\noindent
\begin{minipage}{\linewidth}
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|c|c|c|}
  \hline
  \begin{tabular}[c]{@{}c@{}}\textbf{port}\\\textbf{state}\end{tabular} & \begin{tabular}[c]{@{}c@{}}\textbf{port}\\\textbf{role}\end{tabular} & \begin{tabular}[c]{@{}c@{}}receive\\frames?\end{tabular} & \begin{tabular}[c]{@{}c@{}}receive and\\process\\CBPDUs?\end{tabular} & \begin{tabular}[c]{@{}c@{}}generate and\\propagate\\CBPDUs?\end{tabular} & \begin{tabular}[c]{@{}c@{}}update\\filtering\\database?\end{tabular} & \begin{tabular}[c]{@{}c@{}}forward\\frames?\end{tabular} \\
  \hline
  \multirow{3}{*}{discarding} & alternate & yes & yes & no & no & no \\
  \cline{2-2}
  & backup & yes & yes & no & no & no \\
  \cline{2-2} \cline{5-5}
  & designated\footnote{A designated port is in discarding state during the proposal/agreement sequence (please see section~\ref{sez:proposal_agreement}).} & yes & yes & yes & no & no \\
  \cline{1-2} \cline{6-6}
  \multirow{2}{*}{learning} & designated & yes & yes & yes & yes & no \\
  \cline{2-2} \cline{5-5}
  & root & yes & yes & no & yes & no \\
  \cline{1-2} \cline{5-5} \cline{7-7}
  \multirow{3}{*}{forwarding} & designated & yes & yes & yes & yes & yes \\
  \cline{2-2} \cline{5-5}
  & root & yes & yes & no & yes & yes \\
  \cline{2-2}
  & edge & yes & yes & no & yes & yes \\
  \hline
 \end{tabular}}
 \caption{Port roles and states in RSTP.}
\end{table}
\end{minipage}

%15
\section{Configuration BPDU format}
\label{sez:ieee_802.1t}
Configuration BPDU has the following format:
\begin{table}[H]
  \centering
  \centerline{\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|}
    \multicolumn{1}{r@{}}{\footnotesize{1}} & \multicolumn{1}{r@{}}{\footnotesize{2}} & \multicolumn{1}{r@{}}{\footnotesize{4}} & \multicolumn{1}{r@{}}{\footnotesize{6}} & \multicolumn{1}{r@{}}{\footnotesize{7}} & \multicolumn{1}{r@{}}{\footnotesize{8}} & \multicolumn{1}{r@{}}{\footnotesize{12}} & \multicolumn{1}{r@{}}{\footnotesize{16}} & \multicolumn{1}{r@{}}{\footnotesize{24}} & \multicolumn{1}{r@{}}{\footnotesize{32}}\\
    \hline
    \multicolumn{8}{|c|}{Protocol ID (0)} & Version (2) & BPDU Type (2)\\
    \hline
    TC & P & R & S & A & TCA & Root Priority & \multicolumn{2}{|c|}{STP Instance} & \multicolumn{1}{|c}{}\\
    \cline{1-9} \cdashline{10-10}
    \multicolumn{10}{c}{Root MAC Address}\\
    \cdashline{1-6} \cline{7-10}
    \multicolumn{6}{c|}{} & \multicolumn{4}{|c}{Root Path Cost}\\
    \hline
    \multicolumn{6}{c|}{} & Bridge Priority & \multicolumn{2}{|c|}{STP Instance} & \multicolumn{1}{|c}{}\\
    \cline{1-9} \cdashline{10-10}
    \multicolumn{10}{c}{Bridge MAC Address}\\
    \cdashline{1-6} \cline{7-10}
    \multicolumn{6}{c|}{} & \multicolumn{2}{|c|}{Port Priority} & Port Number & \multicolumn{1}{|c}{Message Age}\\
    \hline
    \multicolumn{6}{c|}{} & \multicolumn{3}{|c|}{Max Age} & \multicolumn{1}{|c}{Hello Time}\\
    \hline
    \multicolumn{6}{c|}{} & \multicolumn{3}{|c|}{Forward Delay} & \multicolumn{1}{|c}{}\\
    \cline{1-9}
  \end{tabular}}
  \caption{Configuration BPDU format (35 bytes) in RSTP.}
\end{table}
%16
\noindent
where there are some changes with respect to BPDUs in the classical STP\footnote{Please see section~\ref{sez:formato_bpdu}.}:
\begin{itemize}
 \item \ul{Version} field (1 byte): it identifies RSTP as version number 2 (in STP it was 0);
 
 \item \ul{BPDU Type} field (1 byte): it identifies the Configuration BPDU always as type 2 (in STP it was 0), since Topology Change Notification BPDUs do no longer exist;\footnote{From now on Configuration BPDUs will be referred simply as BPDUs.}
 
 \item 6 new flags: they handle the proposal/agreement mechanism (please refer to section~\ref{sez:proposal_agreement}):
 \begin{itemize}
  \item \ul{Proposal} (P) and \ul{Agreement} (A) flags (1 bit each one): they specify whether the port role is being proposed by a bridge (P = 1) or has been accepted by the other bridge (A = 1);
  \item 2 flags in \ul{Role} field (R) (2 bits): they encode the proposed or accepted port role (00 = unknown, 01 = alternate/backup, 10 = root, 11 = designated);
  \item 2 flags in \ul{State} field (S) (2 bits): they specify whether the port which the role is being proposed or has been accepted for is in learning (10) or forwarding (01) state;
 \end{itemize}
 
 %94[B1]-95[B1]-96[B1]
 \item \ul{Root Identifier} and \ul{Bridge Identifier} fields (8 bytes each one): RSTP includes technical specifications from \textbf{IEEE 802.1t} (2001) which change the format of Bridge Identifier:
 \begin{itemize}
  \item \ul{Bridge Priority} field (4 bits, default value = 8);
  \item \ul{STP Instance} field (12 bits, default value = 0): used in Virtual LANs to enable multiple protocol instances within the same physical network (please refer to section~\ref{sez:PVST});
  \item \ul{Bridge MAC Address} field (6 bytes): unchanged from IEEE 802.1D-1998;
 \end{itemize}
 
 %53
 \item \ul{Root Path Cost} field (4 bytes): RSTP includes technical specifications from IEEE 802.1t (2001) which change the recommended values for Port Path Cost including new port speeds (up to 10 Tb/s);
 
 %17
 \item \ul{Max Age} and \ul{Forward Delay} fields (2 bytes each one): they are altogether unused in RSTP, but they have been kept for compatibility reasons.
\end{itemize}

\section{Changes in the network topology}
%18
\subsection{Recomputing spanning tree}
\label{sez:convergenza_topologia}
%3-5-38
RSTP is characterized by a greater topology convergence speed with respect to the classical STP: in fact it switches from 50 seconds to less than 1 second (order of about 10 ms) if, as was the norm by then when RSTP was standardized, there are just full-duplex point-to-point links (therefore without hubs).

\subsubsection{Detection of a link fault}
When a link fault occurs, its detection by RSTP is faster than the classical STP thanks to a more efficient BPDU handling.

%19-20
Non-root bridges do not just propagate BPDUs generated by the root bridge: each bridge generates every Hello Time (default: 2 s) a BPDU, with the current root bridge as Root Identifier, even if it has not received the BPDU from the root bridge. If BPDUs have not been received for 3 Hello Time periods, the current BPDU is declared obsolete and a fault is assumed to be occurred on the link which the root port is connected to.

%21
This faster aging of information is useless on modern networks:
\begin{itemize}
 \item in older networks with hubs, a bridge can not detect at the physical layer a fault between the hub and another bridge $\Rightarrow$ the only way to detect it is realizing that the `keep-alive' BPDU messages stopped being received;
 \item in newer networks which are pure switched, a bridge can immediately detect a link fault at the physical layer without waiting 3 Hello Times periods.
\end{itemize}

%22-23-24-25
Once a bridge detected a link fault, it starts generating its own BPDUs $\Rightarrow$ every neighbor bridge, as soon as it receives \ul{on its root port} a BPDU from the bridge which is claiming to be the root bridge, instead of discarding it because it is worse than the current one, accepts the new BPDU forgetting the one previously stored, because it means that something bad happened on its path toward the root bridge. At this point:
\begin{itemize}
 \item if its Bridge Identifier is worse than the one in the BPDU, the bridge starts generating BPDUs on its designated ports with the new Root Identifier;
 \item if its Bridge Identifier is better than the one in the BPDU, the bridge starts generating its own BPDUs claiming to be the root bridge.
\end{itemize}

%26-27
\subsubsection{Recovery from a link fault}
Once a fault is detected, some ports can directly turn to the forwarding state without transitioning across the learning state.

%10-28
\paragraph{Alternate ports}
In case the root port faults, the alternate port provides an alternative path between the bridge and the root bridge:
\begin{figure}[H]
\centering
	\begin{subfigure}[b]{.3\textwidth}
		\centering
		\includegraphics[width=\linewidth]{../pic/B2/10a}
	\end{subfigure}
\hspace{0.1\textwidth}
	\begin{subfigure}[b]{.3\textwidth}
		\centering
		\includegraphics[width=\linewidth]{../pic/B2/10b}
	\end{subfigure}
\caption{The alternate port represents a fast replacement for the root port.}
\end{figure}

%11-30
\paragraph{Backup ports}
In case a designed port faults, the backup port provides an alternative path between the bridge and the link:
\begin{figure}[H]
\centering
	\begin{subfigure}[b]{.22\textwidth}
		\centering
		\includegraphics[width=.8\linewidth]{../pic/B2/11a}
	\end{subfigure}
\hspace{0.2\textwidth}
	\begin{subfigure}[b]{.22\textwidth}
		\centering
		\includegraphics[width=.94\linewidth]{../pic/B2/11b}
	\end{subfigure}
\caption{The backup port represents a fast replacement for the designated port.}
\end{figure}

%31-32-33-39
\subsubsection{Insertion of a new link}
\label{sez:proposal_agreement}
The \textbf{proposal/agreement sequence} is an algorithm for fast synchronization on the port role between two bridges.

When a new link is inserted between two bridges:
\begin{enumerate}
 \item each of the two bridges puts into discarding state its port connected to the new link, as well as all its possible other root and designated ports connected on other links, to prevent creation of possible loops during the transient;
 \item each of the two bridges proposes its port as designated for the link by sending a BPDU to the new link with the \ul{Proposal} flag set;
 \item the worse bridge accepts the proposal from the other bridge by sending back a BPDU with the \ul{Agreement} flag set, and puts its port into the proper role (root, alternate, or backup) according to spanning tree algorithm criteria;
 \item the best bridge receives the acceptance BPDU and puts its port as designated for the link;
 \item each of the two bridges repeats the sequence for the other ports which at the beginning it put into discarding state.
\end{enumerate}

Cooperation between the two bridges through BPDU sending is faster with respect to the timer-based mechanism in the classical STP, and more efficient since it does not stop the whole network for a while but from time to time just a bridge neighborhood. The new link must be full-duplex so that BPDUs can be exchanged in both ways: the proposal BPDU along one direction and the acceptance BPDU along the other one.

\subsection{Filtering database update}
\label{sez:aggiornamento_filtering}
\subsubsection{Detecting topology changes}
RSTP is aimed to be less invasive with respect to the classical STP as regards filtering database updates following topology changes: in fact it avoids cleaning filtering databases from old entries, resulting in a considerably increased traffic sent in flooding, when it is not needed.

%41
\paragraph{Moving to discarding state} When a port moves to the discarding state, it does not trigger a filtering database update:
\begin{itemize}
 \item if the removed link was not belonging to a loop, that is alternative paths do not exist, then stations in the other network segment are no longer reachable and entries associated to them are no longer valid, but this is not considered as a problem to be solved immediately: if a frame is sent to one of those stations, it will arrive at the bridge which was connected to the removed link and will be discarded, until the entry will expire naturally and will be cleaned by the bridge without having to touch other entries;
 \item if the removed link was belonging to a loop, that is an alternative path exists through a port in discarding state, then it will be the latter port which will trigger a filtering database update when moving to the forwarding state according RSTP mechanisms.
\end{itemize}

%44-45
\paragraph{Moving to forwarding state} Only when a non-edge port moves to the forwarding state, it triggers a filtering database update:
\begin{itemize}
 \item if the new link does not create a loop, then a filtering database update should not be triggered because no stations become unreachable, but please remember that a bridge does not have knowledge of the global network topology;
 \item if the new link creates a loop, then a port moving to the forwarding state results in another port along the loop moving to the discarding state according to RSTP mechanisms $\Rightarrow$ stations which have been reachable through that port now are reachable through another path, and therefore entries associated to them should be updated.
\end{itemize}

\subsubsection{Announcing topology changes}
When a bridge detects a topology change requiring a filtering database update:
\begin{enumerate}
 %42
 \item the bridge which detected the topology change generates on all its root and designated ports a BPDU with the \ul{Topology Change} flag set;\footnote{The bridge keeps generating/propagating BPDUs until the TC While timer expires after a time equal to twice the Hello Time.}
 %43
 \item every bridge, when receiving the BPDU:
 \begin{enumerate}
  \item it discards all the entries in its filtering database associated to all its root and designated ports, but the one which it received the BPDU from;
  \item it propagates the BPDU on all its root and designated ports, but the one which it received the BPDU from.\footnotemark[\value{footnote}]
 \end{enumerate}
\end{enumerate}

\subsection{Behaviour of edge ports}
%13
When a host connects to an edge port, the port immediately becomes designated and turns to the forwarding state without transitioning across the learning state $\Rightarrow$ 30 seconds (2 times the Forward Delay) should not be waited anymore before having the port fully operational.

%46
In addition, edge ports never trigger filtering database updates neither on moving to the forwarding state (host connection) or on moving to the discarding state (host disconnection) $\Rightarrow$ the user does not experience anymore a network slowdown due to the increased traffic sent in flooding, and the first broadcast frame which the host will send will update filtering databases according to the usual learning algorithms of bridges.

%14
An edge port still keeps listening to BPDUs coming from possible bridges wrongly connected to it, so as to be ready to immediately exit the edge role and take one of the other roles in order to protect the network against possible loops.

\section{Issues}
\subsection{Coexistence of STP and RSTP}
%48-49-50
If a bridge not supporting RSTP is introduced into the network, on receiving Configuration BPDUs with Type equal to 0 they are able to automatically switch to STP mode, but this has some side effects:
\begin{itemize}
 \item because of a single bridge not supporting RSTP, the whole network goes into STP mode and thus fast convergence times are lost;
 \item if the single bridge not supporting RSTP faults or is disconnected from the network, the other bridges keep working in STP mode, and explicit manual configuration should be taken on every single bridge.
\end{itemize}

%52
A bridge can be configured so as to work in RSTP mode on some ports, and in STP mode on other ports $\Rightarrow$ the network is split in two portions working with different spanning tree protocol versions. However this may lead to network instability because of transient loops due to the fact that the RSTP portion enables the forwarding of data frames earlier than the STP portion.

%3
For a seamless coexistence of RSTP and non-RSTP bridges within the same network, \textbf{Multiple Spanning Tree Protocol}, standardized as IEEE 802.1s (2002), should be used: the network portions working with RSTP and the ones working with STP are separated in different domains.

%54
\subsection{Physical layer reliability}
RSTP works perfectly when links at the physical layer are reliable. If instead a link goes up and down frequently because of a dirty connector (optical fibers are quite sensitive), RSTP re-configures the network at every change of status of the link $\Rightarrow$ the network will stay in a transient instable state most of the time, because of the too fast RSTP reactivity.

The `antiflapping' Cisco's proprietary mechanism puts the port into `error disabled' state when it detects a link flapping.