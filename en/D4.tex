%Note: References are to 2013 slides.
\chapter{Redundancy and load balancing at layer 3 in LANs}
%3
At the boundaries of the corporate LAN with the network layer, the router providing connectivity with outside (typically Internet) represents, for hosts having it as their default gateway, a single point of failure, unless the router is redounded properly.

%4
Simple router duplication is not enough: hosts are not able to automatically switch to the other router in case their default gateway fails, because they are not able to learn the network topology through network-layer routing protocols.

%5
Therefore some protocols for automatic management of redundant routers have been defined:
\begin{itemize}
 %6
 \item HSRP: Cisco's proprietary protocol specific for \textbf{default gateway redundancy} and with partial support to load balancing (section~\ref{sez:hsrp});
 \item VRRP: standard protocol very similar to HSRP but free of patents;
 \item GLBP: Cisco's proprietary protocol which improves \textbf{load balancing} with respect to HSRP (section~\ref{sez:glbp}).
\end{itemize}

\section{HSRP}
\label{sez:hsrp}
%7
\afterpage{
\begin{landscape}
\begin{figure}[p]
	\centering
	\includegraphics[width=0.8\linewidth]{../pic/D4/7}
	\caption{Example of corporate network with three redundant routers thanks to HSRP.}
\end{figure}
\end{landscape}
}

\textbf{Hot Standby Routing Protocol} (HSRP) guarantees automatically that every host keeps connectivity with outside the LAN through its default gateway even in case one of the redundant routers fails.

\subsection{Network configuration}
%8-9
Interfaces belonging to the corporate LAN of all redundant routers are assigned a single \textbf{virtual IP address} and a single \textbf{virtual MAC address}, in addition to their actual IP and MAC addresses. Routers can be:
\begin{itemize}
 \item \textbf{active}: it is the router which has the right to serve the LAN, that is to answer at the virtual IP address and at the virtual MAC address;
 \item \textbf{stand-by}: it is the router which has the right to replace the active router in case the latter fails;
 \item \textbf{listen}: they are other routers neither active nor stand-by; one of them will become the stand-by router in case the active router fails.
\end{itemize}

%11-12
The virtual IP address has to be set explicitly by the network administrator during the HSRP configuration, while the virtual MAC address has Cisco's well-known prefix `00:00:0C:07:AC':
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|}
  \multicolumn{1}{r}{\footnotesize{24}} & \multicolumn{1}{r}{\footnotesize{40}} & \multicolumn{1}{r}{\footnotesize{48}} \\
  \hline
  OUI (00:00:0C) & HSRP string (07:AC) & group ID \\
  \hline
 \end{tabular}}
 \caption{HSRP virtual MAC address format.}
\end{table}
\noindent
where the fields are:
\begin{itemize}
 \item \ul{Organizationally Unique Identifier} (OUI) field (3 bytes): string of bits `00:00:0C' is the OUI assigned to Cisco so that MAC addresses of network cards sold by Cisco are globally unique;
 \item \ul{HSRP string} field (2 bytes): string of bits `07:AC' identifies a HSRP virtual MAC address, and can not appear in any physical MAC address $\Rightarrow$ the virtual MAC address is guaranteed to be unique within the LAN: it is not possible for a host to have a MAC address equal to the HSRP virtual MAC address;
 \item \ul{group ID} field (1 byte): it identifies the group the current HSRP instance is referring to (please refer to section~\ref{sez:gruppi_hsrp}).
\end{itemize}

The virtual IP address is set to all hosts as their default gateway address, that is as the IP address to which the hosts will send IP packets heading outside the LAN.

\subsection{Traffic asymmetric routing}
%14
The goal of HSRP is to `deceive' the host by making it believe to be communicating with outside through a single router characterized by an IP address equal to the default gateway address and by a MAC address equal to the MAC address got via ARP protocol, while actually HSRP in case of fault moves the active router to another router without making the host realize that:
\begin{enumerate}
 \item \ul{ARP Request}: when a host connects to the network, it sends an ARP Request to the IP address set as default gateway, that is the virtual IP address;\footnote{Please remember that the ARP Request is a data-link-layer frame with broadcast destination MAC address and with IP address in its payload.}
 \item \ul{ARP Reply}: the router sends back an ARP Reply with its own virtual MAC address;
 %35
 \item \ul{outgoing traffic}: the host sends every following packet to the \ul{virtual MAC address}, and just the active router processes it, while stand-by and listen routers discard it.\\
 Then, the active router forwards the packet according to external routing protocols (OSPF, BGP, etc.) which are independent of HSRP $\Rightarrow$ the packet may also cross stand-by and listen routers if routing protocols believe that this is the best path;
 %32-33-34
 \item \ul{incoming traffic}: every packet coming from outside and heading to the host can enter the LAN from any of the redundant routers according to external routing protocols independent of HSRP, and the host will receive it with the \ul{actual MAC address} of the router as its source MAC address.\\
 External routing protocols are also able to detect router failures, including the default gateway, for incoming traffic $\Rightarrow$ protection is achieved even if the LAN lacks HSRP.
\end{enumerate}

%31
\subsubsection{Dual-homed server}
HSRP can be used to achieve redundancy of a single machine to improve its fault tolerance: a server can have two network interfaces, one primary and one secondary, to which HSRP assigns a virtual IP address and a virtual MAC address $\Rightarrow$ the server will keep being reachable at the same IP address even if the link connecting the primary interface to the network fails.

\subsection{Hello packets}
%6-10
\textbf{Hello packets} are messages generated by redundant routers to:
\begin{itemize}
 \item \ul{elect the active router}: in the negotiation stage, routers exchange Hello packets proposing themselves as active routers $\Rightarrow$ the active router is the one with the highest priority (configurable by the network administrator), or if there is a tie the one with the highest IP address;
 %26
 \item \ul{detect failures of the active router}: the active router periodically sends Hello packets as `keep-alive' messages $\Rightarrow$ in case router active fails, the stand-by router does no longer receive the `keep-alive' message and elect itself as the active router;
 %15
 \item \ul{update filtering databases}: when the active router changes, the new active router starts sending Hello messages notifying to bridges within the corporate LAN the new location of the virtual MAC address $\Rightarrow$ all bridges will update their filtering databases accordingly.\\
 %16
 When a router becomes active, it also sends a gratuitous ARP Reply in broadcast (normal ARP Replies are unicast) with the virtual MAC address as its source MAC address.
\end{itemize}

%24
In the Hello packet the HSRP header is encapsulated in the following format:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|rl|rl|rl|c|}
 \multicolumn{2}{c}{\footnotesize{14 bytes}} & \multicolumn{2}{c}{\footnotesize{20 bytes}} & \multicolumn{2}{c}{\footnotesize{8 bytes}} & \multicolumn{1}{c}{\footnotesize{20 bytes}} \\
 \hline
 \multicolumn{2}{|c|}{MAC header} & \multicolumn{2}{|c|}{IP header} & \multicolumn{2}{|c|}{UDP header} & \multirow{4}{*}{\begin{tabular}[t]{@{}c@{}}HSRP\\header\end{tabular}} \\
 \begin{tabular}[t]{@{}r@{}}\footnotesize{src:}\end{tabular} & \footnotesize{virtual MAC address} & \begin{tabular}[t]{@{}r@{}}\footnotesize{src:}\end{tabular} & \footnotesize{actual IP address} & \begin{tabular}[t]{@{}r@{}}\footnotesize{src:}\end{tabular} & \footnotesize{port 1985} & \\
 \begin{tabular}[t]{@{}r@{}}\footnotesize{dst:}\end{tabular} & \footnotesize{01:00:5E:00:00:02} & \begin{tabular}[t]{@{}r@{}}\footnotesize{dst:}\end{tabular} & \footnotesize{224.0.0.2} & \begin{tabular}[t]{@{}r@{}}\footnotesize{dst:}\end{tabular} & \footnotesize{port 1985} & \\
 & & \begin{tabular}[t]{@{}r@{}}\footnotesize{TTL:}\end{tabular} & \footnotesize{1} & & & \\
 \hline
 \end{tabular}}
 \caption{HSRP Hello packet format generated by the active router.}
\end{table}

\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|rl|rl|rl|c|}
 \multicolumn{2}{c}{\footnotesize{14 bytes}} & \multicolumn{2}{c}{\footnotesize{20 bytes}} & \multicolumn{2}{c}{\footnotesize{8 bytes}} & \multicolumn{1}{c}{\footnotesize{20 bytes}} \\
 \hline
 \multicolumn{2}{|c|}{MAC header} & \multicolumn{2}{|c|}{IP header} & \multicolumn{2}{|c|}{UDP header} & \multirow{4}{*}{\begin{tabular}[t]{@{}c@{}}HSRP\\header\end{tabular}} \\
 \begin{tabular}[t]{@{}r@{}}\footnotesize{src:}\end{tabular} & \footnotesize{actual MAC address} & \begin{tabular}[t]{@{}r@{}}\footnotesize{src:}\end{tabular} & \footnotesize{actual IP address} & \begin{tabular}[t]{@{}r@{}}\footnotesize{src:}\end{tabular} & \footnotesize{port 1985} & \\
 \begin{tabular}[t]{@{}r@{}}\footnotesize{dst:}\end{tabular} & \footnotesize{01:00:5E:00:00:02} & \begin{tabular}[t]{@{}r@{}}\footnotesize{dst:}\end{tabular} & \footnotesize{224.0.0.2} & \begin{tabular}[t]{@{}r@{}}\footnotesize{dst:}\end{tabular} & \footnotesize{port 1985} & \\
 & & \begin{tabular}[t]{@{}r@{}}\footnotesize{TTL:}\end{tabular} & \footnotesize{1} & & & \\
 \hline
 \end{tabular}}
 \caption{HSRP Hello packet format generated by the stand-by router.}
\end{table}

%25
\paragraph{Remarks}
\begin{itemize}
\item \ul{source MAC address}: it is the virtual MAC address for the active router, it is the actual MAC address for the stand-by router;
\item \ul{destination IP address}: `224.0.0.2' is the IP address for the `all-routers' multicast group; it is one of the multicast addresses unfiltered by IGMP snooping and therefore sent always in flooding by bridges\footnote{Please see chapter~\ref{cap:igmp_snooping}.};
\item \ul{destination MAC address}: `01:00:5E:00:00:02' is the multicast MAC address derived from the multicast IP address;
\item \ul{Time To Live} (TTL): it is equal to 1 so that packets are immediately discarded by routers which they reach, because they can be propagated just within the LAN;
\item the HSRP header in the Hello packet is encapsulated into UDP and not into TCP because losing a Hello packet does not require transmitting it again;
\item listen routers do not generate Hello packets, unless they detect that the stand-by router has become the active router and they should candidate so that one of them will become the new stand-by router.
\end{itemize}

\subsubsection{HSRP header format}
%17
The HSRP header has the following format:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|}
  \multicolumn{1}{r}{\footnotesize{8}} & \multicolumn{1}{r}{\footnotesize{16}} & \multicolumn{1}{r}{\footnotesize{24}} & \multicolumn{1}{r}{\footnotesize{32}} \\
  \hline
  Version & Op Code & State & Hello Time \\
  \hline
  Hold Time & Priority & Group & Reserved \\
  \hline
  \multicolumn{4}{|c}{Authentication} \\
  \hdashline
  \multicolumn{4}{c|}{Data} \\
  \hline
  \multicolumn{4}{|c|}{Virtual IP Address} \\
  \hline
 \end{tabular}}
 \caption{HSRP header format (20 bytes).}
\end{table}
\noindent
where the most significant fields are:
\begin{itemize}
 %18
 \item \ul{Op Code} field (1 byte): it describes the type of message included in the Hello packet:
 \begin{itemize}
  \item[0 =] Hello: the router is running and is capable to become the active or stand-by router;
  \item[1 =] Coup: the router wants to become the active router;
  \item[2 =] Resign: the router does no longer want to be the active router;
 \end{itemize}

 %19-20
 \item \ul{State} field (1 byte): it describes the current state of the router sending the message:
 \begin{itemize}
  \item[8 =] Standby: the HSRP packet has been sent by the stand-by router;
  \item[16 =] Active: the HSRP packet has been sent by the active router;
 \end{itemize}
 
 %21
 \item \ul{Hello Time} field (1 byte): it is the time between Hello messages sent by routers (default: 3 s);
 \item \ul{Hold Time} field (1 byte): it is the time of validity for the current Hello message, at the expiry of which the stand-by router proposes itself as the active router (default: 10 s);
 
 %22
 \item \ul{Priority} field (1 byte): it is the priority of the router used for the election process for the active/stand-by router (default: 100);
 \item \ul{Group} field (1 byte): it identifies the group which the current HSRP instance is referring to (please refer to~\ref{sez:gruppi_hsrp});
 
 %23
 \item \ul{Authentication Data} field (8 bytes): it includes a clear-text 8-character-long password (default: `cisco');
 \item \ul{Virtual IP Address} field (4 bytes): it is the virtual IP address used by the group, that is the IP address used as the default gateway address by hosts in the corporate LAN.
\end{itemize}

%44
With default values for Hello Time and Hold Time parameters, convergence time is equal to about 10 seconds.

\subsection{HSRP groups}
%29
\label{sez:gruppi_hsrp}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{../pic/D4/29}
	\caption{Example of network with multiple HSRP groups.}
	\label{fig:gruppi_hsrp}
\end{figure}

\noindent
\textbf{HSRP groups} allow to distinguish multiple logical IP networks over the same physical LAN: a HSRP group is corresponding to each IP network, with a pair virtual MAC address and virtual IP address. Hosts in an IP network have one of the virtual IP addresses set as their default gateway address, hosts in another IP network have another virtual IP address set as their default gateway address, and so on.

Each redundant router knows multiple pairs virtual MAC address and virtual IP address, one for each group $\Rightarrow$ every router (except listen routers) generates a Hello packet for each group, and answers to one of the virtual MAC addresses on receiving traffic from hosts in an IP network, to another one on receiving traffic from hosts in another IP network, and so on.

The last 8 bits in the virtual MAC address identify the group which the address is referring to $\Rightarrow$ HSRP is able to manage up to 256 different groups over the same LAN.
\FloatBarrier

%30
\subsubsection{With VLANs}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{../pic/D4/30}
	\caption{Example of network with multiple HSRP groups where there are VLANs.}
	\label{fig:gruppi_hsrp_2}
\end{figure}

\noindent
Defining multiple HSRP groups is mandatory if there are VLANs: every VLAN is in fact a separate LAN with its own default gateway $\Rightarrow$ each VLAN is assigned a HSRP group. Every one-arm router\footnote{Please see section~\ref{sez:onearm_router}.} has multiple virtual interfaces\footnote{Please see section~\ref{sez:link_bridge_server}.}, one for every VLAN $\Rightarrow$ HSRP groups are configured on the same physical interface but each one on different logical interfaces.
\FloatBarrier

%36-37-38
\subsubsection{Multi-group HSRP (mHSRP)}
By a proper priority configuration, traffic from IP networks can be distributed over redundant routers (\textbf{load sharing}):
\begin{itemize}
 \item figure~\ref{fig:gruppi_hsrp}: traffic from IP network 1 crosses router R2, while traffic from IP network 2 crosses router R1;
 \item figure~\ref{fig:gruppi_hsrp_2}: traffic from VLAN 1 crosses router R2, while traffic from VLAN 2 crosses router R1.
\end{itemize}.

\paragraph{Advantages}
\begin{itemize}
 \item mHSRP is more convenient when \ul{incoming traffic} for the LAN is \ul{symmetrical}: a one-arm router for VLAN interconnection can be redounded so that a router sustains traffic incoming from a first VLAN and outgoing to a second VLAN, while the other router sustains traffic incoming from the second VLAN and outgoing to the first VLAN;
 \item \ul{better resource utilization}: in a network with a single HSRP group the bandwidth of the stand-by router is altogether unused $\Rightarrow$ mHSRP allows to use the bandwidth of both the routers.
\end{itemize}

\paragraph{Disadvantages}
\begin{itemize}
 \item mHSRP is not so convenient when \ul{incoming traffic} for the LAN is \ul{asymmetrical}: load sharing in fact affects just outgoing traffic (incoming traffic is independent of HSRP), and outgoing (upload) traffic generally is lower with respect to incoming (download) traffic;
 \item load sharing does not necessarily imply \ul{traffic balancing}: traffic coming from a LAN may be very higher than traffic coming from another LAN;
 \item \ul{configuration troubles}: hosts in every IP network must have a different default gateway address with respect to hosts in other IP networks, but the DHCP server usually returns a single default gateway address for all hosts.
\end{itemize}

\subsection{Track feature}
%41
HSRP offers protection from failures of the link connecting the default gateway router to the LAN and from failures of the default gateway router itself, but not from failures of the link connecting the default gateway router to Internet: a failure on the WAN link in fact forces packets to be sent to the active router which in turn sends them all to the stand-by router, instead of immediately going to the stand-by router $\Rightarrow$ this does not imply a real loss of internet connectivity, but implies an additional overhead in the packet forwarding process.

The \textbf{track feature} allows to detect failures on WAN links and trigger the stand-by router to take the place of the active router by automatically decreasing the priority of the active router (default: \textminus 10).

%43
The track feature works only if the \textbf{preemption capability} is on: if the priority of the active router is decreased so as to bring it below the priority of the stand-by router, the latter can `preempt' the active state from the active router by sending a Hello message of Coup type.

%42
However, detecting failures happens exclusively at the physical layer: the track features is not able to detect a failure occurred on a farther link beyond a bridge.

\subsection{Issues}
%47-48
\subsubsection{Data-link-layer resiliency}
\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{../pic/D4/47}
	\caption{Example of fault inside the data-link-layer network.}
	\label{fig:resilienza_l2}
\end{figure}

\noindent
HSRP does not protect against all the faults in the data-link-layer network. For example, the fault in figure~\ref{fig:resilienza_l2} partitions the corporate network into two parts, and since it happens between two bridges it can not be detected by routers at the physical layer. The stand-by router does no longer receive Hello messages from the active router and promotes itself as the active $\Rightarrow$ \ul{outgoing traffic} is not affected at all by the occurrence of a fault: each of the two routers serves the outgoing traffic from one of the two network portions.

The fault has instead an impact on \ul{incoming traffic}, because some frames can not reach the destination hosts. Network-layer routing protocols in fact work exclusively from router to router: they just detect the path between the two router was broken somewhere, but they are not able to detect path breaks between a router and a host, because their task is to forward the packet so that it arrives at any of the edge routers, which then is in charge of the direct delivery of the frame to the final destination. As seen from outside, both the routers appear to have connectivity to the same IP network, therefore network-layer routing protocols will assume all the hosts belonging to that IP network can be reached through both the interfaces and choose any of them based on shortest path criterion:
\begin{itemize}
\item if the router serving the network portion the destination is belonging to is chosen, the frame is seamlessly delivered to the destination;
\item if the router serving the other network portion is chosen, the router performs an ARP Request which no hosts will answer and so the destination will appear non-existing in the network.
\end{itemize}

Therefore it is important to redound all the links inside the data-link-layer network, by putting multiple links in parallel managed by the spanning tree protocol or configured in link aggregation.
\FloatBarrier

%49-50-51-52-53
\subsubsection{Flooding}
\begin{figure}
	\centering
	\includegraphics[width=0.35\linewidth]{../pic/D4/50}
	\caption{Example of network topology suffering from periodic flooding.}
	\label{fig:flooding_hsrp}
\end{figure}

\noindent
In some network topologies, traffic asymmetrical routing may lead to a situation where in some periods of time the incoming traffic from outside sent in flooding increases considerably, while in other ones the incoming traffic is forwarded properly by bridges. This is due to the fact that router ARP caches generally last longer than bridge filtering databases.

For example, in figure~\ref{fig:flooding_hsrp} mappings in the ARP cache on ingress router R2 expire in 5 minutes, while entries in the filtering database on bridge B2 expire in just 2 minutes:
\begin{enumerate}
 \item the outgoing unicast packet just updates filtering database on bridge B1, because it does not cross bridge B2;
 \item the incoming packet triggers router R2 to send an ARP Request to host H1 (in broadcast);
 \item the ARP Reply host H1 sends back updates both the ARP cache on router R2 and the filtering database on bridge B2;
 \item in the first 2 minutes, incoming packets addressed to host H1 are forwarded seamlessly;
 \item after 2 minutes since the ARP Reply, the entry related to host H1 in the filtering database on bridge B2 expires;
 \item in the following 3 minutes, router R2, which still has a valid mapping for host H1 in its ARP cache, sends incoming packets toward bridge B2, which sends them all in flooding because it does not receive frames having host H1 as their sources;
 \item after 5 minutes since the ARP Reply, the mapping in the ARP cache on router R2 expires too;
 \item the next ARP Reply solicited by router R2 at last updates also the filtering database on bridge R2.
\end{enumerate}

\paragraph{Possible solutions} This problem in the network is not easy to be identified because it manifests itself intermittently; once the problem is identified, it is possible to:
\begin{itemize}
 \item force stations to send gratuitous broadcast frames more often, with a frequency lower than the ageing time of entries in bridge filtering databases;
 \item increase the ageing time value on bridges along the ingress path to at least the duration time of router ARP caches.
\end{itemize}
\FloatBarrier
%\clearpage

%54-55
\subsubsection{Unidirectional links}
\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{../pic/D4/54}
	\caption{Example of fault on a unidirectional link.}
	\label{fig:unidirezionali_hsrp}
\end{figure}

\noindent
HSRP does not contemplates fault management on unidirectional links: for example, in figure~\ref{fig:unidirezionali_hsrp} a fault occurs toward the stand-by router, which does no longer receive Hello messages from the active router and elects itself as the active, starting sending Hello packets with the virtual MAC address as their source addresses $\Rightarrow$ the bridge receives alternatively Hello packets from both the active routers having the same MAC address as their source addresses, and the entry related to that MAC address will keep oscillating periodically $\Rightarrow$ if a host sends a frame to its default gateway while the entry in the bridge filtering database is associated to the former stand-by router, the frame, being unable to go through the unidirectional link, will be lost.
\FloatBarrier

%67-68-69-70
\section[GLBP]{GLBP\footnote{This section includes CC BY-SA contents from article \href{https://en.wikipedia.org/wiki/Gateway_Load_Balancing_Protocol}{Gateway Load Balancing Protocol} on English Wikipedia.}}
\label{sez:glbp}
\afterpage{
\begin{landscape}
\begin{figure}[p]
	\centering
	\includegraphics[width=0.85\linewidth]{../pic/D4/68}
	\caption{Example of corporate network with three redundant routers thanks to GLBP.}
\end{figure}
\end{landscape}
}

\textbf{Gateway Load Balancing Protocol} (GLBP) adds to the default gateway redundancy the capability of automatically distributing outgoing traffic over all redundant routers.

GLBP elects one Active Virtual Gateway (AVG) for each group; other group members, called Active Virtual Forwarders (AVF), act as backup in case of AVG failure. The elected AVG then assigns a virtual MAC address to each member of the GLBP group, including itself; each AVF assumes responsibility for forwarding packets sent to its virtual MAC address.

In case of an AVF failure, the AVG notifies one of the still active AVF entrusting it with the task of answering also traffic addressed toward the virtual MAC address of the faulted AVF.

%71
The AVG answers ARP Requests sent by hosts with MAC addresses pointing to different routers, based on one of the following load balancing algorithms:
\begin{itemize}
 \item none: the AVG is the only forwarder (as in HSRP);
 \item \ul{weighted}: every router is assigned a weight, determining the percentage of ARP Requests answered with the virtual MAC address of that router, and therefore the percentage of hosts which will use that router as their forwarder $\Rightarrow$ useful when exit links have different capacities;
 \item \ul{round robin}: virtual MAC addresses are selected sequentially in a circular queue;
 \item \ul{host dependent}: it guarantees that a host always keeps being associated to the same forwarder, that is if the host performs two ARP Requests it will receive two ARP Replies with the same virtual MAC address $\Rightarrow$ this avoids problems with NAT address translation mechanisms.
\end{itemize}