%Note: References are to 2013 slides.
\chapter{Introduction to Local Area Networks}
\section{Origins}
%11
\subsection{LAN definition}
The IEEE 802 working group defined the \textbf{Local Area Network} (LAN) as a communication system through a \ul{shared medium}, which allows \ul{independent devices} to communicate together within a \ul{limited area}, using an \ul{high-speed} and \ul{reliable} communication channel.

\paragraph{Keywords}
\begin{itemize}
 \item \ul{shared medium}: everyone is attached to the same communication medium;
 \item \ul{independent devices}: everyone is peer, that is it has the same privilege in being able to talk (no client-server interaction);
 \item \ul{limited area}: everyone is located within the same local area (e.g. corporate, university campus) and is at most some kilometers far one from each other (no public soil crossing);
 \item \ul{high-speed}: at that time LAN speeds were measured in Megabit per second (Mbps), while WAN speeds in bit per second;
 \item \ul{reliable}: faults are little frequent $\Rightarrow$ checks are less sophisticated to the benefit of performance.
\end{itemize}

%3-4-5-6-7-8
\subsection{LAN vs. WAN comparison}
Protocols for \textbf{Wide Area Network}s (WAN) and for Local Area Networks evolved independently until the 80s because purposes were different. In the 90s the IP technology finally allowed to interconnect these two worlds.

\paragraph{WAN} WANs were born in the 60s to connect remote terminals to the few existing mainframes:
 \begin{itemize}
  \item \ul{communication physical medium}: point-to-point leased line over long distance;
  \item \ul{ownership of physical medium}: the network administrator has to lease cables from government monopoly;
  \item \ul{usage pattern}: smooth, that is bandwidth occupancy for long periods of time (e.g. terminal session);
  \item \ul{type of communication}: always unicast, multiple communications at the same time;
  \item \ul{quality of physical medium}: high fault frequency, low speeds, high presence of electromagnetic disturbances;
  \item \ul{costs}: high, also in terms of operating costs (e.g. leasing fee for cables);
  \item \ul{intermediate communication system}: required to manage large-scale communications (e.g. telephone switches) $\Rightarrow$ switching devices can fault.
 \end{itemize}
 
\paragraph{LAN} LANs appeared at the end of the 70s to share resources (such as printers, disks) among small working groups (e.g. departments):
 \begin{itemize}
  \item \ul{communication physical medium}: multi-point shared bus architecture over short distance;
  \item \ul{ownership of physical medium}: the network administrator owns cables;
  \item \ul{usage pattern}: bursty, that is short-term data peaks (e.g. document printing) followed by long pauses;
  \item \ul{type of communication}: always broadcast, just one communication at the same time;
  \item \ul{quality of physical medium}: greater reliability against failures, high speeds, lower exposure to external disturbances;
  \item \ul{costs}: reasonable, concentrated mainly when setting up the network;
  \item \ul{intermediate communication system}: not required $\Rightarrow$ lower cost, higher speed, greater reliability, greater flexibility in adding and removing stations.
 \end{itemize}

\subsection{Communication medium sharing}
\label{sez:multiplexing_statistico}
%10
Before the advent of hubs and bridges, the shared communication medium could be implemented in two ways:
\begin{itemize}
 \item \ul{physical broadcast}: broadcast-based technologies, such as the bus: the signal sent by a station propagates to all the other stations;
 \item \ul{logical broadcast}: point-to-point technologies, such as the token ring: the signal sent by a station arrives at the following station, which duplicates it toward the station after that one, and so on.
\end{itemize}

%9
\paragraph{Issues}
\begin{itemize}
 \item \ul{privacy}: everyone can hear what crosses the shared medium $\Rightarrow$ an addressing system should be made (nowadays: MAC addresses);
 \item \ul{concurrency}: just one communication at a time is possible:
 \begin{itemize}
  \item \ul{collisions}: if two stations transmit simultaneously, the data sent by a station may overlap the data sent by the other one $\Rightarrow$ a mechanism for collision detection and recovery should be made (nowadays: CSMA/CD protocol, please refer to section~\ref{sez:csma_cd});
  \item \ul{channel monopolization}: in the \textbf{back-to-back transmission}, a station may occupy the channel for a long period of time preventing other stations from talking $\Rightarrow$ a sort of \textbf{statistical multiplexing}, that is simulating multiple communications at the same time by defining a maximum transmission unit called \textbf{chunk} and by alternating chunks from a station with the ones from another one (nowadays: Ethernet frames), should be made.
 \end{itemize}
\end{itemize}

\section{Data-link sub-layers}
%20
In LANs the data-link layer is split in two sub-layers:
\begin{itemize}
 \item MAC: it arbitrates the access to the physical medium, and is specific for each physical-layer technology (section~\ref{sez:mac});
 \item LLC: it defines the interface toward the network layer, and is common in all physical-layer technologies (section~\ref{sez:llc}).
\end{itemize}

\subsection{MAC}
\label{sez:mac}

\let\orighref\href
\renewcommand{\href}{\Collectverb{\hrefii}}
\newcommand\hrefii[2]{\footnote{According to the canonical order (network byte order), which is the native order in IEEE 802.3 (Ethernet) but not in IEEE 802.5 (token ring) (please see section \orighref{#1}{#2} in article \textit{MAC address} on the English Wikipedia).}}

%26
Every network card is identified uniquely by a \textbf{MAC address}. MAC addresses have the following format:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|}
  \multicolumn{1}{r}{\footnotesize{24}} & \multicolumn{1}{r}{\footnotesize{48}} \\
  \hline
  OUI & NIC ID \\
  \hline
 \end{tabular}}
 \caption{MAC address format (6 bytes).}
\end{table}
\noindent
where the fields are:
\begin{itemize}
 \item \ul{Organization Unique Identifier} (OUI) field (3 bytes): code assigned uniquely by IEEE to identify the network card manufacturer:
 %28-30-31
 \begin{itemize}
  \item first least-significant bit in the first byte:\href{https://en.wikipedia.org/wiki/MAC_address#Bit-reversed_notation}{Bit-reversed notation}
  \begin{itemize}
    \item \ul{Individual} (value 0): the address is associated to a single station (unicast);
    \item \ul{Group} (value 1): the address refers to multiple stations (multicast/broadcast);
  \end{itemize}
  \item second least-significant bit in the first byte:\footnotemark[\value{footnote}]
  \begin{itemize}
   \item \ul{Universal} (value 0): the address is assigned uniquely;
   \item \ul{Local} (value 1): the address is customized by the user;
  \end{itemize}
 \end{itemize}
 \item \ul{NIC Identifier} (NIC ID) field (3 bytes): code assigned uniquely by the manufacturer to identify the specific network card (also called `Network Interface Controller' [NIC]).
\end{itemize}

\let\href\orighref

%24
The \textbf{Media Access Control} (MAC) header has the following format:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|c|}
  \multicolumn{1}{r}{\footnotesize{48}} & \multicolumn{1}{r}{\footnotesize{96}} & \multicolumn{1}{r}{\footnotesize{112}} & \multicolumn{1}{c}{\footnotesize{46 to 1500 bytes}} & \multicolumn{1}{c}{\footnotesize{4 bytes}} \\
  \hline
  Destination Address & Source Address & Length & payload & FCS \\
  \hline
 \end{tabular}}
 \caption{MAC header format (18 bytes).}
\end{table}
\noindent
where the fields are:
\begin{itemize}
 \item \ul{Destination Address} field (6 bytes): it specifies the destination MAC address.\\
 This is put before the source MAC address because in this way the destination can process it earlier and discard the frame if it is not addressed to it;
 \item \ul{Source Address} field (6 bytes): it specifies the source MAC address (always unicast);
 \item \ul{Length} field (2 bytes): it specifies the payload length;
 %37-38
 \item \ul{Frame Control Sequence} (FCS) field (4 bytes): it includes the CRC code for integrity control over the entire frame.\\
 If the CRC code check fails, the arrived frame was corrupted (e.g. because of a collision) and is discarded; higher-layer mechanisms (e.g. TCP) will be responsible for recovering the error by sending again the frame.
\end{itemize}

%34-35
A network card when receiving a frame:
\begin{itemize}
\item if the destination MAC address matches with the one of the network card or is of broadcast type (`FF-FF-FF-FF-FF-FF'), it accepts it and sends it to higher layers;
\item if the destination MAC address does not match with the one of the network card, it discards it.
\end{itemize}

%36
A network card set in promiscuous mode accepts all frames $\Rightarrow$ it is useful for network sniffing.

\subsection{LLC}
\label{sez:llc}
%43
The \textbf{Logical Link Control} (LLC) header has the following format:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|}
  \multicolumn{1}{r}{\footnotesize{8}} & \multicolumn{1}{r}{\footnotesize{16}} & \multicolumn{1}{r}{\footnotesize{24 or 32}} \\
  \hline
  DSAP & SSAP & CTRL \\
  \hline
 \end{tabular}}
 \caption{LLC header format (3 or 4 bytes).}
\end{table}
\noindent
where the fields are:
\begin{itemize}
 %43
 \item \ul{DSAP} field (1 byte, of which 2 bits reserved): it identifies the upper-layer protocol used by the destination;
 \item \ul{SSAP} field (1 byte, of which 2 bits reserved): it identifies the upper-layer protocol used by the source;
 %44
 \item \ul{Control} (CTRL) field (1 or 2 bytes): it derives from the HDLC control field, but is unused.
\end{itemize}

%46
\paragraph{Issues of DSAP and SSAP fields}
\begin{itemize}
 \item \ul{limited set of values}: just 64 protocols can be coded;
 \item \ul{codes assigned by ISO}: just protocol published by an internationally recognized standard organization are corresponding to codes, while protocols defined by other bodies or pushed by some vendors (e.g. IP) are excluded;
 \item \ul{code redundancy}: there is no reason to have two fields to defines protocols, because the source and the destination always talk the same protocol (e.g. both IPv4 or both IPv6).
\end{itemize}

%47
\subsubsection{SNAP}
The \textbf{Subnetwork Access Protocol} (SNAP) is a particular implementation of LLC for protocols which have not a standard code.

The LLC SNAP header has the following format:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|c|}
  \multicolumn{1}{r}{\footnotesize{8}} & \multicolumn{1}{r}{\footnotesize{16}} & \multicolumn{1}{r}{\footnotesize{24}} & \multicolumn{1}{r}{\footnotesize{48}} & \multicolumn{1}{r}{\footnotesize{64}} \\
  \hline
  DSAP (0xAA) & SSAP (0xAA) & CTRL (3) & OUI & Protocol Type \\
  \hline
 \end{tabular}}
 \caption{LLC SNAP header format (8 bytes).}
\end{table}
\noindent
where the fields are:
\begin{itemize}
 \item \ul{DSAP}, \ul{SSAP}, \ul{CTRL} fields: LLC fields are fixed to indicate the presence of the SNAP header;
 \item \ul{Organization Unique Identifier} (OUI) field (3 bytes): it identifies the organization which defined the protocol.\\
 If it is equal to 0, the value in the `Protocol Type' field is corresponding to the one used in Ethernet DIX;
 \item \ul{Protocol Type} field (2 bytes): it identifies the upper-layer protocol (e.g. 0x800 = IP, 0x806 = ARP).
\end{itemize}

%48
Actually, the LLC SNAP header is not very used due to waste of bytes, to the benefit of the `Ethertype' field in Ethernet DIX (please refer to section~\ref{sez:ethernet_dix}).