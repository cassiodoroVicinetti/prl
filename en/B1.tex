%Note: References are to 2013 slides.
\chapter{Spanning Tree Protocol}
\label{cap:spanning_tree_protocol}
\section{The loop problem}
\begin{figure}
	\centering
	\includegraphics[width=0.5\linewidth]{../pic/B1/8}
	\caption{Example of sending a unicast frame to a station not included in the filtering database in a data-link network with a ring in topology.}
\end{figure}

%3-4-5
\noindent
If the network has a logical ring in topology, some frames may start moving indefinitely in a chain multiplication around the loop:
\begin{itemize}
%7
\item \ul{broadcast/multicast frames}: they are always propagated on all ports, causing a \textbf{broadcast storm};
%6
\item \ul{unicast frames} sent to a station yet not included in the filtering database or inexistent: they are sent in flooding.
\end{itemize}

%9
Moreover, bridges in the loop may have their filtering databases inconsistent, that is the entry in the filtering database related to the sender station changes its port every time a frame replication arrives through a different port, making the bridge believe that the frame has come from the station itself moving.
\FloatBarrier

\section[Spanning tree algorithm]{Spanning tree algorithm\footnote{This section includes CC BY-SA contents from article \href{https://en.wikipedia.org/wiki/Spanning_Tree_Protocol}{Spanning Tree Protocol} on English Wikipedia.}}
%10-11-13
The \textbf{spanning tree algorithm} allows to remove logical rings from the network physical topology, by disabling links\footnote{Really the spanning tree algorithm blocks ports, not links (please refer to section~\ref{sez:stati_porte}).} to transform a mesh topology (graph) into a tree called \textbf{spanning tree}, whose root is one of the bridges called \textbf{root bridge}.

%62
Each link is characterized by a cost based on the link speed: given a root bridge, multiple spanning trees can be built connecting all bridges one with each other, but the spanning tree algorithm chooses the spanning tree made up of the lowest cost edges.

%61
\paragraph{Parameters}
\begin{itemize}
%14
\item \textbf{Bridge Identifier}: it identifies the bridge uniquely and includes:
\begin{itemize}
\item \ul{bridge priority}: it can be set freely (default value = 32768);
\item \ul{bridge MAC address}: it is chosen between the MAC address of his ports by a vendor-specific algorithm and can not be changed;
\end{itemize}
%16
\item \textbf{Port Identifier}: it identifies the bridge port and includes:
\begin{itemize}
\item \ul{port priority}: it can be set freely (default value = 128);
\item \ul{port number}: in theory a bridge can not have more than 256 ports $ \Rightarrow$ in practice also the port priority field can be used if needed;
\end{itemize}
%15
\item \textbf{Root Path Cost}: it is equal to the sum of the costs of all links, selected by the spanning tree algorithm, traversed to reach the root bridge (the cost for traversing a bridge is null).
\end{itemize}

%17-18-19-20-21-22-23-24-25-26-27-28-29-30-31-32-33-34
\subsection{Criteria}
The spanning tree can be determined by the following criteria.

\subsubsection{Root bridge}
A \textbf{root bridge} is the root for the spanning tree: all the frames going from one of its sub-trees to another one must cross the root bridge.\footnote{Please pay attention to the fact that the traffic moving within the same sub-tree does not cross the root bridge.}

The bridge with the \ul{smallest Bridge Identifier} is selected as the root bridge: the root of the spanning tree will be therefore the bridge with the lowest priority, or if there is a tie the one with the lowest MAC address.

\subsubsection{Root port}
\begin{figure}
\centering
	\begin{subfigure}[b]{.2\textwidth}
		\centering
		\includegraphics[width=\linewidth]{../pic/B1/29a}
		\caption{Least-cost path.}
	\end{subfigure}
\hspace{0.01\textwidth}
	\begin{subfigure}[b]{.26\textwidth}
		\centering
		\includegraphics[width=\linewidth]{../pic/B1/29b}
		\caption{Smallest remote Bridge ID.}
	\end{subfigure}
\hspace{0.01\textwidth}
	\begin{subfigure}[b]{.23\textwidth}
		\centering
		\includegraphics[width=\linewidth]{../pic/B1/29c}
		\caption{Smallest remote Port ID.}
	\end{subfigure}
\hspace{0.01\textwidth}
	\begin{subfigure}[b]{.23\textwidth}
		\centering
		\includegraphics[width=\linewidth]{../pic/B1/29d}
		\caption{Smallest local Port ID.}
	\end{subfigure}
\caption{Criteria for selecting a root port for the bridge the arrow points to.}
\end{figure}

\noindent
A \textbf{root port} is the port in charge of connecting to the root bridge: it sends frames toward the root bridge and receives frames from the root bridge.
\begin{enumerate}
\item The cost of each possible path is determined from the bridge to the root. From these, the one with the smallest cost (a \ul{least-cost path}) is picked. The port connecting to that path is then the root port for the bridge.
\item When multiple paths from a bridge are least-cost paths toward the root, the bridge uses the neighbor bridge with the \ul{smallest Bridge Identifier} to forward frames to the root. The root port is thus the one connecting to the bridge with the lowest Bridge Identifier.
\item When two bridges are connected by multiple cables, multiple ports on a single bridge are candidates for root port. In this case, the path which passes through the port on the neighbor bridge that has the \ul{smallest Port Identifier} is used.
\item In a particular configuration with a hub where the remote Port Identifiers are equal, the path which passes through the port on the bridge itself that has the \ul{smallest Port Identifier} is used.
\end{enumerate}
\FloatBarrier

\subsubsection{Designated port}
\begin{figure}
\centering
	\begin{subfigure}[b]{.3\textwidth}
		\centering
		\includegraphics[width=0.6\linewidth]{../pic/B1/32a}
		\caption{Least-cost path.}
	\end{subfigure}
\hspace{0.01\textwidth}
	\begin{subfigure}[b]{.3\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{../pic/B1/32b}
		\caption{Smallest local Bridge ID.}
	\end{subfigure}
\hspace{0.01\textwidth}
	\begin{subfigure}[b]{.3\textwidth}
		\centering
		\includegraphics[width=0.75\linewidth]{../pic/B1/32c}
		\caption{Smallest local Port ID.}
	\end{subfigure}
\caption{Criteria for selecting a designated port for the link the arrow points to.}
\end{figure}

\noindent
A \textbf{designated port} is the port in charge of serving the link: it sends frames to the leaves and receives frames from the leaves.
\begin{enumerate}
\item The cost of each possible path is determined from each bridge connected to the link to the root. From these, the one with the smallest cost (a \ul{least-cost path}) is picked. The port connected to the link of the bridge which leads to that path is then the designated port for the link.
\item When multiple bridges on a link lead to a least-cost path to the root, the link uses the bridge with the \ul{smallest Bridge Identifier} to forward frames to the root. The port connecting that bridge to the link is the designated port for the link.
\item When a bridge is connected to a link with multiple cables, multiple ports on a single bridge are candidates for designated port. In this case, the path which passes through the port on the bridge itself that has the \ul{smallest Port Identifier} is used.
\end{enumerate}
\FloatBarrier

\subsubsection{Blocked port}
A \textbf{blocked port} never sends frames on its link and discards all the received frames (except for BDPU configuration messages).

Any active port that is not a root port or a designated port is a blocked port.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.45\linewidth]{../pic/B1/Spanning_tree_protocol_at_work_5}
	\caption[]{This diagram illustrates all port states as computed by the spanning tree algorithm for a sample network.\footnotemark}
\end{figure}
\footnotetext{This image is taken from Wikimedia Commons (\href{https://commons.wikimedia.org/wiki/File:Spanning_tree_protocol_at_work_5.svg}{Spanning tree protocol at work 5.svg}), it was made by \href{https://en.wikipedia.org/wiki/User:Ngriffeth}{Nancy Griffeth} and by user \href{https://pl.wikipedia.org/wiki/Wikipedysta:GhosT}{GhosT} and it is licensed under a \href{https://creativecommons.org/licenses/by/3.0/}{Creative Commons Attribution 3.0 Unported License}.}

\section{BPDU messages}
%35-65
The above criteria describe one way of determining what spanning tree will be computed by the algorithm, but the rules as written require knowledge of the entire network. The bridges have to determine the root bridge and compute the port roles (root, designated, or blocked) with only the information that they have.

Since bridges can exchange information about Bridge Identifiers and Root Path Costs, \textbf{Spanning Tree Protocol} (STP), standardized as IEEE 802.1D (1990), defines messages called \textbf{Bridge Protocol Data Unit}s (BPDU).

%37-84
\subsection{BPDU format}
\label{sez:formato_bpdu}
BPDUs have the following format:
\begin{table}[H]
  \centering
  \begin{tabular}{|c|c|c|c|c|c|}
    \multicolumn{1}{r@{}}{\footnotesize{1}} & \multicolumn{1}{r@{}}{\footnotesize{7}} & \multicolumn{1}{r@{}}{\footnotesize{8}} & \multicolumn{1}{r@{}}{\footnotesize{16}} & \multicolumn{1}{r@{}}{\footnotesize{24}} & \multicolumn{1}{r@{}}{\footnotesize{32}}\\
    \hline
    \multicolumn{4}{|c|}{Protocol ID (0)} & Version (0) & BPDU Type (0)\\
    \hline
    TC & 000000 & TCA & \multicolumn{2}{|c|}{Root Priority} & \multicolumn{1}{|c}{}\\
    \cline{1-5} \cdashline{6-6}
    \multicolumn{6}{c}{Root MAC Address}\\
    \cdashline{1-3} \cline{4-6}
    \multicolumn{3}{c|}{} & \multicolumn{3}{|c}{Root Path Cost}\\
    \hline
    \multicolumn{3}{c|}{} & \multicolumn{2}{|c|}{Bridge Priority} & \multicolumn{1}{|c}{}\\
    \cline{1-5} \cdashline{6-6}
    \multicolumn{6}{c}{Bridge MAC Address}\\
    \cdashline{1-3} \cline{4-6}
    \multicolumn{3}{c|}{} & Port Priority & Port Number & \multicolumn{1}{|c}{Message Age}\\
    \hline
    \multicolumn{3}{c|}{} & \multicolumn{2}{|c|}{Max Age} & \multicolumn{1}{|c}{Hello Time}\\
    \hline
    \multicolumn{3}{c|}{} & \multicolumn{2}{|c|}{Forward Delay} & \multicolumn{1}{|c}{}\\
    \cline{1-5}
  \end{tabular}
  \caption{Configuration BPDU format (35 bytes) in STP.}
\end{table}
\begin{table}[H]
  \centering
  \begin{tabular}{|c|c|c|}
   \multicolumn{1}{r@{}}{\footnotesize{16}} & \multicolumn{1}{r@{}}{\footnotesize{24}} & \multicolumn{1}{r@{}}{\footnotesize{32}}\\
   \hline
   Protocol ID (0) & Version (0) & BPDU Type (0x80)\\
   \hline
  \end{tabular}
  \caption{Topology Change Notification BPDU format (4 bytes).}
\end{table}
\noindent
where the fields are:
\begin{itemize}
\item \ul{Protocol Identifier} field (2 bytes): it specifies the IEEE 802.1D protocol (value 0);
\item \ul{Version} field (1 byte): it distinguishes Spanning Tree Protocol (value 0) from Rapid Spanning Tree Protocol (value 2) (please refer to chapter~\ref{cap:rapid_stp});
\item \ul{BPDU Type} field (1 byte): it specifies the type of BPDU:
\begin{itemize}
\item \textbf{Configuration BPDU} (CBPDU) (value 0): used for spanning tree computation, that is to determine the root bridge and the port states (please refer to section~\ref{sez:configuration_bpdu});
\item \textbf{Topology Change Notification BPDU} (TCN BPDU) (value 0x80): used to announce changes in the network topology in order to update entries in filtering databases (please refer to section~\ref{sez:topology_change_notification_bpdu});
\end{itemize}
%43
\item \ul{Topology Change} (TC) flag (1 bit): set by the root bridge to inform all bridges that a change occurred in the network;
\item \ul{Topology Change Acknowledgement} (TCA) flag (1 bit): set by the root bridge to inform the bridge which detected the change that its Topology Change Notification BPDU has been received;
%14-38
\item \ul{Root Identifier} field (8 bytes): it specifies the Bridge Identifier of the root bridge in the network:
\begin{itemize}
\item \ul{Root Priority} field (2 bytes): it includes the priority of the root bridge;
\item \ul{Root MAC Address} field (6 bytes): it includes the MAC address of the root bridge;
\end{itemize}
\item \ul{Bridge Identifier} field (8 bytes): it specifies the Bridge Identifier of the bridge which is propagating the Configuration BPDU:
\begin{itemize}
\item \ul{Bridge Priority} field (2 bytes): it includes the priority of the bridge;
\item \ul{Bridge MAC Address} field (6 bytes): it includes the MAC address of the bridge;
\end{itemize}
%39
\item \ul{Root Path Cost} field (4 bytes): it includes the path cost to reach the root bridge, as seen by the bridge which is propagating the Configuration BPDU;
%16-40
\item \ul{Port Identifier} field (2 bytes): it specifies the Port Identifier of the port which the bridge is propagating the Configuration BPDU on:
\begin{itemize}
\item \ul{Port Priority} field (1 byte): it includes the port priority;
\item \ul{Port Number} field (1 byte): it includes the port number;
\end{itemize}
\item \ul{Message Age} field (2 bytes): value, initialized to 0, which whenever the Configuration BPDU crosses a bridge is increased by the transit time throughout the bridge;\footnote{Time fields are expressed in units of 256\textsuperscript{th} seconds (about 4 ms).}
%64-76-77
\item \ul{Max Age} field (2 bytes, default value = 20 s): if the Message Age reaches the Max Age value, the received Configuration BPDU is no longer valid;\footnotemark[\value{footnote}]
%41
\item \ul{Hello Time} field (2 bytes, default value = 2 s): it specifies how often the root bridge generates the Configuration BPDU;\footnotemark[\value{footnote}]
%42
\item \ul{Forward Delay} field (2 bytes, default value = 15 s): it specifies the waiting time before forcing a port transition to another state.\footnotemark[\value{footnote}]
\end{itemize}

\subsection{BPDU generation and propagation}
%44-45
Only the root bridge can generate Configuration BPDUs: all the other bridges simply propagate received Configuration BPDUs on all their \ul{designated ports}. \ul{Root ports} are the ones that receive the best Configuration BPDUs, that is with the lowest Message Age value = lowest Root Path Cost. \ul{Blocked ports} never send Configuration BPDUs but keep listening to incoming Configuration BPDUs.

Instead Topology Change Notification BPDUs can be generated by any non-root bridge, and they are always propagated on root ports.

%36
When a bridge generates/propagates a BPDU frame, it uses the unique MAC address of the port itself as a source address, and the STP multicast address 01:80:C2:00:00:00 as a destination address:
\begin{table}[H]
  \centering
  \centerline{\begin{tabular}{|c|c|c|c|c|c|c|c|}
    \multicolumn{1}{c}{\footnotesize{6 bytes}} & \multicolumn{1}{c}{\footnotesize{6 bytes}} & \multicolumn{1}{c}{\footnotesize{2 bytes}} & \multicolumn{1}{c}{\footnotesize{1 byte}} & \multicolumn{1}{c}{\footnotesize{1 byte}} & \multicolumn{1}{c}{\footnotesize{1 byte}} & \multicolumn{1}{c}{\footnotesize{}} & \multicolumn{1}{c}{\footnotesize{4 bytes}}\\
    \hline
    \begin{tabular}[c]{@{}c@{}}01:80:C2:00:00:00\\(multicast)\end{tabular} & \begin{tabular}[c]{@{}c@{}}source bridge\\address (unicast)\end{tabular} & \textellipsis & 0x42 & 0x42 & 0x03 & BPDU & \textellipsis\\
    \hline
    \multicolumn{1}{c}{\begin{tabular}[t]{@{}c@{}}\footnotesize{destination}\\\footnotesize{MAC address}\end{tabular}} & \multicolumn{1}{c}{\begin{tabular}[t]{@{}c@{}}\footnotesize{source}\\\footnotesize{MAC address}\end{tabular}} & \multicolumn{1}{c}{\footnotesize{length}} & \multicolumn{1}{c}{\footnotesize{DSAP}} & \multicolumn{1}{c}{\footnotesize{SSAP}} & \multicolumn{1}{c}{\footnotesize{CTRL}} & \multicolumn{1}{c}{\footnotesize{payload}} & \multicolumn{1}{c}{\footnotesize{FCS}}\\
  \end{tabular}}
\end{table}

%46-47-48-49-50-51-52-53-54-55-56-57-58-59-70-71-72-75
\section{Dynamic behavior}
\subsection{Port states}
\label{sez:stati_porte}
\begin{description}
\item[Disabled] A port switched off because no links are connected to the port.
\item[Blocking] A port that would cause a loop if it were active. No frames are sent or received over a port in blocking state (Configuration BPDUs are still received in blocking state), but it may go into forwarding state if the other links in use fail and the spanning tree algorithm determines the port may transition to the forwarding state.
\item[Listening] The bridge processes Configuration BPDUs and awaits possible new information that would cause the port to return to the blocking state. It does not populate the filtering database and it does not forward frames.
\item[Learning] While the port does not yet forward frames, the bridge learns source addresses from frames received and adds them to the filtering database. It populates the filtering database, but does not forward frames.
\item[Forwarding] A port receiving and sending data. STP still keeps monitoring incoming Configuration BPDUs, so the port may return to the blocking state to prevent a loop.
\end{description}

%74
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|c|c|c|c|}
  \hline
  \begin{tabular}[c]{@{}c@{}}\textbf{port}\\\textbf{state}\end{tabular} & \begin{tabular}[c]{@{}c@{}}\textbf{port}\\\textbf{role}\end{tabular} & \begin{tabular}[c]{@{}c@{}}receive\\frames?\end{tabular} & \begin{tabular}[c]{@{}c@{}}receive and\\process\\CBPDUs?\end{tabular} & \begin{tabular}[c]{@{}c@{}}generate or\\propagate\\CBPDUs?\end{tabular} & \begin{tabular}[c]{@{}c@{}}update\\filtering\\database?\end{tabular} & \begin{tabular}[c]{@{}c@{}}forward\\frames?\end{tabular} & \begin{tabular}[c]{@{}c@{}}generate or\\propagate\\TCN BPDUs?\end{tabular} \\
  \hline
  disabled & \multirow{2}{*}{blocked} & no & no & no & no & no & no \\
  \cline{1-1} \cline{3-4}
  blocking & & yes & yes & no & no & no & no \\
  \cline{1-2} \cline{5-5}
  listening & (on transitioning) & yes & yes & yes & no & no & no \\
  \cline{1-2} \cline{6-6}
  \multirow{2}{*}{learning} & designated & yes & yes & yes & yes & no & no \\
  \cline{2-2} \cline{5-5} \cline{8-8}
  & root & yes & yes & no & yes & no & yes \\
  \cline{1-2} \cline{5-5} \cline{7-8}
  \multirow{2}{*}{forwarding} & designated & yes & yes & yes & yes & yes & no \\
  \cline{2-2} \cline{5-5} \cline{8-8}
  & root & yes & yes & no & yes & yes & yes \\
  \hline
 \end{tabular}}
 \caption{Port roles and states in STP.}
\end{table}

\subsection{Ingress of a new bridge}
\label{sez:configuration_bpdu}
When a new bridge is connected to a data-link network, assuming it has a Bridge Identifier highest than the one of the current root bridge in the network:
\begin{enumerate}
\item at first the bridge, without knowing anything about the rest of the network (yet), assumes to be the root bridge: it set all its ports as designated (\ul{listening} state) and starts generating Configuration BPDUs on them, saying it is the root bridge;
\item the other bridges receive Configuration BPDUs generated by the new bridge and compare the Bridge Identifier of the new bridge with the one of the current root bridge in the network, then they discard them;
\item periodically the root bridge in the network generates Configuration BPDUs, which the other bridges receive from their root ports and propagate through their designated ports;
\item when the new bridge receives a Configuration BPDU from the root bridge in the network, it learns it is not the root bridge because another bridge having a Bridge Identifier lower than its one exists, then it stops generating Configuration BPDUs and sets the port from which it received the Configuration BPDU from the root bridge as a root port;
\item also the new bridge starts propagating Configuration BPDUs, this time related to the root bridge in the network, on all its other (designated) ports, while it keeps receiving Configuration BPDUs propagated by the other bridges;
\item when a new bridge receives on a designed port a Configuration BPDU `best', based on criteria for designated port selection, with respect to the Configuration BPDU it is propagating on that port, the latter stops propagating Configuration BPDUs and turns to blocked (\ul{blocking} state);
\item after a time Forward Delay long, ports still designated and the root port switch from the listening state to the \ul{learning} one: the bridge starts populating its filtering database, to avoid the bridge immediately starts sending the frames in flooding overloading the network;
\item after a time Forward Delay long, designated ports and the root port switch from the learning state to the \ul{forwarding} one: the bridge can propagate also normal frames on those ports.
\end{enumerate}

%60-89
\section{Changes in the network topology}
\subsection{Recomputing spanning tree}
When a topology change occurs, STP is able to detect the topology change, thanks to the periodic generation of Configuration BPDUs by the root bridge, and to keep guaranteeing there are no rings in topology, by recomputing if needed the spanning tree, namely the root bridge and the port states.

%40
\subsubsection{Link fault}
When a link (belonging to the current spanning tree) faults:
\begin{enumerate}
\item Configuration BPDUs which the root bridge is generating can not reach the other network portion anymore: in particular, the designed port for the faulted link does not send Configuration BPDUs anymore;
\item the last Configuration BPDU listened to by the blocked port beyond the link `ages' within the bridge itself, that is its Message Age is increased over time;
\item when the Message Age reaches the Max Age value, the last Configuration BPDU listened to expires and the bridge starts over again electing itself as the root bridge: it resets all its ports as designated, and starts generating Configuration BPDUs saying it is the root bridge;
\item STP continues analogously to the case previously discussed related to the ingress of a new bridge:
\begin{itemize}
\item if a link not belonging to the spanning tree connecting the two network portions exists, the blocked port connected to that link at last will become root port in forwarding state, and the link will join the spanning tree;
\item otherwise, if the two network portions can not be connected one with each other anymore, in every network portion a root bridge will be elected.
\end{itemize}
\end{enumerate}

\subsubsection{Insertion of a new link}
When a new link is inserted, the ports which the new link is connected to become designated in listening state, and start propagating Configuration BPDUs generated by the root bridge in the network $\Rightarrow$ new Configuration BPDUs arrive through the new link:
\begin{itemize}
\item if the link has a cost low enough, the bridge connected to the link starts receiving from a non-root port Configuration BPDUs having a Root Path Cost lower than the one from the Configuration BPDUs received from the root port $\Rightarrow$ the root port is updated so that the root bridge can be reached through the best path (based on criteria for root port selection), as well as designated and blocked ports are possibly updated accordingly;
\item if the link has a too high cost, Configuration BPDUs crossing it have a too high Root Path Cost $\Rightarrow$ one of the two ports connected to the new link becomes blocked and the other one keeps being designated (based on criteria for designated port selection).
\end{itemize}

\subsection{Announcing topology changes}
\label{sez:topology_change_notification_bpdu}
%81-82
When after a topology change STP alters the spanning tree by changing the port states, it does not change entries in filtering databases of bridges to reflect the changes $\Rightarrow$ entries may be out of date: for example, the frames towards a certain destination may keep being sent on a port turned to blocked, until the entry related to that destination expires because its ageing time goes to 0 (in the worst case: 5 minutes!).

%83-84-85-86-87-88
STP contemplates a mechanism to speed up the convergence of the network with respect to the filtering database when a topology change is detected:
\begin{enumerate}
 \item the bridge which detected the topology change generates a \ul{Topology Change Notification BPDU} through its root port towards the root bridge to announce the topology change;\footnote{The bridge keeps generating the Topology Change Notification BPDU every Hello Time, until it receives the acknowledge.}
 \item crossed bridges immediately forward the Topology Change Notification BPDU through their root ports;
 \item the root bridge generates back a Configuration BPDU with Topology Change and \ul{Topology Change Acknowledgement} flags set to 1, which after being forwarded back by crossed bridges will be received by the bridge which detected the topology change;\footnote{The root bridge keeps generating back the acknowledgement Configuration BPDU for Max Age + Forward Delay.}
 \item the root bridge generates on all its designated ports a Configuration BPDU with the \ul{Topology Change} flag set;
 \item every bridge, when receiving the Configuration BPDU:
 \begin{enumerate}
  \item drops all the entries in its filtering database having ageing times lower than the Forward Delay;
  \item in turn propagates the Configuration BPDU on all its designated ports (keeping the Topology Change flag set);
 \end{enumerate}
 %90
 \item the network temporarily works in a sub-optimal condition because more frames are sent in flooding, until bridges populate again their filtering databases with new paths by learning algorithms\footnote{Please see section~\ref{sez:algoritmi_apprendimento}.}.
\end{enumerate}

\section{Issues}
\subsection{Performance}
\label{sez:prestazioni_stp}
%66-67-69
The STP philosophy is `deny always, allow only when sure': when a topology change occurs, frames are not forwarded until it is sure that the transient has dead out, that is there are no loops and the network is in a coherent status, also introducing long waiting times at the expense of convergence speed and capability of reaching some stations.

%68-73-80
Assuming to follow the timers recommended by the standard, namely:
\begin{itemize}
 \item the timing values recommended by the standard are adopted: Max Age = 20 s, Hello Time = 2 s, Forward Delay = 15 s;
 \item the transit time through every bridge by a BPDU does not exceed the TransitDelay = HelloTime $\div$ 2 = 1 s;
\end{itemize}
more than 7 bridges in a cascade between two end-systems can not be connected so that a Configuration BPDU can cross the entire network twice within the Forward Delay: if an eighth bridge was put in a cascade, in fact, in the worst case the ports at the new bridge, self-elected as the root bridge, would turn from the listening state to the forwarding one\footnote{When this constraint was established, the learning state had not been introduced yet and the ports turned directly from the listening state to the forwarding one.} before the Configuration BPDU coming from the root bridge at the other end of the network can arrive in time at the new bridge:\footnote{Exactly the minimum value for the Forward Delay would be equal to 14 s, but a tolerance 1 s long was contemplated.}
\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{../pic/B1/68.png}
\end{figure}

%76
With the introduction of the learning state, after a link fault the network takes approximately 50 seconds to converge to a coherent state:
\begin{itemize}
 \item 20 s (Max Age): required for the last Configuration BPDU listened to to expire and for the fault to be detected;
 \item 15 s (Forward Delay): required for the port transition from the listening state to the learning one;
 \item 15 s (Forward Delay): required for the port transition from the learning state to the forwarding one.
\end{itemize}

%91
In addition, achieving a coherent state within the network does not result necessarily in ending the disservice experienced by the user: in fact the fault may reflect also at the application layer, very sensitive to connectivity losses beyond a certain threshold:
\begin{itemize}
 \item database management systems may start long fault-recovery procedures;
 %106
 \item multimedia networking applications generating inelastic traffic (such as VoIP applications) suffer much from delay variations.
\end{itemize}

It would be possible to try customizing the values related to timing parameters to increase the convergence speed and extend the maximum bridge diameter, but this operation is not recommended:
\begin{itemize}
 %78
 \item without paying attention one risks reducing network reactivity to topology changes and impairing network functionality;
 %79
 \item at first sight it appears enough to work just on the root bridge because those values are all propagated by the root bridge to the whole network, but indeed if the root bridge changes the new root bridge must advertise the same values $\Rightarrow$ those parameters must actually be updated on all bridges.
\end{itemize}

Often STP is disabled on edge ports, that is the ports connected directly to the end hosts, to relieve disservices experienced by the user:
\begin{itemize}
 %73
 \item due to port transition delays, a PC connecting to the network would initially be isolated for a time two Forward Delays long;
 %90
 \item connecting a PC represents a topology change $\Rightarrow$ the cleanup of old entries triggered by the announcement of the topology change would considerably increase the number of frames sent in flooding in the network.
\end{itemize}

However exclusively hosts must be connected to edge ports, otherwise loops in the network could be created $\Rightarrow$ some vendors do not allow this: for example, Cisco's proprietary mechanism PortFast achieves the same objective without disabling STP on edge ports, being able to make them turn immediately to the forwarding state and to detect possible loops on them (that is two edge ports connected one to each other through a direct wire).

%13-102
\subsection{Scalability}
\label{sez:stp_scalabilita}
Given a root bridge, multiple spanning trees can be built connecting all bridges one with each other, but the spanning tree algorithm chooses the spanning tree made up of the lowest cost edges. In this way, paths are optimized only with respect to the root of the tree:
\begin{itemize}
\item disabled links are altogether unused, but someone still has to pay for keeping them active as secondary links for fault tolerance;
\item load balancing is not possible to distribute traffic over multiple parallel links $\Rightarrow$ links belonging to the selected spanning tree have to sustain also the load for the traffic which, if there was not STP, would take a shorter path by crossing disabled links:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.15\linewidth]{../pic/B1/102}
	\caption{STP is not suitable to work on a geographical scale.}
\end{figure}
\end{itemize}

An IP network instead is able to organize traffic better: the spanning tree is not unique in the entire network, but every source can compute its own tree and send traffic to the shortest path guaranteeing a higher link load balancing.

Virtual LANs (VLAN) solve this problem by setting up multiple spanning trees (please refer to chapter~\ref{cap:vlan}).

%93
\subsection{Unidirectional links}
STP assumes every link is bidirectional: if a link faults, frames can not be sent in either of two directions. Indeed fiber optical cables are unidirectional $\Rightarrow$ to connect two nodes two optical fiber cables are needed, one for communication in one direction and another for communication in the opposite direction, and a fault on one of the two cables stops only traffic in one direction.

If one of the two unidirectional links faults, a loop may arise on the other link despite STP: Configuration BPDUs are propagated unidirectionally from the root to the leaves $\Rightarrow$ if the direct propagation path breaks, the bridge at the other endpoint stops receiving from that link Configuration BPDUs from the root bridge, then moves the root port to another link and, assuming there is no one on that link, sets the port as designated creating a loop:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.66\linewidth]{../pic/B1/93.png}
\end{figure}

Unidirectional Link Detection (UDLD) is a proprietary protocol of Cisco's able to detect whether there are faults on a unidirectional link thanks to a sort of `ping', and to disable the port (`error disabled' state) instead of electing it as designated.

%98-99-100-101
\subsection{Positioning the root bridge}
\begin{figure}
\centering
	\begin{subfigure}[b]{.45\textwidth}
		\centering
		\includegraphics[width=0.8\linewidth]{../pic/B1/98a.png}
		\caption{Optimal configuration.}
	\end{subfigure}
\ \hspace{.05\textwidth} \
	\begin{subfigure}[b]{.45\textwidth}
		\centering
		\includegraphics[width=\linewidth]{../pic/B1/98b.png}
		\caption{Configuration to be avoided.}
	\end{subfigure}
\end{figure}

\noindent
The location of the root bridge has heavy impact on the network:
\begin{itemize}
 \item traffic from one side to another one of the network has to cross the root bridge $\Rightarrow$ \ul{performance}, in terms of aggregate throughput and bandwidth of ports, of the bridge selected as the root bridge should be enough to sustain a high amount of traffic;
 \item a \ul{star topology}, where the root bridge is the star center, is to be preferred $\Rightarrow$ every link connect only one bridge to the root bridge:
 \begin{itemize}
 \item more equable link \ul{load balancing}: the link should not sustain traffic coming from other bridges;
 \item higher \ul{fault tolerance}: a fault of the link affects only connectivity for one bridge;
 \end{itemize}
 \item \ul{servers} and \ul{datacenters} should be placed near the root bridge in order to reduce the latency of data communication;
\end{itemize}
$\Rightarrow$ the priority needs to be customized to a very low value for the bridge which has to be the root bridge, so as not to risk that another bridge is elected as the root bridge.
\FloatBarrier

The location of the \ul{backup bridge}, meant to come into play in case of fault of a primary link or of the primary root bridge:
\begin{itemize}
 \item fault of a primary link: the optimal configuration is a redundant star topology made up of secondary links, where every bridge is connected to the backup bridge by a secondary link;
 \item fault of the primary root bridge: the priority of the backup root bridge needs to be customized to a value slightly higher than the priority of the primary root bridge too, so as to force that bridge to be elected as the root bridge in case of fault.
\end{itemize}

\subsection{Security}
%92
STP has not built-in security mechanisms against attacks from outside.

\paragraph{Electing the user's bridge as the root bridge}
A user may connect to the network a bridge with a very low priority forcing it to become the new root bridge and changing the spanning tree of the network. Cisco's proprietary feature \textbf{BPDU Guard} allows edge ports to propagate only Configuration BPDUs coming from inside the network, rejecting the ones received from outside (the port goes into `error disabled' state).

%103
\paragraph{Rate limit on broadcast storm}
Almost all professional bridges have some form of broadcast storm control able to limit the amount of broadcast traffic on ports by dropping excess traffic beyond a certain threshold, but these traffic meters can not distinguish between frames in a broadcast storm and broadcast frames sent by stations $\Rightarrow$ they risk filtering legitimate broadcast traffic, and a broadcast storm is more difficult to be detected.

%104-105
\paragraph{Connecting bridges without STP}
A single bridge without STP or with STP disabled can start pumping broadcast traffic into the network so that a loop outside the control of STP is created: connecting a bridge port directly to another one of the same bridge, or connecting the user's bridge to two internal bridge of the network through two redundant links, are examples.

\paragraph{Multiple STP domains}
Sometimes two different STP domains, each one with its own spanning tree, should be connected to the same shared channel (e.g. two providers with different STP domains in the same datacenter). Cisco's proprietary feature \textbf{BPDU Filter} disables sending and receiving Configuration BPDUs on ports at domain peripheries, to keep the spanning trees separated.