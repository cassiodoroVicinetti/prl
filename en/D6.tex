\chapter{The network layer in LANs}
%3
Routers are a fundamental part of a LAN because they provide internet access and VLAN interconnection.

%5-6-7-8-9-10-11
\begin{multicols}{2}
\paragraph{Data-link-layer advantages}
\begin{itemize}
\item mobility (section~\ref{sez:mobility})
\item transparency (section~\ref{sez:algoritmi_apprendimento})
\item simple and fast forwarding algorithms
\end{itemize}
\vspace*{\fill}
\columnbreak
\paragraph{Network-layer advantages}
\begin{itemize}
\item scalability (sections~\ref{sez:scalability_l2},~\ref{sez:diametro_rete_l2})
\item security and network isolation (section~\ref{sez:sicurezza_l2})
\item efficient forwarding algorithms: hierarchical addressing, multiple forwarding trees
\item no slow fault recovery due to STP (section~\ref{sez:prestazioni_stp})
\end{itemize}
\end{multicols}

\section{Evolutions of interconnection devices}
\subsection{Layer 3 switch}
%13
In a corporate network the router represents the bottleneck for internet access and VLAN interconnection, because it implements complex algorithms running on a CPU.

%15
The \textbf{layer 3 switch} is a router made purely in hardware to improve performance. Its manufacture is less expensive with respect to traditional routers, but it lacks some advanced features:
\begin{itemize}
 \item no sophisticated routing protocols (e.g. BGP);
 \item limited set of network interfaces;
 \item no capability of applying patches and updates (e.g. IPv6 support, bug fixes, etc.);
 \item no protection features (e.g. firewall).
\end{itemize}

\subsection{Multilayer switch}
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{../pic/D6/17}
	\caption{Example of multilayer switch.}
\end{figure}

\noindent
The \textbf{multilayer switch} is a device integrating both L2 and L3 capabilities on the same hardware card: the customer can buy a multilayer switch and then configure every interface in L2 or L3 mode according to his needs, for a greater flexibility in the network deployment.

%18-19
On a multilayer switch four types of interfaces can be configured:
\begin{itemize}
 \item \ul{L2 physical interfaces}: in trunk (A) or access (B) mode;
 \item \ul{L3 physical interfaces}: they can terminate L3-pure (C) or trunk-mode (D) links;
 \item logical interfaces for VLAN interconnection:
 \begin{itemize}
  \item \ul{L3 sub-interfaces} (E): a L3 physical interface can split into multiple L3 sub-interfaces, one per VLAN;
  \item \ul{L3 virtual interfaces} (F): they connect the internal router with the internal bridge, one per VLAN.
 \end{itemize}
\end{itemize}
\FloatBarrier

%25
Interconnection of two VLANs through a one-arm router requires that traffic crosses twice the trunk link toward the router $\Rightarrow$ the multilayer switch, thanks to integrating routing and switching functionalities, virtualizes the one arm so that traffic enters with a VLAN tag and exits (even the same port which entered) directly with another VLAN tag, without making the load on a link be doubled:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.45\linewidth]{../pic/D6/25}
	\caption{The multilayer switch optimizes the one-arm router.}
\end{figure}

\section{Positioning interconnection devices}
\begin{figure}
\centering
	\begin{subfigure}[b]{.45\textwidth}
		\centering
		\includegraphics[width=\linewidth]{../pic/D6/12a}
		\caption{Backbone with VLAN segmentation.}
	\end{subfigure}
\hspace{0.01\textwidth}
	\begin{subfigure}[b]{.52\textwidth}
		\centering
		\includegraphics[width=\linewidth]{../pic/D6/12b}
		\caption{Backbone with IP segmentation.}
	\end{subfigure}
	\caption{Examples of router positioning in a corporate network.}
\end{figure}

%4-12
\noindent
Where is it better to position routers in a corporate network?
\begin{itemize}
 \item \ul{access}: only bridges (typically multilayer switches) connected directly to hosts;
 \item \ul{backbone}: two possible solutions exist:
 \begin{itemize}
  \item \ul{VLAN segmentation}: the whole corporate network is at the data-link layer, and every area (e.g. university department) is assigned a VLAN $\Rightarrow$ mobility is extended to the whole corporate network;
  \item \ul{IP segmentation}: each access bridge is connected to a router (typically layer 3 switch), and every area is assigned an IP network $\Rightarrow$ higher network isolation and higher scalability.\\
  Often internal bridges connect all the access routers one to each other and to the exit gateway router;
 \end{itemize}
 %51
 \item \ul{edge}: a router as the exit gateway toward Internet, usually an L4-7 multilayer switch having features at the transport layer and higher, such as protection (e.g. firewall), quality of service, load balancing, etc.
\end{itemize}
\FloatBarrier

%26-27-28-29-30-31-32-33-34-35-36-37-38-39-40-41-42-43-44-45-46-47-48-49-50
\section{Example of LAN design}
\afterpage{
\begin{landscape}
\begin{figure}[p]
	\centering
	\includegraphics[width=\linewidth]{../pic/D6/42}
	\caption{Example of LAN design.}
\end{figure}
\end{landscape}
}

\begin{itemize}
 \item \ul{multilayer switch on the edge}:
 \begin{itemize}
  \item with simple routers there would be a different IP network for each floor, to the benefit of mobility between floors;
  \item as many virtual interfaces are configured on the internal router as VLANs are in the building;
  \item all the ports towards floor bridges are configured in trunk mode, then every port can accept any VLAN, to the benefit of mobility between floors;
  \item it is an L4-7 multilayer switch for upper-layer features (in particular security functions);
 \end{itemize}
 \item \ul{traffic between edge routers}: an additional VLAN is specifically dedicated to L3 traffic which routers exchange (e.g. OSPF messages, HSRP messages), to split it from normal LAN traffic (otherwise a host may pretend to be a router and sniff traffic between routers);
 \item \ul{Multi-group HSRP (mHSRP)}: a multilayer switch can be active for some VLANs and stand-by for other ones;
 \item \ul{Per-VLAN Spanning Tree (PVST)}: a spanning tree protocol instance is active for each VLAN, to optimize paths based on VLANs.\\
 The root bridge must always be the HSRP active router, otherwise some paths are not optimized;
 \item \ul{direct link between multilayer switches}:
 \begin{itemize}
  \item it provides a direct path for additional L3 traffic between routers;
  \item it lightens traffic load on floor bridges, which typically are dimensioned to support few traffic;
  \item ports at the endpoints of the link are configured as L2 ports, to give the possibility even to normal traffic to cross this link in case one of the link to floor bridges fails;
  \item it is doubled in link aggregation for a greater fault tolerance and to exploit the available bandwidth on both the links (avoiding STP disables one of the two links).
 \end{itemize}
\end{itemize}