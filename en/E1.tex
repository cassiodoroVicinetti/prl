\chapter{Introduction to Storage Area Networks}
\section{Storage architectures}
%3
A company typically needs to store a lot of data:
\begin{itemize}
 \item \textbf{mainframes} (historical): data access is centralized on the same machine where they are physically stored;
 \item \textbf{client-server model}: several clients ask a server machine to retrieve data stored on hard disks;
 \item \textbf{peer-to-peer model}: data are distributed among all the machines connected one with each other, and every machine can ask every other machine to have some data.
\end{itemize}

\paragraph{Comparison}
\begin{itemize}
 \item \ul{costs}: each machine in a peer-to-peer network does not require a high computing power and a high storage capacity, in contrast to a server having to manage requests from multiple clients at the same time $\Rightarrow$ servers are very expensive;
 \item \ul{scalability}: in the peer-to-peer model data can be distributed over an unlimited number of machines, while the computing capacity and the storage capacity of a server are limited;
 \item \ul{robustness}: a server is characterized by a high reliability, but a fault is more critical to be solved; machines in a peer-to-peer network instead are more subject to faults because they are low-end and less reliable machines, but software managing the peer-to-peer network, being aware of this weakness, is designed to keep data integrity, by performing for example automatic backups.
\end{itemize}

%2
\paragraph{Datacenter}
A \textbf{datacenter} is a centralized location where all servers are concentrated, and allows to avoid having too many servers scattered around the company under the control of so many different organizations:
\begin{itemize}
 \item \ul{data access}: data may be available, but people needing them may belong to another organization or may not have the required permissions;
 \item \ul{integrity}: it is difficult to back up all servers if they are scattered around the company;
 \item \ul{security}: it is easy to steal a hard disk from an unprotected server.
\end{itemize}

%8
\section{DAS}
In a \textbf{Direct-Attached Storage} (DAS) system, every server has exclusive access to its own hard disk set:
\begin{itemize}
 \item \ul{internal disks}: it is not a proper solution for servers because, in case of fault, hard disks have to be physically extracted from the inner of the machine;
 %10
 \item \ul{external disks}: disks are connected to the server via SCSI; multiple disk sets can be connected in a cascade like a bus architecture.\\
 %25
 Disks can be put in a dedicated cabinet called \textbf{Just a Bunch of Disks} (JBOD): the SCSI controller is able to export a virtual drive structure which is different from the one of the physical disks, by aggregating or splitting disk capacities and providing advanced services (e.g. RAID).
\end{itemize}

The \textbf{Small Computer System Interface} (SCSI) standard defines a full protocol stack:
\begin{itemize}
 %9
 \item \ul{physical interfaces} (e.g. cables and connectors): they allow to physically connect hard disks to servers;
 %7
 \item \ul{protocols}: they allow to perform read and write transactions by directly addressing disk blocks according to the Logical Block Addressing (LBA) schema;
 %12
 \item \ul{commands} exported to applications: they allow to perform read and write operations by issuing commands like READ, WRITE, FORMAT, etc.
\end{itemize}

%11
\paragraph{Advantages}
\begin{itemize}
 \item \ul{low latency}: it is in the order of milliseconds through a disk and of microseconds through a cache;
 \item \ul{high reliability}: the error probability is very low, and the data integrity is always guaranteed;
 \item \ul{wide compatibility}: it is widely supported by operating systems and is used by a lot of external devices besides disks.
\end{itemize}

%15
\paragraph{Disadvantages}
\begin{itemize}
 \item \ul{slow error recovery}: since errors rarely occur, error recovery mechanisms are not particularly efficient from the performance point of view;
 \item \ul{centralized access to disks}: just the server can access disks $\Rightarrow$ in case the server faults, disks can no longer be accessed;
 \item \ul{scalability limitations}: at most 16 devices for a maximum length of 25 meters can be connected in a cascade.
\end{itemize}

%16
NASes (section~\ref{sez:NAS}) and SANs (section~\ref{sez:SAN}) allow to decouple disks from servers connecting those entities through a \ul{network} $\Rightarrow$ a disk can be accessed by multiple servers.

\section{NAS}
\label{sez:NAS}
\begin{figure}
	\centering
	\includegraphics[width=0.4\linewidth]{../pic/E1/19}
	\caption{Example of NAS.}
\end{figure}

%19
\noindent
A \textbf{Network-Attached Storage} (NAS) exports file systems, serving logical \ul{files}, instead of disk blocks, over the network (usually LAN).

%18
File systems are shared with \ul{network clients}: both servers and clients connected to the network can access files.

%20
Typical protocols used to export file systems are:
\begin{itemize}
 \item \textbf{Network File System} (NFS): popular on UNIX systems;
 \item \textbf{Common Internet File System} (CIFS): used by Windows systems;
\end{itemize}
\FloatBarrier
\noindent
which work over a TCP/IP network:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|}
  \hline
  NFS/CIFS \\
  \hline
  TCP \\
  \hline
  IP \\
  \hline
  Ethernet \\
  \hline
 \end{tabular}}
 \caption{NAS protocol stack.}
\end{table}

%21-22
\paragraph{Advantages}
\begin{itemize}
 \item along with the file system, user \ul{permissions} and access \ul{protections} (e.g. username and password) can be exported;
 \item \ul{compatibility with network clients}: a NAS system has a minimal impact on the existing infrastructure: all operating systems are able to mount a shared disk without additional drivers.
\end{itemize}

\paragraph{Disadvantages}
\begin{itemize}
 \item \ul{compatibility with applications}: the raw disk is invisible to the client: disks can not be formatted or managed at the block level $\Rightarrow$ some applications which need to directly access disk blocks can not work on remote disks: operating systems, database management systems, swap files/partitions;
 \item the NAS appliance requires enough \ul{computational power} for user permission management and remapping from file-related requests to block-related requests;
 \item the \ul{protocol stack} is not developed for NASes: TCP error-recovery mechanisms may introduce a non-negligible performance overhead.
\end{itemize}

\section{SAN}
\label{sez:SAN}
\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{../pic/E1/23}
	\caption{Example of two-tier SAN.}
\end{figure}

%23
\noindent
A \textbf{Storage Area Network} (SAN) exports \ul{physical disks}, instead of logical volumes, and allows to address disk \ul{blocks} according to the LBA schema, just as if the disk was connected directly to the server via SCSI (DAS system).

Clients can access data through servers, which they are connected to via a Wide or Local Area Network. Typically a datacenter follows a \textbf{three-tier model}:
\begin{enumerate}
 \item \ul{web server}: it is the front-end exposed to clients;
 \item \ul{application/database server}: it can mount a shared-disk file system which converts file-related requests by clients to block-related requests to be sent to remote disks via the SAN;
 \item \ul{hard disks}: they are often put in JBODs.
\end{enumerate}
\FloatBarrier

%24-27
SANs can not base exclusively on the classical TCP/IP, since TCP error-recovery mechanisms may introduce a non-negligible performance overhead $\Rightarrow$ some protocols have been developed for SANs aimed to keep as much as possible high speed, low latency and high reliability typical of SCSI:
%26
\begin{table}[H]
  \centering
  \begin{subfigure}[b]{0.28\linewidth}
    \centering
    \begin{tabular}{|c|}
      \hline
      SCSI \\
      \hline
      Fibre Channel \\
      \hline
    \end{tabular}
    \caption{Fibre Channel (\ref{sez:Fibre_Channel})}
  \end{subfigure}
  %
  \begin{subfigure}[b]{0.24\linewidth}
    \centering
    \begin{tabular}{|c|}
      \hline
      SCSI \\
      \hline
      Fibre Channel \\
      \hline
      FCoE \\
      \hline
      10 Gigabit Ethernet \\
      \hline
    \end{tabular}
    \caption{FCoE (\ref{sez:FCoE})}
  \end{subfigure}
  %
  \begin{subfigure}[b]{0.22\linewidth}
    \centering
    \begin{tabular}{|c|}
      \hline
      SCSI \\
      \hline
      iSCSI \\
      \hline
      TCP \\
      \hline
      IP \\
      \hline
      Ethernet \\
      \hline
    \end{tabular}
    \caption{iSCSI (\ref{sez:iSCSI})}
  \end{subfigure}
  %
  \begin{subfigure}[b]{0.22\linewidth}
    \centering
    \begin{tabular}{|c|}
      \hline
      SCSI \\
      \hline
      Fibre Channel \\
      \hline
      FCIP \\
      \hline
      TCP \\
      \hline
      IP \\
      \hline
      Ethernet \\
      \hline
    \end{tabular}
    \caption{FCIP (\ref{sez:FCIP})}
  \end{subfigure}
  \caption{SAN protocol stacks.}
\end{table}

All SAN protocols adopt SCSI as the upper layer in their protocol stacks and work below it $\Rightarrow$ this guarantees compatibility with all the existing SCSI-based applications, with a minimum impact for DAS to SAN migration.

\subsection{Fibre Channel}
\label{sez:Fibre_Channel}
\begin{figure}
	\centering
	\includegraphics[width=0.6\linewidth]{../pic/E1/29}
	\caption{Example of Fibre Channel-based SAN with switched fabric topology.}
\end{figure}

%28
\noindent
The \textbf{Fibre Channel} standard was born from the need to have a reliable support for \ul{optical fiber} connections between servers and storage disks, and is thought to replace the physical layer of SCSI. Fibre Channel supports high transfer rates: 1 Gbps, 2 Gbps, 4 Gbps, 8 Gbps, 16 Gbps.

%29
\subsubsection{Topologies}
The standard contemplates three possible topologies for SANs:
\begin{itemize}
 \item \ul{point-to-point}: direct connection between a server and a JBOD, like in SCSI;
 \item \ul{arbitrated loop}: ring topology for reliability purpose;
 \item \ul{switched fabric}: multiple servers are connected to multiple JBODs through a \textbf{fabric}, that is a mesh network of bridges.\\
 The switched fabric topology is new in the storage world: SCSI allowed only to connect in a cascade like a bus architecture.
\end{itemize}
\FloatBarrier

%32-33-34
%https://web.archive.org/web/20050122042808/http://www.redbooks.ibm.com/abstracts/tips0202.html
\subsubsection{Routing}
Routing is performed by the \textbf{Fabric Shortest Path First} (FSPF) protocol, very similar to the OSPF protocol in IP networks. No spanning tree protocols are contemplated for rings in topology.

Every port of a Fibre Channel node (server or JBOD) is dynamically assigned a 24-bit address:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|}
  \multicolumn{1}{r}{\footnotesize{8}} & \multicolumn{1}{r}{\footnotesize{16}} & \multicolumn{1}{r}{\footnotesize{24}} \\
  \hline
  Domain ID & Area ID & Port ID \\
  \hline
 \end{tabular}}
\end{table}
\noindent
where the fields are:
\begin{itemize}
 \item \ul{Domain ID} field (8 bits): it identifies the bridge which the node is connected to;
 \item \ul{Area ID} field (8 bits): it identifies the group of ports which the bridge port, to which the node is connected, belongs to;
 \item \ul{Port ID} field (8 bits): it identifies the node port.\\
 Every server is connected to the fabric through an interface called \textbf{Host Bus Adapter} (HBA).
\end{itemize}

%35-36
\subsubsection{Flow control}
Fibre Channel enhances SCSI error-recovery mechanisms by introducing a hop-by-hop flow control based on a \textbf{credit mechanism}: each port has an amount of credits, which is decreased whenever a packet is forwarded and is increased whenever an acknowledge is received $\Rightarrow$ if the available amount of credits goes down to 0, the port can not send other packets and has to wait for the next hop to communicate via an acknowledge which it is ready to receive other data into its buffer $\Rightarrow$ this mechanism avoids node buffer congestions and therefore packet losses.

Moreover the credit mechanism allows resource reservation and guarantees in-order delivery of frames: the destination node has not to implement a mechanism for packet re-ordering (like in TCP).

\paragraph{Issues}
\begin{itemize}
 \item traffic over a link can be blocked for a while due to lack of credits $\Rightarrow$ the maximum number of credits for a port has to be set properly based on the buffer capacity of the port which is at the other endpoint of the link;
 \item deadlocks may happen in a mesh network with circular dependencies.
\end{itemize}

%37
\subsubsection{Advanced features}
\begin{itemize}
 \item \ul{Virtual SAN} (VSAN): the equivalent of VLANs for SANs;
 \item \ul{link aggregation};
 \item \ul{load balancing};
 \item \ul{virtualization}: virtualization features of the SCSI controller can be moved directly to the bridge which the JBOD is connected to.
\end{itemize}

%41-42-43
\subsection[FCoE]{FCoE\footnote{This section includes CC BY-SA contents from article \href{https://en.wikipedia.org/wiki/Fibre_Channel_over_Ethernet}{Fibre Channel over Ethernet} on English Wikipedia.}}
\label{sez:FCoE}
The \textbf{Fibre Channel over Ethernet} (FCoE) technology allows to encapsulate Fibre Channel frames into Ethernet frames via the FCoE adaptation layer, which replaces the physical layer of Fibre Channel $\Rightarrow$ this allows to use 10 Gigabit Ethernet (or higher speeds) networks while preserving the Fibre Channel protocol.

Before FCoE, datacenters used Ethernet for TCP/IP networks and Fibre Channel for SANs. With FCoE, Fibre Channel becomes another network protocol running on Ethernet, alongside traditional IP traffic: FCoE operates directly above Ethernet in the network protocol stack, in contrast to iSCSI which runs on top of TCP and IP:
\begin{itemize}
 \item advantage: the server has no longer to have a Fibre Channel-specific HBA interface, but a single NIC interface can provide connectivity both to the SAN and to the Internet $\Rightarrow$ smaller number of cables and bridges, and lower power consumption;
 \item disadvantage: FCoE is not routable at the IP layer, that is it can not go over the Internet network outside the SAN.
\end{itemize}

Since, unlike Fibre Channel, the classical Ethernet includes no flow control mechanisms, FCoE required some enhancements to the Ethernet standard to support a priority-based flow control mechanism, to reduce frame loss from congestion.

\begin{figure}
	\centering
	\includegraphics[width=0.67\linewidth]{../pic/E1/42}
	\caption{Priority-based flow control in FCoE.}
\end{figure}

The basic idea is adopting PAUSE packets from the 802.3x standard for flow control over Ethernet\footnote{Please see section~\ref{sez:PAUSE}.}, but the Ethernet channel between two bridges is logically partitioned into \textbf{lanes} (for example, one dedicated to storage traffic and another one dedicated to the normal internet traffic) $\Rightarrow$ the PAUSE packet, instead of blocking the whole traffic over the concerned link, just blocks traffic of a certain lane without affecting traffic of other lanes.
\FloatBarrier

Typically for servers with FCoE technology top-of-the-rack (TOR) switches are preferred to end-of-the-row (EOR) switches used with Fibre Channel, because switches with FCoE technology are less expensive with respect to switches with Fibre Channel technology:
\begin{itemize}
 \item \ul{end-of-the-row switch}: there is a single main switch and every server is connected to it through its own cable $\Rightarrow$ longer cables;
 \item \ul{top-of-the-rack switch}: on top of each rack there is a switch, and every server is connected to its rack switch, then all rack switches are connected to the main switch $\Rightarrow$ more numerous switches, but shorter cables.
\end{itemize}

\subsection{iSCSI}
\label{sez:iSCSI}
\begin{figure}
	\centering
	\includegraphics[width=0.47\linewidth]{../pic/E1/iSCSI}
	\caption{Example of iSCSI-based SAN.}
\end{figure}

\noindent
The \textbf{Internet Small Computer System Interface} (iSCSI) protocol, proposed by Cisco to counteract Fibre Channel hegemony, allows to make a SAN by using the most common network technology, namely TCP/IP: SCSI commands are encapsulated into TCP packets via the iSCSI adaptation layer and cross the SAN over an Ethernet network.

\paragraph{Advantages}
\begin{itemize}
 \item the server has no longer to have a Fibre Channel-specific HBA interface, but a single NIC interface can provide connectivity both to the SAN and to the Internet $\Rightarrow$ smaller number of cables and bridges, and lower power consumption;
 \item disks can be reached also by clients via the Internet;
 \item optical fibers dedicated specifically for SAN connection do not need to be laid.
\end{itemize}

\paragraph{Disadvantages}
\begin{itemize}
 \item bridge buffers in the SAN need to be sized so as to minimize packet losses due to buffer overflow and therefore performance overhead due to TCP error-recovery mechanisms;
 \item the Ethernet technology is not very known in the storage world, where Fibre Channel tools are used to be used $\Rightarrow$ the iSCSI protocol has not been very successful.
\end{itemize}
\FloatBarrier

\subsection{FCIP}
\label{sez:FCIP}
\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{../pic/E1/FCIP}
	\caption{Example of FCIP-based SAN.}
\end{figure}

\noindent
A datacenter is subject to data loss risk due to natural disasters (like earthquakes, tsunamis, etc.) $\Rightarrow$ in order to improve resiliency (business continuity), the datacenter can entirely be replicated in another location, generally at a distance of some hundred kilometers. The main datacenter and the backup datacenter could communicate one with each other by using Fibre Channel, but connecting them through a simple optical fiber would be too expensive due to the long distance.

The \textbf{Fibre Channel over IP} (FCIP) technology allows geographically distributed SANs to be interconnected by using the existing TCP/IP infrastructure, namely Internet, without making internal devices in datacenters be aware of the presence of the IP network:
\begin{enumerate}
 \item the main datacenter sends a Fibre Channel frame;
 \item the edge router encapsulates the Fibre Channel frame into a TCP packet, via the FCIP adaptation layer replacing the physical layer of Fibre Channel, then forwards the TCP packet over the Internet network, in a sort of tunnel, up to the other edge router;
 \item the other edge router extracts the Fibre Channel frame and sends it to the backup datacenter;
 \item the backup datacenter receives the Fibre Channel frame.
\end{enumerate}

The Fibre Channel frame minimum size, however, exceeds the Ethernet payload size limit, and the overhead for fragmentation would be excessive $\Rightarrow$ Ethernet frames must be extended to about 2.2 KB so that minimum-size Fibre Channel frames can be encapsulated.
\FloatBarrier