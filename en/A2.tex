\chapter{Ethernet}
%3
\textbf{Ethernet} is nowadays the most used technology in wired LANs with shared bus architecture, because it is a \ul{simple} and \ul{little expensive} solution with respect to other LAN technologies such as token ring and token bus.

\section{Ethernet frame format}
Two versions of Ethernet exist, with different frame formats:
\begin{itemize}
 \item \textbf{DIX Ethernet II} (1982): version developed by DEC, Intel and Xerox (after this the `DIX' acronym) (section~\ref{sez:ethernet_dix});
 \item \textbf{IEEE 802.3} standard (1983): version standardized by the IEEE 802 working group (section~\ref{sez:ieee_8023}).
\end{itemize}

Since there are two versions of Ethernet, a considerable inhomogeneity in upper-layer protocol envelopments exists:
\begin{itemize}
 \item older protocols (e.g. IP) and protocols farther from IEEE use the DIX Ethernet II enveloping;
 \item protocols standardized since the beginning by IEEE (e.g. STP) use the IEEE 802.3 enveloping.
\end{itemize}

%19
\subsection{DIX Ethernet II}
\label{sez:ethernet_dix}
%20
The DIX Ethernet II packet\footnote{The standard names the Ethernet frame + the `Preamble', `SFD' e `IFG' fields of the physical layer as `Ethernet packet'.} has the following format:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|c|c|c|c|}
  \multicolumn{1}{c}{\footnotesize{7 bytes}} & \multicolumn{1}{c}{\footnotesize{1 byte}} & \multicolumn{1}{c}{\footnotesize{6 bytes}} & \multicolumn{1}{c}{\footnotesize{6 bytes}} & \multicolumn{1}{c}{\footnotesize{2 bytes}} & \multicolumn{1}{c}{\footnotesize{46 to 1500 bytes}} & \multicolumn{1}{c}{\footnotesize{4 bytes}} & \multicolumn{1}{c}{\footnotesize{12 bytes}} \\
  \hline
  preamble & SFD & \begin{tabular}[c]{@{}c@{}}\footnotesize{destination}\\\footnotesize{MAC address}\end{tabular} & \begin{tabular}[c]{@{}c@{}}\footnotesize{source}\\\footnotesize{MAC address}\end{tabular} & EtherType & payload & FCS & IFG \\
  \hline
  \multicolumn{2}{c|}{\footnotesize{}} & \multicolumn{5}{|c|}{\footnotesize{DIX Ethernet II frame (64 to 1518 bytes)}} & \multicolumn{1}{|c}{\footnotesize{}} \\
 \end{tabular}}
 \caption{DIX Ethernet II packet format (84 to 1538 bytes).}
\end{table}
\noindent
where the most significant fields are:
\begin{itemize}
 %21-23[A1]
 \item \ul{preamble} (7 bytes): bit sequence to recover synchronization between the transmitter clock and the receiver clock.\\
 %23
 Preamble can be shortened whenever the packet crosses a hub $\Rightarrow$ it is not possible to connect more than 4 hubs in a cascade (please refer to section~\ref{sez:hub});
 \item \ul{Start of Frame Delimiter} (SFD) field (1 byte): bit sequence identifying the beginning of the frame;
 \item \ul{EtherType} field (2 bytes): it identifies the upper-layer protocol used in the payload (it is a number greater or equal to 1500);
 \item \ul{Inter-Frame Gap} (IFG) field (12 bytes): pause, that is no signal, identifying the end of the frame.
\end{itemize}

%26
\subsection{IEEE 802.3}
\label{sez:ieee_8023}
The IEEE 802.3 packet can have one of the two following formats:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|c|c|c|c|}
  \multicolumn{1}{c}{\footnotesize{7 bytes}} & \multicolumn{1}{c}{\footnotesize{1 byte}} & \multicolumn{1}{c}{\footnotesize{14 bytes}} & \multicolumn{1}{c}{\footnotesize{3 bytes}} & \multicolumn{1}{c}{\footnotesize{0 to 1497 bytes}} & \multicolumn{1}{c}{\footnotesize{0 to 43 bytes}} & \multicolumn{1}{c}{\footnotesize{4 bytes}} & \multicolumn{1}{c}{\footnotesize{12 bytes}} \\
  \hline
  preamble & SFD & \begin{tabular}[c]{@{}c@{}}\footnotesize{MAC}\\\footnotesize{header}\end{tabular} & \begin{tabular}[c]{@{}c@{}}\footnotesize{LLC}\\\footnotesize{header}\end{tabular} & payload & padding & FCS & IFG \\
  \hline
  \multicolumn{2}{c|}{\footnotesize{}} & \multicolumn{5}{|c|}{\footnotesize{IEEE 802.3 frame (64 to 1518 bytes)}} & \multicolumn{1}{|c}{\footnotesize{}} \\
 \end{tabular}}
 \caption{IEEE 802.3 packet format with LLC header (84 to 1538 bytes).}
\end{table}
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|c|c|c|c|}
  \multicolumn{1}{c}{\footnotesize{7 bytes}} & \multicolumn{1}{c}{\footnotesize{1 byte}} & \multicolumn{1}{c}{\footnotesize{14 bytes}} & \multicolumn{1}{c}{\footnotesize{8 bytes}} & \multicolumn{1}{c}{\footnotesize{0 to 1492 bytes}} & \multicolumn{1}{c}{\footnotesize{0 to 38 bytes}} & \multicolumn{1}{c}{\footnotesize{4 bytes}} & \multicolumn{1}{c}{\footnotesize{12 bytes}} \\
  \hline
  preamble & SFD & \begin{tabular}[c]{@{}c@{}}\footnotesize{MAC}\\\footnotesize{header}\end{tabular} & \begin{tabular}[c]{@{}c@{}}\footnotesize{LLC SNAP}\\\footnotesize{header}\end{tabular} & payload & padding & FCS & IFG \\
  \hline
  \multicolumn{2}{c|}{\footnotesize{}} & \multicolumn{5}{|c|}{\footnotesize{IEEE 802.3 frame (64 to 1518 bytes)}} & \multicolumn{1}{|c}{\footnotesize{}} \\
 \end{tabular}}
 \caption{IEEE 802.3 packet format with LLC SNAP header (84 to 1538 bytes).}
\end{table}

\paragraph{Remarks}
\begin{itemize}
 \item the DIX Ethernet II and IEEE 802.3 frames have the same minimum and maximum lengths, because IEEE had to specify a frame format compatible with the old version of Ethernet;
 \item a DIX Ethernet II frame and an IEEE 802.3 frame can be distinguished by looking at the value in the field following the source MAC address:
  \begin{itemize}
   \item if it is lower or equal to 1500 (`Length' field), the frame is IEEE 802.3;
   \item if it is greater or equal to 1536 (`EtherType' field), the frame is DIX Ethernet II;
  \end{itemize}
 %28
 \item in the IEEE 802.3 frame the `Length' field would make superfluous the `Inter-Frame Gap' (IFG) field, but it is present to keep compatibility with the DIX Ethernet II frame;
 \item in the DIX Ethernet II frame the upper layer has to transmit at least 46 bytes, while in the IEEE 802.3 frame the frame can be stretched to the minimum size with some padding as needed;
 \item the LLC and LLC SNAP headers in the IEEE 802.3 frame waste a lot more bytes with respect to the `EtherType' field in the DIX Ethernet II frame although they are aimed to the same functionality of specifying the upper-layer protocol, and this is why the IEEE 802.3 standard has not been widely adopted to the benefit of DIX Ethernet II.
\end{itemize}

\section{Physical layer}
%29
10-Mbps Ethernet can work over the following transmission physical media:
\begin{itemize}
 \item \textbf{coaxial cable}: (section~\ref{sez:coassiale})
 \begin{itemize}
  \item \ul{10Base5}: thick cable (max 500 m);
  \item \ul{10Base2}: thin cable (max 185 m);
 \end{itemize}
 \item \textbf{twisted copper pair}: (section~\ref{sez:doppino})
 \begin{itemize}
  \item \ul{10BaseT}: cable with 4 twisted pairs of which just 2 used (max 100 m):
  %34
  \begin{itemize}
    \item Unshielded (UTP): unshielded;
    \item Shielded (STP): shielded with single global shield;
    \item Foiled (FTP): shielded with single global shield + a shield per pair;
  \end{itemize}
 \end{itemize}
 \item \textbf{optical fiber} (max 1-2 km) (section~\ref{sez:fibra})
\end{itemize}

%30-4[D4]
\subsection{Coaxial cable}
\label{sez:coassiale}
At the beginning shared bus was physically made by a coaxial cable:
 \begin{itemize}
  %31
  \item \ul{vampire taps}: every network card is connected to a thick coaxial cable through a vampire clamp, which allowed electrical propagation via physical contact (galvanic continuity) $\Rightarrow$ uncomfortable connection;
  %32
  \item \ul{T-connectors}: every network card is connected to a thin coaxial cable through a T-connector $\Rightarrow$ connecting and disconnecting a host requires to unplug the whole network.
 \end{itemize}

%33
\subsection{Twisted copper pair}
\label{sez:doppino}
With the introduction of the twisted copper pair, cabling (that is cable laying in buildings) acquired a greater flexibility: every host can be connected to a RJ45 wall socket through the specific RJ45 connector, and all sockets are in turn connected to a cabinet.

In addition, the RJ11 connector used by telephony can be connected to the RJ45 wall socket, too $\Rightarrow$ in cabling RJ45 sockets can be placed in the whole building and then one can decide whenever whether an Ethernet card or a telephone should be connected, by switching between the data connection and the telephone connection in the cabinet.

%35
\subsection{Optical fiber}
\label{sez:fibra}
\paragraph{Characteristics}
\begin{itemize}
 \item no sensitivity to electromagnetic interferences
 \item larger distances
 \item higher costs
 \item lower flexibility
\end{itemize}

%8
\section{CSMA/CD}
\label{sez:csma_cd}
A \textbf{collision} occurs when two or more nodes within the same collision domain\footnote{Please see section~\ref{sez:dominio_collisione}.} transmit at the same time and their signals overlap. The \textbf{Carrier Sense Multiple Access with Collision Detection} (CSMA/CD) protocol specify how to recognize a collision (CD) and how to recover a collision (retransmission).

%15-18
CSMA/CD is a simple and distributed \textbf{random-access} (that is non-deterministic) \textbf{protocol}: it does not contemplates intermediate devices or particular synchronization mechanisms, unlike token ring where the synchronization mechanism is the token itself $\Rightarrow$ the CSMA/CD protocol is efficient in terms of throughput because there is no overhead for synchronization, in terms of delays and channel occupancy.

In full-duplex mode the CSMA/CD protocol does no longer need to be enabled (please refer to section~\ref{sez:modalita_full_duplex}).

%6
\subsection{Detecting collisions}
Instead of transmitting the whole frame and just at the end checking for a collision, the node can use \textbf{Collision Detection} (CD): during the transmission sometimes it tries to understand whether a collision occurred (`listen while talking'), and if so it immediately stops the transmission, avoiding to waste the channel for a useless transmission.

%12-13
In the real world, collision detection is performed in two different ways depending on the type of transmission medium:
\begin{itemize}
 \item \ul{coaxial cable}: there is a single channel for both transmission and reception $\Rightarrow$ measuring the average DC on link is enough;
 \item \ul{twisted copper pair}, \ul{optical fiber}: there are two channels, one for transmission and another for reception:
 \begin{itemize}
  \item transmitting stations: they can realize that a collision occurred by detecting activity on the receiving channel during the transmission;
  \item non-transmitting stations: they can realize that a collision occurred only by detecting a wrong CRC code on the received frame.\\
  %14
  The \textbf{jamming sequence} is a powerful signal which is sent by who has noticed a collision to guarantee that the CRC code is invalid and to maximize probability that all the other nodes understand that a collision occurred.
 \end{itemize}
\end{itemize}

%4-5
\subsection{Reducing the number of collisions}
\textbf{Carrier Sense} (CS) allows to reduce the number of collisions: the node which wants to transmit listens to the channel before transmitting:
\begin{itemize}
 \item if it senses the channel is free: the node transmits the frame;
 \item if it senses the channel is busy:
\begin{itemize}
 \item \ul{1-persistent CSMA}: the node keeps checking whether the channel is free and transmit as soon as it becomes free;
 \item \ul{0-persistent CSMA}: the node tries again after a random time;
 \item \ul{$p$-persistent CSMA}: the node with probability $1 - p$ waits a random time (0-persistent), with probability $p$ immediately checks again (1-persistent).
\end{itemize}
\end{itemize}

In a LAN in the worst case the channel occupancy is equal to 30-40\% the available bandwidth $\Rightarrow$ Ethernet implements 1-persistent CSMA/CD because it is aimed for averagely unloaded networks with low probability of collisions.

\paragraph{CSMA limitations}
However, with twisted copper pair or optical fiber CSMA is not able to avoid collisions altogether (otherwise CD would not be useful): if propagation times are considered, a far node can sense the channel as free, even if actually it is busy but transmission has not reached the far node yet.

\begin{figure}
	\centering
	\includegraphics[width=0.33\linewidth]{../pic/A2/CSMA_vulnerability_interval.png}
\end{figure}

The \textbf{vulnerability interval} is defined as the time interval where starting a transmission by the far node would create a collision (it is equal to the propagation delay on the channel), and this interval is as larger as distance increases $\Rightarrow$ this protocol works well on small networks.
\FloatBarrier

%7-16
\subsection{Recovering collisions}
After a collision occurred, the frame has to be transmitted again. If the stations involved in the collision transmitted again immediately, another collision would occur $\Rightarrow$ \textbf{back-off algorithm} inserts into the wait a randomness element exponential in retransmissions:
\begin{itemize}
 \item 1\textsuperscript{st} retransmission: the node waits a time $T$ chosen randomly between 0 and 1 slot times;
\item 2\textsuperscript{nd} retransmission: the node waits a time $T$ chosen randomly from 0 to 3 slot times;
\item 3\textsuperscript{rd} retransmission: the node waits a time $T$ chosen randomly from 0 to 7 slot times;
\end{itemize}
%17
and so on, according to formula:
\[
 T = r \cdot \tau , \quad 0 \leq r < 2^k , \; k = \min{\left( n , 10 \right)} , \; n \leq 16
\]
where:
\begin{itemize}
 \item $n$ is the number of collisions occurred on the current frame;
 %22
 \item $\tau$ is the \textbf{slot time}, that is the time required to send an Ethernet frame of minimum size (64 bytes), equivalent to 51.2 $\mu$s.
\end{itemize}

At the end of every wait, the node listens again to the channel by CS.

\subsection{Constraint between frame size and collision diameter}
\label{sez:diametro_rete_l2}
%9-10
Since the channel access is contended, when one manages to get the network access it is better to transmit large packets. A minimum size for frames needs to be established: if the frame is too small and the collided transmission lasts too little time, it may happen that no stations notice the collision:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.2\linewidth]{../pic/A2/1-persistent_CSMA-CD_collision.png}
\end{figure}

%11-24
A constraint between the frame size $L_\text{PDU}$ and the collision diameter $D$ exists so that all collisions are recognized: collision detection works only if the round trip time $\text{RTT}$, that is the outward and return time, is lower than the transmission time $T_{\text{TX}}$:
\begin{figure}
	\centering
	\includegraphics[width=0.66\linewidth]{../pic/A2/Collision_detection_LANs.png}
\end{figure}
\[
 \text{RTT} < T_{\text{TX}} \Rightarrow 2 \frac{D}{V_{\text{PR}}} < \frac{L_{\text{PDU}}}{V_{\text{TX}}} \Rightarrow \begin{cases} \displaystyle L_{\text{PDU}} > \frac{V_{\text{TX}} \cdot 2D}{V_\text{PR}} \\ \displaystyle D < \frac{V_\text{PR} \cdot L_{\text{PDU}}}{2 V_\text{TX}} \end{cases}
\]
where $V_\text{TX}$ is the transmission speed and $V_\text{PR}$ is the propagation speed.

Increasing the transmission speed means increasing the frame minimum size, or for the same minimum size it means decreasing the maximum distance among nodes, but too large frames would increase the transmission error probability and would clog the network.
\FloatBarrier

In Ethernet DIX the theoretical collision diameter can not exceed 5750 meters:\footnote{For frame length $L_\text{PDU}$ preamble and SFD, but not IFG, are considered.}
\[
 \begin{cases}
  {L_\text{PDU}}_{\text{min}} = 72 \; \text{bytes} \\
  V_\text{TX} = 10 \; \text{Mbps} \\
  V_\text{PR} = c = 200000 \; \text{km/s}
 \end{cases} \Rightarrow D_\text{max} = \frac{V_\text{PR} \cdot {L_\text{PDU}}_\text{min}}{2 V_\text{TX}} = 5750 \; \text{m}
\]

%36-37
Without hubs the maximum network size is quite limited by maximum distances supported by transmission media (e.g. due to signal attenuation). Thanks to hubs the network size can be extended (although at most to 3 km due to non-idealities in devices): the hub, typically placed as the star center in a star topology, re-generates the signal (repeater) and internally simulates the shared bus allowing to connect multiple stations together through the twisted copper pair (please refer to section~\ref{sez:hub}).