\chapter{Ethernet evolutions}
%3
With the success of Ethernet new issues arose:
\begin{itemize}
 \item need for \ul{higher speed}: DIX Ethernet II supported a transmission speed equal to 10 Mbps, while FDDI, used in backbone, supported a very higher speed (100 Mbps), but it would have been too expensive to wire buildings by optical fiber;
 \item need to \ul{interconnect multiple networks}: networks of different technologies (e.g. Ethernet, FDDI, token ring), were difficult to interconnect because they had different MTUs $\Rightarrow$ having the same technology everywhere would have solved this problem.
\end{itemize}

\section{Fast Ethernet}
%6-8-9
\textbf{Fast Ethernet}, standardized as IEEE 802.3u (1995), raises the transmission speed to 100 Mbps and makes the maximum collision diameter 10 times shorter ($\sim$200-300 m) accordingly, keeping the same frame format and the same CSMA/CD algorithm.

%7
\subsection{Physical layer}
Fast Ethernet physical layer is altogether different than 10-Mbps Ethernet physical layer: it partially derives from existing standards in the FDDI world, so much that Fast Ethernet and FDDI are compatible at the physical layer, and definitively abandons the coaxial cable:
\begin{itemize}
 \item \ul{100BASE-T4}: twisted copper pair using 4 pairs;
 \item \ul{100BASE-TX}: twisted copper pair using 2 pairs;
 \item \ul{100BASE-FX}: optical fiber (only in backbone).
\end{itemize}

%5-10-11
\subsection{Adoption}
When Fast Ethernet was introduced, its adoption rate was quite low because of:
\begin{itemize}
 \item \ul{distance limit}: network size was limited $\Rightarrow$ Fast Ethernet was not appropriate for backbone;
 \item \ul{bottlenecks in backbone}: the backbone made in 100-Mbps FDDI technology had the same speed as access networks in Fast Ethernet technology $\Rightarrow$ it was unlikely to be able to drain all the traffic coming from access networks.
\end{itemize}

Fast Ethernet started to be adopted more widely with:
\begin{itemize}
 \item the introduction of \ul{bridges}: they break the collision domain overcoming the distance limit;
 \item the introduction of \ul{Gigabit Ethernet} in backbone: it avoids bottlenecks in backbone.
\end{itemize}

%12-16
\section{Gigabit Ethernet}
\textbf{Gigabit Ethernet}, standardized as IEEE 802.3z (1998), rises the transmission speed to 1 Gbps and introduces two features, `Carrier Extension' and `Frame Bursting', to keep the CSMA/CD protocol working.

\subsection{Carrier Extension}
%13-17-18
Decuplicating the transmission speed would make the maximum collision diameter 10 more times shorter putting it down to a few tens of meters, too few for cabling $\Rightarrow$ to keep the maximum collision diameter unchanged, the minimum frame size should be increased to 512 bytes\footnote{In theory the frame should be stretched 10 times more, then to 640 bytes, but the standard decides otherwise.}.

Stretching the minimum frame however would cause an incompatibility issue: in the interconnection of a Fast Ethernet network and a Gigabit Ethernet network by a bridge, minimum-sized frames coming from the Fast Ethernet network could not enter the Gigabit Ethernet network $\Rightarrow$ instead of stretching the frame the slot time, that is the minimum transmission time unit, was stretched: a \textbf{Carrier Extension} made up of padding dummy bits (up to 448 bytes) was appended to all the frames shorter than 512 bytes:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|c|c|c|c|}
  \multicolumn{1}{c}{\footnotesize{7 bytes}} & \multicolumn{1}{c}{\footnotesize{1 bytes}} & \multicolumn{1}{c}{\footnotesize{64 to 1518 bytes}} & \multicolumn{1}{c}{\footnotesize{0 to 448 bytes}} & \multicolumn{1}{c}{\footnotesize{12 bytes}} \\
  \hline
  preamble & SFD & Ethernet II DIX/IEEE 802.3 frame & Carrier Extension & IFG \\
  \hline
  \multicolumn{2}{c|}{\footnotesize{}} & \multicolumn{2}{|c|}{\footnotesize{512 to 1518 bytes}} & \multicolumn{1}{|c}{\footnotesize{}} \\
 \end{tabular}}
 \caption{Gigabit Ethernet packet format (532 to 1538 bytes).}
\end{table}

%14-15
\paragraph{Disadvantages}
\begin{itemize}
 \item Carrier Extension occupies the channel with \ul{useless bits}.\\
 For example with 64-byte-long frames useful throughput is very low:
 \[
  \frac{64 \; \text{bytes}}{512 \; \text{bytes}} \cdot 1 \; \text{Gbit/s} = 125 \; \text{Mbit/s}
 \]
 %24
 \item in newer pure switched networks the \ul{full-duplex mode} is enabled $\Rightarrow$ CSMA/CD is disabled $\Rightarrow$ Carrier Extension is useless.
\end{itemize}

\subsection{Frame Bursting}
%19
The maximum frame size of 1518 bytes is obsolete by now: in 10-Mbps Ethernet the channel occupancy was equal to 1.2 ms, a reasonable time to guarantee the statistical multiplexing\footnote{Please see section~\ref{sez:multiplexing_statistico}.}, while in Gigabit Ethernet the channel occupancy is equal to 12 $\mu$s $\Rightarrow$ collisions are a lot less frequent $\Rightarrow$ to reduce the header overhead in relation to useful data improving efficiency, the maximum frame size could be increased.

%20-21
Stretching the maximum frame however would cause an incompatibility issue: in the interconnection of a Fast Ethernet network and a Gigabit Ethernet network by a bridge, maximum-sized frames coming from the Gigabit Ethernet network could not enter the Fast Ethernet networks $\Rightarrow$ \textbf{Frame Bursting} consists in concatenating several standard-sized frames one after the other, without realising the channel:
\begin{table}
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|c|c|cc|c|}
 \hline
 \begin{tabular}[c]{@{}c@{}}frame 1\footnotemark\\\footnotesize{+ Carrier Extension}\end{tabular} & FILL & frame 2\footnotemark[\value{footnote}] & FILL & \textellipsis & FILL & \mbox{} & last frame\footnotemark[\value{footnote}] & IFG \\
 \hline
 \multicolumn{7}{|c|}{\footnotesize{burst limit (8192 bytes)}} & \multicolumn{2}{|c}{\footnotesize{}} \\
 \end{tabular}}
 \caption{Gigabit Ethernet packet format with Frame Bursting.}
\end{table}
\footnotetext{preamble + SFD + Ethernet II DIX/IEEE 802.3 frame}
\begin{itemize}
 \item just the first frame is possibly extended by Carrier Extension, to make sure that the collision window is filled; in the next frames Carrier Extension is useless because, if a collision would occur, it would already be detected by the first frame;
 \item IFG between a frame and another is replaced by a `Filling Extension' (FILL) to frame bytes and announce that another frame will follow;
 \item the transmitter station keeps a byte counter: when it arrives to byte number 8192, the frame currently in transmission must be the last one $\Rightarrow$ up to 8191 bytes + 1 frame can be sent with Frame Bursting.
\end{itemize}
\FloatBarrier

\paragraph{Advantages}
\begin{itemize}
 \item the \ul{number of collision chances} is reduced: once the first frame is transmitted without collisions, all the other stations detect that the channel is busy thanks to CSMA;
 \item the frames following the first one do not require Carrier Extension $\Rightarrow$ useful throughput increases especially in case of \ul{small frames}, thanks to saving for Carrier Extension.
\end{itemize}

%22
\paragraph{Disadvantages}
\begin{itemize}
 \item Frame Bursting does not address the primary goal of \ul{reducing the header overhead}: it was opted to keep in every frame all headers (including preamble, SFD and IFG) to make the processing hardware simpler;
 \item typically a station using Frame Bursting has to send a lot of data $\Rightarrow$ \ul{big frames} do not require Carrier Extension $\Rightarrow$ there is no saving for Carrier Extension;
 %24
 \item in newer pure switched networks the \ul{full-duplex mode} is enabled $\Rightarrow$ CSMA/CD is disabled $\Rightarrow$ Frame Bursting has no advantages and therefore is useless.
\end{itemize}

\subsection{Physical layer}
%25
Gigabit Ethernet can work over the following transmission physical media:
\begin{itemize}
 \item \ul{twisted copper pair}:
 \begin{itemize}
  \item \ul{Shielded} (STP): the 1000BASE-CX standard uses 2 pairs (25 m);
  \item \ul{Unshielded} (UTP): the 1000BASE-T standard uses 4 pairs (100 m);
 \end{itemize}
 \item \ul{optical fiber}: the 1000BASE-SX and 1000BASE-LX standards use 2 fibers, and can be:
 \begin{itemize}
  \item \ul{Multi-Mode Fiber} (MMF): less valuable (275-550 m);
  \item \ul{Single-Mode Fiber} (SMF): its maximum length is 5 km.
 \end{itemize}
\end{itemize}

%33
\paragraph{GBIC}
Gigabit Ethernet introduces for the first time \textbf{gigabit interface converter}s (GBIC), which are a common solution for having the capability of updating the physical layer without having to update the rest of the equipment: the Gigabit Ethernet board has not the physical layer integrated on board, but it includes just the logical part (from the data-link layer upward), and the user can plug into the dedicated board slots the desired GBIC implementing the physical layer.

%34-35
\section{10 Gigabit Ethernet}
\textbf{10 Gigabit Ethernet}, standardized as IEEE 802.3ae (2002), raises the transmission speed to 10 Gbps and finally abandons the half-duplex mode, removing all the issues deriving from CSMA/CD.

It is not still used in access networks, but it is mainly being used:
\begin{itemize}
 \item in \ul{backbones}: it works over optical fiber (26 m to 40 km) because the twisted copper pair is no longer enough because of signal attenuation limitations;
 \item in \ul{datacenters}: besides optical fibers, also very short cables are used to connect servers to top-of-the-rack (TOR) switches:\footnote{Please refer to section~\ref{sez:FCoE}.}
 \begin{itemize}
  \item Twinax: coaxial cables, first used because units for transmission over twisted copper pairs were consuming too much power;
  \item 10Gbase T: shielded twisted copper pairs, having a very high latency;
 \end{itemize}
 %36
 \item in \ul{Metropolitan Area Network}s (MAN) and \ul{Wide Area Network}s (WAN): 10 Gigabit Ethernet can be transported over already existing MAN and WAN infrastructures, although at a transmission speed decreased to 9.6 Gb/s.
\end{itemize}

%42
\section{40 Gigabit Ethernet and 100 Gigabit Ethernet}
\textbf{40 Gigabit Ethernet} and \textbf{100 Gigabit Ethernet}, both standardized as IEEE 802.3ba (2010), raise the transmission speed respectively to 40 Gbps and 100 Gbps: for the first time the transmission speed evolution is no longer at 10$\times$, but it was decided to define a standard at an intermediate speed due to still high costs for 100 Gigabit Ethernet. In addition, 40 Gigabit Ethernet can be transported over the already existing DWDM infrastructure.

These speeds are used only in backbone because they are not suitable yet not only for hosts, but also for servers, because they are very close to internal speeds in processing units (bus, memory, etc.) $\Rightarrow$ the bottleneck is no longer the network.