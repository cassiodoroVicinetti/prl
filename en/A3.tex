%Note: References are to 2013 slides.
\chapter{Repeaters and bridges}
\section{Interconnection at the physical layer}
\label{sez:dominio_collisione}
\label{sez:hub}
\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{../pic/A3/4}
	\caption{Interconnection at the physical layer in OSI stack.}
\end{figure}

%4-7
\noindent
\textbf{Repeater} and \textbf{hub}\footnote{The difference between repeaters and hubs lies in the number of ports: repeater has two ports, hub has more than two ports.} are network devices for interconnection at the physical layer, which just receive and propagate a sequence of bits. The interconnected physical-layer channels can also have different technologies (e.g. twisted pair to optical fiber), but all the upper layers must be equal (e.g. Ethernet-only, FDDI-only).

%6
Repeater also performs the function of recovering the signal degradation: it synchronizes itself with the square wave signal and regenerates it so as to clean it. The preamble preceding the frame is used for synchronization, that is signal recognition, and so whenever the signal crosses a repeater a part of this preamble is `eaten' $\Rightarrow$ it is not possible to connect more than 4 repeaters in a cascade.
\FloatBarrier

\begin{figure}
	\centering
	\includegraphics[width=0.66\linewidth]{../pic/A3/5}
	\caption{Example of interconnection at the physical layer.}
\end{figure}

%5-13
A \textbf{collision domain} is the set of nodes competing to access the same transmissive medium $\Rightarrow$ the simultaneous transmission causes collision. Interconnecting two \textbf{network segments} creates a single collision domain: repeater is not able to recognize collisions which are propagated to all ports $\Rightarrow$ this is a limit to the size of the physical domain.
\FloatBarrier

\section{Interconnection at the data-link layer}
\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{../pic/A3/8}
	\caption{Interconnection at the data-link layer in OSI stack.}
\end{figure}

%8-10-21
\noindent
\textbf{Bridge} and \textbf{switch} are network devices for interconnection at the data-link layer, which store (store-and-forward mode) and then regenerate the frame. Also the interconnected data-link-layer domains can have different technologies (e.g. Ethernet to FDDI).
\FloatBarrier

%9
\paragraph{Maximum frame size issue} In practice it is often impossible to interconnect two different data-link-layer technologies, due to issues related to the maximum frame size: for example, in an Ethernet-based network having MTU = 1518 bytes interconnected with a token ring-based network having MTU = 4 KB, what happens if a frame larger than 1518 bytes comes from the token ring network? In addition fragmentation at the data-link layer does not exist.

\begin{figure}
	\centering
	\includegraphics[width=0.66\linewidth]{../pic/A3/14}
	\caption{Example of interconnection at the data-link layer.}
\end{figure}

%9-12-15
Bridge decouples broadcast domain from collision domain:
\begin{itemize}
\item it `splits' the collision domain: it implements the CSMA/CD protocol to detect collisions, avoiding propagating them to the other ports;
\item it extends the \textbf{broadcast domain}: frames sent in broadcast are propagated to all ports.
\end{itemize}
\FloatBarrier

\begin{figure}[H]
	\centering
	\includegraphics[width=0.85\linewidth]{../pic/A3/12}
	\caption{Bridges avoid creating collisions thanks to their store-and-forward mode.}
\end{figure}

\subsection{Half-duplex and full-duplex modes}
\label{sez:modalita_full_duplex}
%17-18-19
A point-to-point link at the data-link layer between two nodes (e.g. a host and a bridge) can be performed in two ways:
\begin{itemize}
\item \textbf{half-duplex mode}: the two nodes are connected through a single bidirectional wire $ \Rightarrow$ each node can not transmit and receive at the same time, because a collision would happen;
\item \textbf{full-duplex mode}: the two nodes are connected through two separate unidirectional wires $ \Rightarrow$ each node can transmit and receive at the same time, thanks to the splitting of collision domains.
\end{itemize}
%\clearpage

%20
\paragraph{Full-duplex mode advantages}
\begin{itemize}
\item higher bandwidth: the throughput between the two nodes doubles;
\item absence of collisions:
\begin{itemize}
\item the CSMA/CD protocol does no longer need to be enabled;
\item the constraint on the minimum Ethernet frame size is no longer needed;
\item the limit on the maximum diameter for the collision domain does no longer exist (the only distance limit is the physical one of the channel).
\end{itemize}
\end{itemize}

%22-23-24-25-26-27-28-29-30-31-32-33-34-35-36-37-38-39-40-41-44-45-46-47-49-50-51-52-53-54-64
\subsection{Transparent bridge}
\label{sez:algoritmi_apprendimento}
\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{../pic/A3/37}
	\caption{Example of learning algorithm behaviour.}
\end{figure}

\noindent
Routing is performed in a transparent way: the bridge tries to learn the positions of the nodes connected to it filling a forwarding table called \textbf{filtering database}, whose entries have the following format:\\
\centerline{\textless MAC address\textgreater \ \textless destination port\textgreater \ \textless ageing time\textgreater}
where destination port is the port of the bridge, learnt by learning algorithms, which to make frames exit heading towards the associated destination MAC address.

\paragraph{Learning algorithms}
\begin{itemize}
\item \textbf{frame forwarding}: learning is based on \ul{destination MAC address}: when a frame arrives whose destination is not still in the filtering database, the bridge sends the frame in broadcast on all ports but the input port (\textbf{flooding}) and it waits for the reply which the destination is very likely to send back and which the backward learning algorithm will act on;
\item \textbf{backward learning}: learning is based on \ul{source MAC address}: when a frame arrives at a certain port, the bridge checks if there is already the source associated to that port in the filtering database, and if needed it updates it.
\end{itemize}

\textbf{Smart forwarding process} increases the network \ul{aggregate bandwidth}: frames are no longer propagated always in broadcast on all ports, but they are forwarded only on the port towards the destination, leaving other links free to transport other flows at the same time.
\FloatBarrier

\subsubsection{Mobility}
\label{sez:mobility}
\textbf{Ageing time} allows to keep the filtering database updated: it is set to 0 when the entry is created or updated by the backward learning algorithm, and it is increased over time until it exceeds the expiration time and the entry is removed. In this way the filtering database contains information about the only stations which are actually within the network, getting rid of information about old stations.

Data-link-layer networks natively support \textbf{mobility}: if the station is moved, remaining within the same LAN, so as to be reachable through another port, the bridge has to be `notified' of the movement by sending any broadcast frame (e.g. ARP Request), so that the backward learning algorithm can fix the filtering database. Windows systems tend to be more `loquacious' than UNIX systems.

Examples of stations which can move are:
\begin{itemize}
\item mobile phones;
\item virtual machines in datacenters: during the day they can be spread over multiple web servers to distribute the workload, during the night they can be concentrated on the same web server because traffic is lower allowing to save power;
%48
\item stations connected to the network via two links, one primary used in normal conditions and one secondary fault-tolerant: when the primary link breaks, the host can restore the connectivity by sending a frame in broadcast through the secondary link.
\end{itemize}

%67-68
\subsection{Switches}
\begin{figure}
	\centering
	\includegraphics[width=0.66\linewidth]{../pic/A3/70-71}
	\caption{Internal architecture of a switch.}
\end{figure}

\noindent
`\textbf{Switch}' is the commercial name given to bridges having advanced features to emphasize their higher performance:
\begin{itemize}
%21
\item a switch is a multi-port bridge: a switch has \ul{a lot more ports}, typically all in full-duplex mode, than a bridge;
\item the smart forwarding process is no longer a software component but it is implemented into a \ul{hardware chip} (the spanning tree algorithm keeps being implemented in software because more complex);
\item lookup for the port associated to a given MAC address in the filtering database is faster thanks to the use of \ul{Content Addressable Memories} (CAM), which however have a higher cost and a higher energy consumption;
\item switches support \ul{cut-through} forwarding technology faster than store-and-forward mode: a frame can be forwarded on the target port (unless it is already busy) immediately after receiving the destination MAC address.
\end{itemize}
\FloatBarrier

%65
\subsection{Issues}
\subsubsection{Scalability}
\label{sez:scalability_l2}
Bridges have scalability issues because they are not able to organize traffic, and therefore they are not suitable for complex networks (such as Wide Area Network):
\begin{itemize}
\item no \ul{filtering for broadcast traffic} $\Rightarrow$ over a wide network with a lot of hosts broadcast frames risk clogging the network;
\item the spanning tree algorithm make completely unused some links which would create rings in topology disadvantaging \ul{load balancing} (please refer to section~\ref{sez:stp_scalabilita}).
\end{itemize}

\subsubsection{Security}
\label{sez:sicurezza_l2}
Some attacks to the filtering database are possible:

%42
\paragraph{MAC flooding attack} The attacking station generates frames with random \ul{source} MAC addresses $\Rightarrow$ the filtering database gets full of MAC addresses of inexistent stations, while the ones of the existent stations are thrown out $\Rightarrow$ the bridge sends in flooding (almost) all the frames coming from the existent stations because it does not recognize the destination MAC addresses anymore $\Rightarrow$ the network is slowed down and (almost) all the traffic within the network is received by the attacking station.\footnote{Not all the traffic can be intercepted: when a frame comes from an existent station the bridge saves its source MAC address into its filtering database, so if immediately after a frame arrives heading towards that station, before the entry is cleared, it will not be sent in flooding.}

%43
\paragraph{Packet storm} The attacking station generates frames with random \ul{destination} MAC addresses $\Rightarrow$ the bridge sends in flooding all the frames coming from the attacking station because it does not recognize the destination MAC addresses $\Rightarrow$ the network is slowed down.