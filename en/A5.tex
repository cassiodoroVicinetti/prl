\chapter{Advanced features on Ethernet networks}
\section{Autonegotiation}
%4
\textbf{Autonegotiation} is a plug-and-play oriented feature: when a network card connects to a network, it sends impulses by a particular encoding to try to determine network characteristics:
\begin{itemize}
 \item \ul{mode}: half-duplex or full-duplex (over twisted pair);
 \item \ul{transmission speed}: starting from the highest speed down to the lowest one (over twisted pair and optical fiber).
\end{itemize}

\paragraph{Negotiation sequence}
\begin{itemize}
 \item 1 Gb/s full-duplex
 \item 1 Gb/s half-duplex
 \item 100 Mb/s full-duplex
 \item 100 Mb/s half-duplex
 \item 10 Mb/s full-duplex
 \item 10 Mb/s half-duplex
\end{itemize}

%5
\subsection{Issues}
Autonegotiation is possible only if the station connects to another host or to a bridge: hubs in fact work at fixed speed, hence they can not negotiate anything. If during the procedure the other party does not respond, the negotiating station assumes it is connected to a hub $\Rightarrow$ it automatically sets the mode to half-duplex.

If the user manually configures his own network card to work always in full-duplex mode disabling the autonegotiation feature, when he connects to a bridge the latter, not receiving reply from the other party, assumes to be connected to a hub and sets the half-duplex mode $\Rightarrow$ the host considers sending and receiving at the same time over the channel as possible, while the bridge considers that as a collision on the shared channel $\Rightarrow$ the bridge detects a lot of collisions which are false positives, and erroneously discards a lot of frames $\Rightarrow$ every discarded frame is recovered by TCP error-recovery mechanisms, which however are very slow $\Rightarrow$ the network access speed is very low. Very high values in collision counters on a specific bridge port are symptom of this issue.

%6-7-8
\section{Increasing the maximum frame size}
The original Ethernet specification defines:
\begin{itemize}
 \item maximum frame size: 1518 bytes;
 \item maximum payload size (Maximum Transmission Unit [MTU]): 1500 bytes.
\end{itemize}

However in several cases it would be useful to have a frame larger than normal:
\begin{itemize}
 \item additional headers (section~\ref{sez:baby_giant})
 \item bigger payload (section~\ref{sez:jumbo_frame})
 \item less CPU interrupts (section~\ref{sez:tcp_offloading})
\end{itemize}

\subsection{Baby Giant frames}
\label{sez:baby_giant}
\textbf{Baby Giant frame}s are frames having sizes bigger than the maximum size of 1518 bytes defined by the Ethernet original specification, because of inserting new data-link-layer headers in order to transport additional information about the frame:
\begin{itemize}
 \item \ul{frame VLAN tagging} (IEEE 802.1Q) adds 4 bytes;\footnote{Please refer to section~\ref{sez:frame_tagging}.};
 \item \ul{VLAN tag stacking} (IEEE 802.1ad) adds 8 bytes;\footnote{Please refer to section~\ref{sez:tag_stacking}.}
 \item \ul{MPLS} adds 4 bytes per stacked label.\footnote{Please refer to section \textit{MPLS header} in chapter \textit{MPLS} in lecture notes `Computer network technologies and services'.}
\end{itemize}

In Baby Giant frames the maximum payload size (e.g. IP packet) is unchanged $\Rightarrow$ a router, when receiving a Baby Giant frame, in re-generating the data-link layer can envelop the payload into a normal Ethernet frame $\Rightarrow$ interconnecting LANs having different supported frame maximum sizes is not a problem.

The IEEE 802.3as standard (2006) proposes to extend the maximum frame size to 2000 bytes, keeping the MTU size unchanged.

%9-10
\subsection{Jumbo Frames}
\label{sez:jumbo_frame}
\textbf{Jumbo Frame}s are frames having sizes bigger than the maximum size of 1518 bytes defined by the Ethernet original specification:
\begin{itemize}
 \item \ul{Mini Jumbo}s: frames having MTU size equal to 2500 bytes;
 \item \ul{Jumbo}s (or `Giant' or `Giant Frames'): frames having MTU size up to 9 KB;
\end{itemize}
because of enveloping bigger payloads in order to:
\begin{itemize}
 \item transport \ul{storage data}: typically elementary units of storage data are too big to be transported in a single Ethernet frame:
 \begin{itemize}
  \item the \ul{NFS protocol} for NASes transports data blocks of about 8 KB;\footnote{Please refer to section~\ref{sez:NAS}.}
  \item the \ul{FCoE protocol} for SANs and the \ul{FCIP protocol} for SAN interconnection transport Fibre Channel frames of about 2.5 KB;\footnote{Please refer to sections~\ref{sez:FCoE} and~\ref{sez:FCIP}.}
  \item the \ul{iSCSI protocol} for SANs transports SCSI commands of about 8 KB;\footnote{Please refer to section~\ref{sez:iSCSI}.}
 \end{itemize}
 \item reduce the \ul{header overhead} in terms of:
 \begin{itemize}
  \item \ul{saving for bytes}: it is not very significant, especially considering the high available bandwidth in today's networks;
  \item \ul{processing capability} for TCP mechanisms (sequence numbers, timers\textellipsis): every TCP packet triggers a CPU interrupt.
 \end{itemize}
\end{itemize}

If a LAN using Jumbo Frames is connected to a LAN not using Jumbo Frames, all Jumbo Frames will be fragmented at the IP layer, but IP fragmentation is not convenient from the performance point of view $\Rightarrow$ Jumbo Frames are used in independent networks within particular scopes.

\subsection{TCP offloading}
\label{sez:tcp_offloading}
Network cards with the \textbf{TCP offloading} feature can automatically condense on-the-fly multiple TCP payloads into a single IP packet before turning it to the operating system (sequence numbers and other parameters are internally handled by the network card) $\Rightarrow$ the operating system, instead of having to process multiple small packets by triggering a lot of interrupts, sees a single bigger IP packet and can make the CPU process it at once $\Rightarrow$ this reduces overhead due to TCP mechanisms.

\section{PoE}
%11-13
Bridges having the \textbf{Power over Ethernet} (PoE) feature are able to distribute electrical power (up to few tens of Watts) over Ethernet cables (only twisted copper pairs), to connect devices with moderate power needs (VoIP phones, wi-fi access points, surveillance cameras, etc.) avoiding additional cables for electrical power.

Non-PoE stations can be connected to PoE sockets.

%12
\subsection{Issues}
\begin{itemize}
 \item \ul{energy consumption}: a PoE bridge consumes a lot more electrical power (e.g. 48 ports each one at 25 W consume 1.2 kW) and is more expensive than a traditional bridge;
 \item \ul{service continuity}: a failure of the PoE bridge or a power blackout cause telephones, which instead are an important service in case of emergency, to stop working $\Rightarrow$ uninterruptible power supplies (UPS) need to be set but they, instead of providing just traditional telephones with electrical power, have to provide the whole data infrastructure with electrical power.
\end{itemize}