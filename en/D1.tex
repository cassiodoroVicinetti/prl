%Note: References are to 2013 slides.
\chapter{Virtual LANs}
\label{cap:vlan}
%6-7
\textbf{Virtual LAN}s (VLAN) allow to share a single physical infrastructure (same devices, same cabling) among multiple logical LANs: just traffic of a certain LAN flows through some ports, just traffic of another LAN flows through other ports, and so on $\Rightarrow$ each bridge has one filtering database for each VLAN.\footnote{In the real implementation, filtering database is unique and usually made with a single TCAM across the network device.}

A data-link network made up of multiple VLANs is more advantageous with respect to:
\begin{itemize}
%3
\item a network-layer network, thanks to \ul{mobility} support: hosts can keep being reachable at the same address (their MAC addresses) when moving;

%4
\item a single physical LAN, thanks to:
  \begin{itemize}
    \item greater \ul{scalability}: broadcast traffic is confined within smaller broadcast domains;
    \item greater \ul{security}: a user belonging to a VLAN can not carry out a MAC flooding attack on other VLANs;
    \item better \ul{policing}: the network administrator can configure different policies based on VLAN;
  \end{itemize}
  
%5
\item multiple LANs altogether separate from the physical point of view, thanks to a greater \ul{saving} of resources and costs: bridges are not duplicate for each LAN but are shared among all VLANs, as well as cables between bridges can transport traffic of any VLAN.
\end{itemize}

An example of VLAN application is a campus network: a VLAN is reserved for students, another VLAN is reserved for teachers with less restrictive policies, and so on.

\section{Interconnecting VLANs}
\label{sez:onearm_router}
%10
Data can not cross at the data-link layer the VLAN boundaries: a station in a VLAN can not send a frame to another station in a different VLAN, since VLANs have different broadcast domains. A possible solution may consist in connecting a port of a VLAN to a port of another VLAN, but in this way a single broadcast domain would form $\Rightarrow$ the two VLANs would belong actually to the same LAN.

%11-12
Therefore a station in a VLAN can send data to another station in a different VLAN just at the network layer $\Rightarrow$ a router is needed to connect a port of a VLAN to a port of another VLAN: a station in a VLAN sends an IP\footnote{For the sake of simplicity here IP protocol is considered as the network-layer protocol.} packet towards the router, then the latter re-generates the data-link-layer header of the packet (in particular it changes MAC addresses) and sends the packet to the station in the other VLAN. This solution however occupies two interfaces in a router and two ports in the same bridge, and requires two wires connecting these two network devices themselves $\Rightarrow$ a \textbf{one-arm router} allows to interconnect two VLANs through a single wire, occupying a single bridge port and a single router interface: traffic of both the VLANs can flow through the single wire and through the bridge port.

%13
Data-link-layer broadcast traffic still can not cross the VLAN boundaries, because the router do not propagate it to its other interfaces by splitting the broadcast domain $\Rightarrow$ a station in a VLAN which wants to contact a station in another VLAN can not discover its MAC address by ARP protocol, but has to send a packet to its IP address, which has a different network prefix because the two VLANs must have different addressing spaces.

%27
\section{Assigning hosts to VLANs}
%22
Every bridge makes available some ports, called \textbf{access ports}, which hosts can connect to. In access links \textbf{untagged frames}, that is without the VLAN tag (please refer to section~\ref{sez:frame_tagging}), flow; access ports tag frames based on their membership VLANs.

When a host connects to an access port, its membership VLAN can be recognized in four ways:
\begin{itemize}
 \item port-based assignment (section~\ref{sez:ass_porte})
 \item transparent assignment (section~\ref{sez:ass_trasparente})
 \item per-user assignment (section~\ref{sez:ass_utente})
 \item cooperative or anarchic assignment (section~\ref{sez:ass_cooperativa})
\end{itemize}

%14-28-29
\subsection{Port-based assignment}
\label{sez:ass_porte}
Every access port is associated to a single VLAN $\Rightarrow$ a host can access a VLAN by connecting to the related bridge port.

\paragraph{Advantages}
\begin{itemize}
 \item \ul{configuration}: VLANs should not be configured on hosts $\Rightarrow$ maximum compatibility with devices.
\end{itemize}

\paragraph{Disadvantages}
\begin{itemize}
 \item \ul{security}: the user can connect to any VLAN $\Rightarrow$ different VLAN-based policies can not be set;
 \item \ul{network-layer mobility}: although the user can connect to any VLAN, he still can not keep the same IP address across VLANs.
\end{itemize}

%30
\subsection{Transparent assignment}
\label{sez:ass_trasparente}
Every host is associated to a certain VLAN based on MAC address.

\paragraph{Disadvantages}
\begin{itemize}
 \item \ul{configuration}: a new user should contact the network administrator to record the MAC address of his device $\Rightarrow$ finding the MAC address of his own device can not be simple for a user;
 \item \ul{database cost}: a server, along with management staff, storing a database containing boundings between MAC addresses and VLANs, is needed;
 \item \ul{database maintenance}: entries corresponding to MAC address no longer in use should be cleared, but often the user when dismissing its device forgets contacting back the network administrator to ask him to delete its MAC address $\Rightarrow$ over time the database keeps growing;
 \item \ul{security}: the user can configure a fake MAC address and access another VLAN pretending to be another user.
\end{itemize}

%31
\subsection{Per-user assignment}
\label{sez:ass_utente}
Every user owns an account, and every user account is associated to a certain VLAN. When he connects to a bridge port, the user authenticates himself by inserting his own login credentials by the 802.1x standard protocol, and the bridge is able to contact a RADIUS server to check credentials and assign the proper VLAN to the user if successful.

\paragraph{Disadvantages}
\begin{itemize}
 \item \ul{compatibility}: authentication is performed at the data-link layer directly by the network card $\Rightarrow$ every device should have a network card compatible with the 802.1x standard;
 \item \ul{configuration}: the user has to set several configuration parameters (e.g. the authentication type) on his own device before being able to access the network.
\end{itemize}

%32-33
\subsection{Cooperative assignment}
\label{sez:ass_cooperativa}
Every user is associated by himself to the VLAN he wants: it is the operating system on the host which tags outgoing frames, so they will arrive through a trunk link to a bridge port already tagged.

\paragraph{Disadvantages}
\begin{itemize}
 \item \ul{configuration}: the user has to manually configure his own device before being able to access the network;
 \item \ul{security}: the user can connect to any VLAN $\Rightarrow$ different VLAN-based policies can not be set.
\end{itemize}

%15
\section{Frame tagging}
\label{sez:frame_tagging}
%16-17-19-20-21
\textbf{Trunk links} are links which can transport traffic of different VLANs:
\begin{itemize}
 \item trunk link between bridges (section~\ref{sez:link_bridge})
 \item trunk link between a bridge and a server (section~\ref{sez:link_bridge_server})
 \item trunk link between a bridge and a one-arm router (section~\ref{sez:link_bridge_server})
\end{itemize}

In trunk links \textbf{tagged frames}, that is having VLAN tag standardized as \textbf{IEEE 802.1Q} (1998), flow:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|}
  \multicolumn{1}{r@{}}{\footnotesize{16}} & \multicolumn{1}{r@{}}{\footnotesize{19}} & \multicolumn{1}{r@{}}{\footnotesize{20}} & \multicolumn{1}{r@{}}{\footnotesize{32}} \\
  \hline
  TPID (0x8100) & PCP & CFI & VLAN ID \\
  \hline
 \end{tabular}}
 \caption{VLAN tag format (4 bytes).}
\end{table}
\noindent
where fields are:
\begin{itemize}
 \item \ul{Tag Protocol Identifier} (TPID) field (2 bytes): it identifies a tagged frame (value 0x8100);
 \item \ul{Priority Code Point} (PCP) field (3 bits): it specifies the user priority for quality of service\footnote{Please see section~\ref{sez:ieee_8021p}.};
 \item \ul{Canonical Format Indicator} (CFI) flag (1 bit): it specifies whether the MAC address is in canonical format (value 0, e.g. Ethernet) or not (value 1, e.g. token ring);
 \item \ul{VLAN Identifier} (VID) field (12 bits): it identifies the VLAN of the frame:
 \begin{itemize}
  \item value 0: the frame does not belong to any VLAN $\Rightarrow$ used in case the user just wants to set the priority for his traffic;
  \item value 1: the frame belongs to the default VLAN;
  \item values from 2 to 4094: the frame belongs to the VLAN identified by this value;
  \item value 4095: reserved.
 \end{itemize}
\end{itemize}

IEEE 802.1Q does not actually encapsulate the original frame; instead, it adds the tag between the source MAC address and the EtherType/Length fields of the original frame, leaving the minimum frame size unchanged at 64 bytes and extending the maximum frame size from 1518 bytes to 1522 bytes $\Rightarrow$ on trunk links there can not be hubs because they do not support frames more than 1518 bytes long:
\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{../pic/D1/Ethernet_8021Q_Insert}
	\caption[]{Insertion of VLAN tag in an Ethernet frame.\footnotemark}
\end{figure}
\footnotetext{This picture is derived from an image on Wikimedia Commons (\href{https://commons.wikimedia.org/wiki/File:Ethernet_802.1Q_Insert.svg}{Ethernet 802.1Q Insert.svg}), made by user \href{https://commons.wikimedia.org/wiki/User:Arkrishna}{Arkrishna} and by \href{https://commons.wikimedia.org/wiki/User:Bill_Stafford}{Bill Stafford}, and is licensed under the \href{https://creativecommons.org/licenses/by-sa/3.0/}{Creative Commons Attribution-ShareAlike 3.0 Unported license}.}

%18-24
\subsection{In the backbone}
\label{sez:link_bridge}
\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{../pic/D1/18}
	\caption{Example of VLAN transport of a frame through a trunk link in the backbone.}
\end{figure}

\noindent
Transporting a frame from a station to another through trunk links is performed in the following way:\footnote{It is assumed to adopt the port-based assignment (please see section~\ref{sez:ass_porte}).}
\begin{enumerate}
\item the source host sends toward the access port an untagged frame;
\item when the frame arrives at the access port, the bridge tags the frame by the tag corresponding to the VLAN associated to the port;
\item the bridge sends the tagged frame to a trunk link;
\item every bridge receiving the frame looks at the filtering database related to the VLAN specified by the tag:
\begin{itemize}
 \item if the destination is `remote', the bridge propagates the frame to a trunk link leaving its VLAN tag unchanged;
 \item if the destination is `local', that is it can be reached through one of the access ports associated to the frame VLAN, the bridge removes the VLAN tag from the frame and sends the untagged frame to the access link toward the destination host.
\end{itemize}
\end{enumerate}
\FloatBarrier

\subsection{Virtual network interfaces}
\label{sez:link_bridge_server}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{../pic/D1/36}
	\caption{Example of VLAN transport of a frame through virtual network interfaces in a one-arm router and in a server.}
\end{figure}

%34
\noindent
Typically a server needs to be contacted at the same time by multiple hosts located in different VLANs $\Rightarrow$ since just one VLAN can be associated to each network interface, the server would require to have a network interface for each VLAN, each one connected to the bridge by its own physical link. A similar problem applies to a one-arm router, which needs to receive and send traffic from/to multiple different VLANs to allow their interconnection.

%35-36
\textbf{Virtual network interfaces} allow to have at the same time multiple logical network interfaces virtualized on the same physical network card, whose single physical interface is connected to the bridge through just one trunk physical link: the operating system sees multiple network interfaces installed on the system, and the network card driver based on VLAN tag exposes to the operating system every frame as if it had arrived from one of the virtual network interfaces.

Virtual network interfaces have different IP addresses, because every VLAN has its own addressing space, but they have the same MAC address, equal to the one of the physical network card; however this is not a problem as a MAC address just has to be unique within the broadcast domain (so within the VLAN).
\FloatBarrier

%58-59-60-61-62
\subsection{Tag stacking}
\label{sez:tag_stacking}
\textbf{Tag stacking} (also known as `provider bridging' or `Stacked VLANs' or `QinQ'), standardized as IEEE 802.1ad (2005), allows to insert multiple VLAN tags into the stack of a tagged frame, from the outer tag to the inner one:
\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{../pic/D1/TCPIP_8021ad_DoubleTag}
	\caption[]{Insertion of two VLAN tags in an Ethernet frame.\footnotemark}
\end{figure}
\footnotetext{This picture is derived from an image on Wikimedia Commons (\href{https://commons.wikimedia.org/wiki/File:TCPIP_802.1ad_DoubleTag.svg}{TCPIP 802.1ad DoubleTag.svg}), made by user \href{https://commons.wikimedia.org/wiki/User:Arkrishna}{Arkrishna} and by \href{https://it.wikipedia.org/wiki/Utente:Luca_Ghio}{Luca Ghio}, and is licensed under the \href{https://creativecommons.org/licenses/by-sa/4.0/}{Creative Commons Attribution-ShareAlike 4.0 International license}.}

Tag stacking is useful to transport traffic from multiple customers using VLANs over a shared provider network: two different customers may decide to use the same VLAN Identifier within their company networks $\Rightarrow$ bridges at the edges of the provider network add to incoming frames and remove from outgoing frames external tags which distinguish VLANs having the same VLAN Identifier but being of different customers.

%61
\paragraph{Advantages}
\begin{itemize}
 \item \ul{flexibility}: tag stacking is more flexible and less disruptive with respect to defining another tagging format with a larger VLAN Identifier;
 \item \ul{simplicity}: tag stacking is simpler with respect to \textbf{Ethernet tunneling}:
 \begin{itemize}
  \item Ethernet tunneling: edge bridges have to encapsulate the frame into a new Ethernet header $\Rightarrow$ complex operation;
  \item tag stacking: edge bridges just perform quicker push and pop operations in the tag stack;
 \end{itemize}
 \item \ul{VLAN scalability}: tag stacking is more scalable with respect to \textbf{VLAN translation}:
 \begin{itemize}
  \item VLAN translation: edge bridges change the VLAN Identifier in every frame so that each VLAN Identifier is unique within the provider network $\Rightarrow$ scalability issue: just a maximum of 4094 VLANs are available;
  \item tag stacking: edge bridges use an outer VLAN Identifier for each customer, regardless of the number of inner VLAN Identifiers each customer is using $\Rightarrow$ the provider network can serve up to 4094 customers, each one with 4094 VLANs.
 \end{itemize}
\end{itemize}

%62
\paragraph{Disadvantages}
\begin{itemize}
 \item \ul{MAC address scalability}: tag stacking is less scalable with respect to Ethernet tunneling:
 \begin{itemize}
  \item tag stacking: the filtering database in every bridge within the provider network should learn all the MAC addresses of the network interfaces located in all customers' VLANs $\Rightarrow$ scalability problem: filtering databases of bridges are stored in limited-size TCAM memories;
  \item Ethernet tunneling: the filtering database in every bridge within the provider network sees just MAC addresses of bridges at the edges of the network;
 \end{itemize}
 \item \ul{security}: a broadcast storm on a customer's VLAN may impair other customers' traffic (please refer to section~\ref{sez:isolamento_rete}).
\end{itemize}

\section{PVST}
\label{sez:PVST}
%46
Standard STP and RSTP do not support VLANs: the spanning tree is unique across the network and the spanning tree algorithm works regardless of VLANs. Several vendors offer proprietary features for VLAN support: for example Cisco offers \textbf{Per-VLAN Spanning Tree} (PVST) and Per-VLAN Spanning Tree Plus (PVST+), based on STP, and Rapid Per-VLAN Spanning Tree Plus (Rapid-PVST+), based on RSTP.

%47-48
PVST allows multiple spanning trees in the network, one for each VLAN; every tree is determined through per-VLAN configuration of spanning tree protocol parameters. In particular, priority of each bridge should be customized based on VLAN in order to differentiate the root bridge among different VLANs, otherwise the same tree would result for all VLANs, by identifying the priority value refers to by the STP Instance field (12 bits), introduced into Bridge Identifier by IEEE 802.1t (2001):
\begin{table}[H]
  \centering
  \centerline{\begin{tabular}{|c|c|c|}
    \multicolumn{1}{r@{}}{\footnotesize{4}} & \multicolumn{1}{r@{}}{\footnotesize{16}} & \multicolumn{1}{r@{}}{\footnotesize{64}} \\
    \hline
    Bridge Priority & STP Instance & Bridge MAC Address \\
    \hline
  \end{tabular}}
  \caption{Bridge Identifier format settled by IEEE 802.1t.}
\end{table}

\paragraph{Disadvantages}
\begin{itemize}
 \item \ul{traffic optimization}: optimization performed by PVST on traffic load is not so significant, even considering the high link bandwidth in modern networks:
  \begin{itemize}
   %49
   \item PVST optimizes the traffic load \ul{across the network}: if spanning trees are well balanced, all links are used $\Rightarrow$ there are not active active but altogether unused anymore;
   \item PVST does not optimize the traffic load \ul{inside a VLAN}: traffic in a VLAN is still bounded to a specific spanning tree $\Rightarrow$ the shortest path toward the destination can not be chosen as it happens in IP networks;
  \end{itemize}
 \item \ul{CPU load}: running multiple spanning tree protocol instances at the same time increases the load on bridge CPUs;
 \item \ul{interoperability}: coexistence within the same network of bridges having PVST support and bridges without it may lead to broadcast storms;
 \item \ul{complexity}: the network administrator has to manage multiple spanning trees in the same network $\Rightarrow$ troubleshooting is more complicated: the traffic path is more difficult to understand, since frames cross different links depending on the VLAN they belong to.
\end{itemize}

\section{Issues}
\subsection{Optimization of broadcast traffic}
\label{sez:isolamento_rete}
%39
Broadcast traffic is sent on all trunk links, besides access links associated to the VLAN which the broadcast frame belongs to:
\begin{itemize}
%50
\item a broadcast storm on a link, caused by traffic of one VLAN, may affect other VLANs by saturating trunk links $\Rightarrow$ although frames can not go from a VLAN to another at the data-link layer, \textbf{network isolation} is not complete even with VLANs due to the fact that links are shared;
%38
\item a broadcast frame belonging to a certain VLAN can reach a bridge at the end of the network on which there are no access ports belonging to that VLAN $\Rightarrow$ the filtering database in that bridge will insert through learning mechanisms a useless entry containing the source MAC address.
\end{itemize}

%37-40
In order to reduce broadcast traffic on trunk links and avoid useless entries in filtering databases, every bridge needs to know of which VLANs to propagate broadcast traffic on each trunk port:
\begin{itemize}
 \item \ul{GVRP protocol}: it is a standard, very complex protocol which allows bridges to exchange information about VLANs in the network topology;
 \item \ul{proprietary mechanisms}: they are used instead of GVRP protocol because they are simpler, although they introduce interoperability problems;
 %41
 \item \ul{manual configuration}: the network administrator explicitly configures on every bridge the VLANs which to propagate broadcast traffic of $\Rightarrow$ VLANs are statically configured and can not change in case of re-convergence of the spanning tree following a link fault.
\end{itemize}

\subsection{Interoperability}
%51
VLANs are not a plug-and-play technology (as STP was), and home users are not skilled enough to configure them $\Rightarrow$ low-end bridges typically are without VLAN support, and may discard tagged frames because too big.

%25
Another reason of incompatibility among network devices from different vendors is tagging on trunk ports: some bridges tag the traffic belonging to all VLANs, other ones leave the traffic belonging to VLAN 1 untagged.